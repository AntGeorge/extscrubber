# Web API

## Table of Contents

1. [finj](#module-finj)
    * [read_jreg](#command-read_jreg)
    * [write_jreg](#command-write_jreg)
    * [read_creg](#command-read_creg)
    * [write_creg](#command-write_creg)
    * [read_frames](#command-read_frames)
    * [write_frames](#command-write_frames)
    * [injection](#command-injection)
    * [examples](#examples)
2. [scrubber](#module-scrubber)
    * [enable](#command-enable)
    * [status](#command-status)
    * [reset_stats](#command-reset_stats)
3. [config](#module-config)
    * [get_configs_jtag](#command-get_configs_jtag)

## Module: finj

This module needs the following extra fields:

* port: The jtag controller to be used

### Command: read_jreg

Parameters:

* reg: Register to read (hex-string)
* n: Number of bits (decimal)

Read n bits from jtag register (LSB first)

### Command: write_jreg

Parameters:

* reg: Register to write (hex-string)
* n: Number of bits (decimal)
* data: Data to be written (list of hex-string values)

Write n bits from data to jtag register (LSB first)

### Command: read_creg

Parameters:

* reg: Configuration register to read (hex-string)

Read configuration register (MSB first)

### Command: write_creg

Parameters:

* reg: Configuration register to write (hex-string)
* data: Data to be written (hex-string)

Write configuration register (LSB first)

### Command: read_frames

Parameters:

* frad: Frame Address (hex-string)
* n: Number of frames to read (decimal)
* type: Selects readback (0) or readback capture (1) (decimal)

Read configuration frame

### Command: write_frames

Parameters:

* frad: Frame Address (hex-string)
* n: Number of frames to write (decimal)
* data: Frame data (list of lists of hex-string)

Write configuration frame

### Command: injection

Parameters:

* from_frad: Starting Frame Address (hex-string)
* to_frad: Ending Frame Address (hex-string)
* from_bit: Starting bit position in frame *from_frad* (decimal)
* to_bit: Ending bit position in frame *to_frad* (decimal)
* flips: Number of bit flips in the selected area, default 1 (decimal)
* iterations: Number of times the fault schema will applied (decimal)
* delay: Time delay between the iterations, default 0 (decimal)
* patterns: List of MBU patterns (list of hex-strings)
* type: Type of fault injection, default 0 (decimal)

Select an area based on the **from_frad** - **to_frad** and **from_bit** - **to_bit** parameters. Then bit-flip **flips** number of bits.

If **flips** is 0 or less then every bit in the selected area will be injected.
otherwise it injects the given number of bits in pseudo-random positions.

If **type** is 0 then the fault injection targets the configuration memory, if it is 1 then targets Flip-Flops.

The random positions are generated based on the parameters in order to be deterministic. So if parameters are the same (the selected area and the number of flips) then the random positions will also be the same.

### Examples

Read configuration register IDCODE
```json
{
    "module": "finj",
    "port": "JC",
    "command": "read_creg",
    "reg": "c"
}
```

Read configuration frame 0x00000900
```json
{
    "module": "finj",
    "port": "JC",
    "command": "read_frames",
    "frad": "900",
    "n": 1,
    "type": 0
}
```

Write configuration frame 0x00000900
```json
{
    "module": "finj",
    "port": "JC",
    "command": "write_frames",
    "frad": "900",
    "n": 1,
    "data": [["20800","0","20800","0","20800","0","20800","0","20800","0",
        "6020800","0","20800","0","20800","0","20800","0","20800","0",
        "6020800","0","20800","0","20800","0","20800","0","20800","0","20800",
        "0","20800","0","6020800","0","6020800","0","20800","0","20800","0",
        "6020800","0","6020800","0","20800","0","20800","0","e11","20800","0",
        "20800","0","6020800","0","20800","0","20800","0","20800","0","20800",
        "0","20800","0","20800","0","20800","0","6020800","0","20800","0",
        "20800","0","20800","0","6020800","0","20800","0","0","0","6020000",
        "0","20800","0","20800","0","0","0","6020800","0","20800","0",
        "6020800","0","0","0"]]
}
```

## Module: scrubber

### Command: enable

Parameters:

* value: 1 or 0 (decimal)

Enable or Disable scrubbing of configuration memory.

### Command: status

Parameters:

- Without parameters

Get current scrubber status.

Example of returned data.

```json
{
    "frame_corrections": 3,
    "bit_corrections": 3,
    "fails_count": 0,
    "enabled": 1,
    "heartbeat": 1,
    "failed_frames": ["0x900", "0x901"],
    "failed_syndromes": ["02310321", "299a87c62"]
}
```

### Command: reset_stats

Parameters:

- Without parameters

Reset statistics (frame corrections, bit corrections, etc).

## Module: Config

### Command: get_configs_jtag

Parameters:

* Without parameters

Get configuration values of Master System.

### Command: get_configs_sdcard_files

Parameters:

* Without parameters

Returns the files tree of sdcard.
