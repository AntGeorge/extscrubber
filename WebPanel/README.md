The control panel is generated using jekyll static site generator. We also use [Material Design for Bootstrap](https://mdbootstrap.com/) framework for the UI components.

To build the WebUI for the end use type

JEKYLL_ENV=production bundle exec jekyll build

Or if you want to test the application locally

JEKYLL_ENV=development bundle exec jekyll serve --trace
