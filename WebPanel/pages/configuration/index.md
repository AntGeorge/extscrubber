---
title: Master System Configuration
layout: default
module: config
---

<div class="container mt-3">
  <div class="row">
    <div class="col-md border">
        <div class="d-flex align-items-center justify-content-center">
            JTAG
            <span class="badge bg-primary mx-1" id="status">Status</span>
        </div>
        <div class="row">
            <div class="col">
                <div class="input-group mb-3">
                    <span class="input-group-text">divider</span>
                    <input type="text" id="divider" class="form-control" disabled />
                </div>
            </div>

            <div class="col">
                <div class="input-group mb-3">
                    <span class="input-group-text">shifts</span>
                    <input type="text" id="shifts" class="form-control" disabled />
                </div>
            </div>
        </div>
    </div>

    
    <!-- <div class="col-md border">
      One of three columns
    </div> -->

  </div>
    <div class="row">
        <div class="col-md border">
            {% include configuration/sdcard_browser.html %}
        </div>
    </div>
</div>


<script type="text/javascript">
    
    function refresh_jtag_config() {

        function set_obj(obj) {
            $("#divider").val(obj.divider);
            $("#shifts").val(obj.shifts);

            if (obj.status == 0) {
                $('#status').removeClass().addClass(['bg-success', 'badge', 'mx-1']);
            } else {
                $('#status').removeClass().addClass(['bg-danger', 'badge', 'mx-1']);
            }
        }

        {% if jekyll.environment == "production" %}
            json_data.command = "get_configs_jtag";
            
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '');
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.send(JSON.stringify(json_data));

            xhr.onload = function(e) {
                xhr = e.target

                var obj = JSON.parse(xhr.responseText);
                set_obj(obj)
            }
        {% elsif jekyll.environment == "development" %}
            var obj = JSON.parse(`{
                "divider": 2,
                "shifts": 1,
                "status": 0
            }`);

            set_obj(obj);
        {% endif %}
    }

    $(window).on("load", function () {
        refresh_jtag_config();
    });

</script>