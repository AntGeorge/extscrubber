---
title: Scrubber Blind
layout: default
module: scrubber_blind
---

<div class="container">
	<div class="row d-flex justify-content-center">
		<div class="col-md-6">
			<div class="custom-control custom-switch">
				<input type="checkbox" class="custom-control-input" id="enable_scrubber">
				<label class="custom-control-label" for="enable_scrubber">Enable Scrubber</label>
			</div>
			<span class="badge bg-primary mx-1" id="status" data-mdb-toggle="tooltip" title="Unknown">
				Status
			</span>
		</div>
	</div>
</div>

<script type="text/javascript">

	window.setInterval(function(){

		{% if jekyll.environment == "production" %}

			json_data.command = "status";

			var xhr = new XMLHttpRequest();
			xhr.open('POST', '');
			xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.send(JSON.stringify(json_data));

			xhr.onload = function(e) {
				xhr = e.target
			
				var obj = JSON.parse(xhr.responseText)

		{% elsif jekyll.environment == "development" %}

			var obj = JSON.parse(`{
                "status": 0,
			    "enabled": 1
			}`);

		{% endif %}

		statusExecution = 0;

		// Update page's components
		$("#enable_scrubber").prop("checked", obj.enabled);

		if (obj.status == 0) {
			$('#status').removeClass().addClass(['bg-success', 'badge', 'mx-1']);
			$('#status').attr('title','OK');
		} else if (obj.status == 1) {
			$('#status').removeClass().addClass(['bg-danger', 'badge', 'mx-1']);
			$('#status').attr('title','INIT FAILED');
		} else {
			$('#status').removeClass().addClass(['bg-danger', 'badge', 'mx-1']);
			$('#status').attr('title','STOPPED');
		}

		// Close xhr.onload
		{% if jekyll.environment == "production" %}
			};
		{% endif %}

	}, 1000);

	$("#enable_scrubber").change(function () {
		json_data.command = "enable";
		json_data.value = this.checked

		{% if jekyll.environment == "production" %}
			var xhr = new XMLHttpRequest();
			xhr.open('POST', '');
			xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.send(JSON.stringify(json_data))
		{% endif %}
	});

</script>