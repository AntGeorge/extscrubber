---
title: Fault Injection
layout: finjection
module: finj
---

<div class="container mt-3">
    <form id="command">
        <legend>Read n bits from jtag register (LSB first)</legend>
        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Register</span>
                </div>
                {% include platform/jregs_selection.html %}
                <small class="w-100">Register to read</small>
            </div>

        </div>

        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">N</span>
                </div>
                <input type="number" class="form-control" id="n" value="32">
                <small class="w-100">Number of bits to shift out</small>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Send Command</button>

    </form>
</div>

<div class="container">
    <div id="response" class="text-break">

    </div>
</div>

<script type="text/javascript"> 
    $("#command").submit(function(event) {
        
        json_data.reg = $("#register").val(); 
        json_data.n =  parseInt($("#n").val(), 10);
        json_data.port = $("#jtagPort").val();
        json_data.command = "read_jreg"
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '');
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(json_data))

        xhr.onload = function(e) {
            xhr = e.target
            $("#response").html(xhr.responseText);
        };

        event.preventDefault();
    });
</script>
