---
title: Fault Injection
layout: finjection
module: finj
---

<div class="container mt-3">
    <div class="row">
        <div class="col-md">
            <form id="command">
                <legend>Inject errors to specific area of the configuration memory</legend>
                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">From Frame Address</span>
                        </div>
                        <input type="text" class="form-control" id="from_frad">
                        <small class="w-100">Starting Frame Address in hex-string</small>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">To Frame Address</span>
                        </div>
                        <input type="text" class="form-control" id="to_frad">
                        <small class="w-100">Ending Frame Address in hex-string</small>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">From Bit</span>
                        </div>
                        <input type="number" class="form-control" id="from_bit">
                        <small class="w-100">Starting bit position of <strong>starting frame</strong> in decimal</small>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">To Bit</span>
                        </div>
                        <input type="number" class="form-control" id="to_bit">
                        <small class="w-100">Ending bit position of <strong>ending frame</strong> in decimal</small>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Flips</span>
                        </div>
                        <input type="number" class="form-control" id="flips">
                        <small class="w-100">Number of bit flips in the selected area, default 1</small>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Iterations</span>
                        </div>
                        <input type="number" class="form-control" id="iterations">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Delay</span>
                        </div>
                        <input type="number" class="form-control" id="delay">
                        <small class="w-100">Number of times the fault injection will be performed and the delay between the iterations (in ms), default 1 iteration with 0ms delay</small>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" for="type">Type</span>
                        </div>
                        <div class="form-check form-check-inline">
                            <input
                            class="form-check-input"
                            type="radio"
                            name="radio_type"
                            value="0"
                            checked
                            />
                            <label class="form-check-label" for="radio_config">Configuration</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input
                            class="form-check-input"
                            type="radio"
                            name="radio_type"
                            value="1"
                            />
                            <label class="form-check-label" for="radio_ff">FF</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input
                            class="form-check-input"
                            type="radio"
                            name="radio_type"
                            value="2"
                            disabled
                            />
                            <label class="form-check-label" for="radio_bram">BRAM</label>
                        </div>
                        <small class="w-100">Sets the injection type to Configuration memory bit, Flip-Flop memory or BRAM. This executes a SHUTDOWN-RCRC-GRESTORE-START sequence for Flip-Flop memory</small>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Send Command</button>

            </form>
        </div>

        <div class="col">
            <div class="container" id="patterns_container">
                <div>Patterns
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="enable_patterns">
                        <label class="custom-control-label" for="enable_patterns">Activate Patterns</label>
                    </div>
                </div>
                <button type="button" class="btn btn-primary btn-sm" id="add_pattern" disabled>Add MBU pattern</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="response" class="text-break">
            <table class="table">
                <caption style="caption-side: top">Injections</caption>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Frame</th>
                        <th scope="col">Bit</th>
                    </tr>
                </thead>
                <tbody id="resp_tbody">
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Return Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var num_of_patterns = 0;

    function removePattern(p) {
        $("#MBU_"+p).remove();
        for (var i = parseInt(p)+1; i < num_of_patterns; i++) {
            $("#MBU_"+i).attr("id", "MBU_"+(i-1));
            $("#pattern_"+i).attr("id", "pattern_"+(i-1));
            $("#switch_"+i).attr("id", "switch_"+(i-1));
            $("#enable_switch_"+i).attr("id", "enable_switch_"+(i-1));
            $("#delete_pattern_"+i).attr("id", "delete_pattern_"+(i-1));
        }
        num_of_patterns--;
    }

    function addPattern() {

        $("#patterns_container").append('<div id="MBU_'+num_of_patterns+'" ></div>')

        // Add Enable switch
        $("#MBU_"+num_of_patterns).append(`
            <div class="row">
            <div class="custom-control custom-switch col-12" id="switch_`+num_of_patterns+`">
            <input class="custom-control-input" id="enable_switch_`+num_of_patterns+`" type="checkbox" name="exampleSwitch" checked>
            <label class="custom-control-label" for="enable_switch_`+num_of_patterns+`"></label>
            <button type="button" class="close" id="delete_pattern_`+num_of_patterns+`" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            </div>
            `);

        $("#enable_switch_"+num_of_patterns).change(function (e) {
            $("#pattern_"+this.id.split('_').pop()+" :input").prop("disabled", function(i, v) { return !v; });
        });

        $("#delete_pattern_"+num_of_patterns).click(function (e) {
            removePattern(this.id.split('_').pop());
        });

        // Add 2D bit array
        $("#MBU_"+num_of_patterns).append('<div class="row d-flex justify-content-center" id="pattern_'+num_of_patterns+'">');
        for (var i = 0; i < 5; i++) {
            $("#pattern_"+num_of_patterns).append('<div class="w-100"></div>');
            for (var j = 0; j < 5; j++) {
                $("#pattern_"+num_of_patterns).append('<div class="col-sm-1"><input class="form-check-input position-static" type="checkbox" id="bit_'+j+'_'+i+'"></div>');
            }
        }

        num_of_patterns++;
    }

    function createPatternRegisters() {
        var registers_list = [];

        $('div[id^="MBU_"]').each(function () {

            if ($('input[id^="enable_switch_"]', this).is(":checked")) {
                var tmp_reg = 0;
                for (var x = 0; x < 5; x++) {
                    for (var y = 0; y < 5; y++) {
                        tmp_reg |= ($('#bit_'+x+'_'+y, this).is(":checked") << x*5 + y);
                    }
                }
                registers_list.push(tmp_reg.toString(16));
            }

        });

        return registers_list
    }

    function renderResponse(resp) {

        if (resp.return) {
            $(".modal-body").html(resp.return);
            $('#exampleModal').modal('show');
            return;
        }

        $("#resp_tbody").html("");

        for (var i in resp.injections) {
            $("#resp_tbody").append(`
                <tr>
                <th scope="row">` + i + `</th>
                <td>` + resp.injections[i].frame + `</th>
                <td>` + resp.injections[i].bit + `</th>
                </tr>
                `);
        }
    }

    $("#add_pattern").click(function () {
        addPattern();
    });

    $("#enable_patterns").change(function () {
        $("#patterns_container :input").not(this).prop("disabled", function(i, v) { return !v; });
    });


    $("#command").submit(function(event) {

        json_data.from_frad = $("#from_frad").val();
        json_data.to_frad = $("#to_frad").val();

        json_data.from_bit =  parseInt($("#from_bit").val(), 10);
        json_data.to_bit =  parseInt($("#to_bit").val(), 10);        

        json_data.flips =  parseInt($("#flips").val(), 10);

        json_data.iterations = parseInt($("#iterations").val(), 10);
        json_data.delay = parseInt($("#delay").val(), 10);

        json_data.type = parseInt($('input[name=radio_type]:checked', '#command').val(), 10);

        if ($("#enable_patterns").is(":checked")) {
            json_data.patterns = createPatternRegisters();
        } else {
            delete json_data.patterns;
        }

        json_data.port = $("#jtagPort").val();
        json_data.command = "injection"

        {% if jekyll.environment == "production" %}
        
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '');
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.send(JSON.stringify(json_data))

            xhr.onload = function(e) {
                xhr = e.target

                renderResponse(JSON.parse(xhr.responseText));
            };

        
        {% elsif jekyll.environment == "development" %}

            var responseText = `{
                "injections": 
                [
                    {"frame": "900", "bit": 0},
                    {"frame": "900", "bit": 1},
                    {"frame": "900", "bit": 2}
                ]
            }`

            renderResponse(JSON.parse(responseText));

        {% endif %}

        event.preventDefault();
    });

</script>