---
title: Fault Injection
layout: finjection
module: finj
---

<div class="container mt-3">
	<form id="command">
		<legend>Read n frames started from address frad</legend>
		<div class="row">
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text">Frame Address</span>
				</div>
				<input type="text" class="form-control" id="frad">
				<small class="w-100">Frame address to read in hex-string (e.g. 900)</small>
			</div>
		</div>

		<div class="row">
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text">N</span>
				</div>
				<input type="text" class="form-control" id="n">
				<small class="w-100">Number of frames to read in decimal</small>
			</div>
		</div>

		<div class="row">
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text" for="type">Type</span>
				</div>
				<div class="form-check form-check-inline">
					<input
					class="form-check-input"
					type="radio"
					name="radio_type"
					value="0"
					checked
					/>
					<label class="form-check-label" for="radio_rb">Readback</label>
				</div>

				<div class="form-check form-check-inline">
					<input
					class="form-check-input"
					type="radio"
					name="radio_type"
					value="1"
					/>
					<label class="form-check-label" for="radio_rbc">Readback Capture</label>
				</div>

				<small class="w-100">Sets the read frame type to readback or readback capture.</small>
			</div>
		</div>

		<button type="submit" class="btn btn-primary">Send Command</button>
	</form>
</div>

<div class="container">
	<div id="response" class="text-break">

	</div>
</div>

<script type="text/javascript"> 
	$("#command").submit(function(event) {
		
		json_data.frad = $("#frad").val(); 
		json_data.n =  parseInt($("#n").val(), 10);
		json_data.type = parseInt($('input[name=radio_type]:checked', '#command').val(), 10);
		json_data.port = $("#jtagPort").val();
		json_data.command = "read_frames"
		
		var xhr = new XMLHttpRequest();
		xhr.open('POST', '');
		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xhr.send(JSON.stringify(json_data))

		xhr.onload = function(e) {
			xhr = e.target
			$("#response").html(xhr.responseText);
		};

		event.preventDefault();
	});
</script>