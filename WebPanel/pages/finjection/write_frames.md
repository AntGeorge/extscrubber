---
title: Fault Injection
layout: finjection
module: finj
---

<div class="container mt-3">
    <form id="command">
        <legend>Write n frames started from address frad</legend>
        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Frame Address</span>
                </div>
                <input type="text" class="form-control" id="frad">
                <small class="w-100">Frame address to read in hex-string (e.g. 900)</small>
            </div>
        </div>

        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">N</span>
                </div>
                <input type="text" class="form-control" id="n">
                <small class="w-100">Number of frames to write in decimal</small>
            </div>
        </div>

        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Data</span>
                </div>
                <textarea class="form-control" id="data" rows="5"></textarea>
                <small class="w-100">Data to be written. Formated as a list of lists of hex-strings (e.g. [["frame1w0", "frame1w1", ...], ["frame2w0", ...]])</small>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Send Command</button>
    </form>
</div>

<div class="container">
    <div id="response" class="text-break">

    </div>
</div>

<script type="text/javascript"> 
    $("#command").submit(function(event) {
        
        json_data.frad = $("#frad").val(); 
        json_data.n =  parseInt($("#n").val(), 10);
        json_data.port = $("#jtagPort").val();
        json_data.command = "write_frames"
        json_data.data = eval($("#data").val())

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '');
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(json_data))

        xhr.onload = function(e) {
            xhr = e.target
            $("#response").html(xhr.responseText);
        };

        event.preventDefault();
    });
</script>