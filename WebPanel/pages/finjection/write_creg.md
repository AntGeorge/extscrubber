---
title: Fault Injection
layout: finjection
module: finj
---

<div class="container mt-3">
    <form id="command">
        <legend>Write 32-bit data to configuration register (LSB first)</legend>
        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Register</span>
                </div>
                {% include platform/cregs_selection.html %}
                <small class="w-100">Configuration register to write</small>
            </div>
        </div>

        <div class="row">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Data</span>
                </div>
                <input type="text" class="form-control" id="data">
                <small class="w-100">Data to be written. Formated as 8 hex characters (e.g. AABBCCDD) </small>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Send Command</button>
    </form>
</div>

<div class="container">
    <div id="response" class="text-break">

    </div>
</div>

<script type="text/javascript"> 
    $("#command").submit(function(event) {
        
        json_data.reg = $("#register").val(); 
        json_data.port = $("#jtagPort").val();
        json_data.command = "write_creg"
        json_data.data = $("#data").val()
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '');
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(json_data))

        xhr.onload = function(e) {
            xhr = e.target
            $("#response").html(xhr.responseText);
        };

        event.preventDefault();
    });
</script>