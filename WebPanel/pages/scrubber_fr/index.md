---
title: Scrubber FR
layout: default
module: scrubber_fr
---

<div class="container">
	<div class="row d-flex justify-content-center">
		<div class="col-md-6">
			<div class="custom-control custom-switch">
				<input type="checkbox" class="custom-control-input" id="enable_scrubber">
				<label class="custom-control-label" for="enable_scrubber">Enable Scrubber</label>
			</div>
			<span class="badge bg-primary mx-1" id="status" data-mdb-toggle="tooltip" title="Unknown">
				Status
			</span>
			<span class="badge bg-primary mx-1" id="heartbeat" data-mdb-toggle="tooltip" title="Unknown">
				Heartbeat
			</span>
		</div>
		<div class="col-md-6">
			<div class="row">
				<canvas id="lineChart"></canvas>
				<button type="button" class="btn btn-primary btn-sm" id="reset_chart">Reset</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$("#reset_chart").click(function() {
		json_data.command = "reset_stats";

		{% if jekyll.environment == "production" %}
			var xhr = new XMLHttpRequest();
			xhr.open('POST', '');
			xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.send(JSON.stringify(json_data));
		{% endif %}
	});

	var ctxL = document.getElementById("lineChart").getContext("2d");
	var myLineChart = new Chart(ctxL, {
		type: 'bar',
		data: {
			datasets: [
			{
				label: "Frame Corrections",
				data: [],
				backgroundColor: [
				'rgba(149, 244, 9, .2)'
				],
				borderColor: [
				'rgba(149, 198, 9, .7)'
				],
				borderWidth: 2
			},
			{
				label: "Unsuccessful corrections",
				data: [],
				backgroundColor: [
				'rgba(213, 16, 9, .2)'
				],
				borderColor: [
				'rgba(157, 16, 9, .7)'
				],
				borderWidth: 2
			}
			]
		},
		options: {
			responsive: true,
			scales: {
				yAxes: [{
					ticks: {
						min: 0
					}
				}]
			}
		}
	});

	window.setInterval(function(){

		{% if jekyll.environment == "production" %}

			json_data.command = "status";

			var xhr = new XMLHttpRequest();
			xhr.open('POST', '');
			xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.send(JSON.stringify(json_data));

			xhr.onload = function(e) {
				xhr = e.target
			
				var obj = JSON.parse(xhr.responseText)

		{% elsif jekyll.environment == "development" %}

			var obj = JSON.parse(`{
                "status": 0,
			    "frame_corrections": 3,
			    "fails_count": 0,
			    "enabled": 1,
			    "heartbeat": 1
			}`);

		{% endif %}

		statusExecution = 0;

		myLineChart.data.datasets[0].data = [obj.frame_corrections];
		myLineChart.data.datasets[1].data = [obj.fails_count];

		myLineChart.update();

		// Update page's components
		$("#enable_scrubber").prop("checked", obj.enabled);

		if (obj.heartbeat == 1) {
			$('#heartbeat').removeClass().addClass(['bg-success', 'badge', 'mx-1']);
			$('#heartbeat').attr('title','OK');
		} else if (obj.heartbeat == 0) {
			$('#heartbeat').removeClass().addClass(['bg-danger', 'badge', 'mx-1']);
			$('#heartbeat').attr('title','ERROR');
		} else {
			$('#heartbeat').removeClass().addClass(['bg-dark', 'badge', 'mx-1']);
			$('#heartbeat').attr('title','DISABLED');
		}

		if (obj.status == 0) {
			$('#status').removeClass().addClass(['bg-success', 'badge', 'mx-1']);
			$('#status').attr('title','OK');
		} else if (obj.status == 1) {
			$('#status').removeClass().addClass(['bg-danger', 'badge', 'mx-1']);
			$('#status').attr('title','INIT FAILED');
		} else {
			$('#status').removeClass().addClass(['bg-danger', 'badge', 'mx-1']);
			$('#status').attr('title','STOPPED');
		}

		// Close xhr.onload
		{% if jekyll.environment == "production" %}
			};
		{% endif %}

	}, 1000);

	$("#enable_scrubber").change(function () {
		json_data.command = "enable";
		json_data.value = this.checked

		{% if jekyll.environment == "production" %}
			var xhr = new XMLHttpRequest();
			xhr.open('POST', '');
			xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xhr.send(JSON.stringify(json_data))
		{% endif %}
	});

</script>