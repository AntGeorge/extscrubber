/*
 * Copyright (c) 2007 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <stdio.h>
#include <string.h>
#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#endif
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwipopts.h"

#include "webserver.h"

#include "utils/picohttpparser/picohttpparser.h"

#include "config_apps.h"
#ifdef __arm__
#include "FreeRTOS.h"
#include "task.h"
#include "xil_printf.h"
#endif
int generate_response(int sd, char *http_req, int http_req_len);
static unsigned http_port = 80;

/*
 * Read HTTP headers (not body).
 * Returns number of bytes read or -1 for
 * socket error.
 */
static int read_headers(int sd, char *buf)
{
	int bytesread = 0, read_len, i;
	char lastbyte, last4bytes[4+1];

	last4bytes[4] = '\0';

	while (1) {
		if ((read_len = read(sd, &lastbyte, 1)) < 0) {
			return -1;
		}
		buf[bytesread] = lastbyte;
		bytesread += read_len;

		for (i = 0; i < 3; i++) {
			last4bytes[i] = last4bytes[i+1];
		}
		last4bytes[3] = lastbyte;

		if (strcmp(last4bytes, "\r\n\r\n\0") == 0) {
			return bytesread;
		}
	}
}

/* thread spawned for each connection */
void
process_http_request(int sd)
{

	int initial_buffer_size = 1024;
	int read_len,j;
	int RECV_BUF_SIZE = initial_buffer_size;
	char *recv_buf = 0, *new_buf = 0;

	// Initial buffer size for headers. If more space is needed
	// for the body then a new allocation should be made.
	recv_buf = pvPortMalloc(RECV_BUF_SIZE);

	read_len = read_headers(sd, recv_buf);

	int pret, minor_version;
	char *method, *path;
	struct phr_header headers[20];
	size_t method_len, path_len, prevbuflen=0, num_headers;

	num_headers = sizeof(headers) / sizeof(headers[0]);
	pret = phr_parse_request(recv_buf, read_len, (const char **) &method, &method_len, (const char **) &path, &path_len,
				&minor_version, headers, &num_headers, prevbuflen);

	RECV_BUF_SIZE = 0;
	for (int i = 0; i < num_headers; i++) {
		int k = strncmp(headers[i].name, "Content-Length", 14);
		if (k == 0) {
			RECV_BUF_SIZE = atoi(headers[i].value);
			break;
		}
	}

	if (RECV_BUF_SIZE > 0) {

		if (pret + RECV_BUF_SIZE > initial_buffer_size) {
			new_buf = pvPortMalloc(pret + RECV_BUF_SIZE);
			memcpy(new_buf, recv_buf, pret);
			vPortFree(recv_buf);
			recv_buf = new_buf;
		}

		// Skip header length
		int bufferOffset = read_len;

		// Read the body of the request
		while(bufferOffset < pret + RECV_BUF_SIZE)
		{
			/* read in the request */
			if ((read_len = read(sd, recv_buf + bufferOffset, RECV_BUF_SIZE)) < 0) {
				close(sd);
				if (recv_buf != 0)
					vPortFree(recv_buf);
				if (new_buf != 0)
					vPortFree(new_buf);
				#ifdef OS_IS_FREERTOS
					vTaskDelete(NULL);
				#endif
				return;
			}

			bufferOffset += read_len;
		}
	}

	/* respond to request */
	generate_response(sd, recv_buf, pret + read_len);
	j = 0;
	while(j < 1000)
		j++;

	if (recv_buf != 0)
		vPortFree(recv_buf);
	if (new_buf != 0)
		vPortFree(new_buf);

	/* close connection */
	close(sd);
#ifdef OS_IS_FREERTOS
	vTaskDelete(NULL);
#endif
	return;
}

/* http server */
int
web_application_thread()
{
	int sock, new_sd;
	struct sockaddr_in address, remote;
	int size;

	/* create a TCP socket */
	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
#ifdef OS_IS_FREERTOS
	vTaskDelete(NULL);
#endif
		return -1;
	}

	/* bind to port 80 at any interface */
	address.sin_family = AF_INET;
	address.sin_port = htons(http_port);
	address.sin_addr.s_addr = INADDR_ANY;
	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0) {
#ifdef OS_IS_FREERTOS
	vTaskDelete(NULL);
#endif
		return -1;
	}

	/* listen for incoming connections */
	lwip_listen(sock, 0);

	size = sizeof(remote);
	while (1) {
		new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
		/* spawn a separate handler for each request */
		sys_thread_new("httpd", (void(*)(void*))process_http_request, (void*)new_sd,
                        THREAD_STACKSIZE,
                        DEFAULT_THREAD_PRIO);
	}
	return 0;
}

void
print_web_app_header()
{
#if HTTP_SERVER_DEBUG==1
    xil_printf("%20s %6d %s\r\n", "http server",
                        http_port,
                        "Point your web browser to http://192.168.1.10");
#endif
}
