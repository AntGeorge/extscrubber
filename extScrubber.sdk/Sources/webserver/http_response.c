/*
 * Copyright (c) 2007-2009 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <string.h>

#include "scrubber/scrubber.h"

#include "lwip/inet.h"
#include "lwip/sockets.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "webserver.h"
#include "platform/platform_fs.h"
#include "xil_printf.h"

#include "utils/utils.h"
#include "utils/frozen/frozen.h"
#include "utils/tiny-json/tiny-json.h"

#include "utils/picohttpparser/picohttpparser.h"
#include "strings.h"

#include "config_apps.h"

#include "ff.h"

extern jtag_t jtag_objects[ACTIVE_PORTS];

char *notfound_header =
	"<html> \
	<head> \
		<title>404</title> \
  		<style type=\"text/css\"> \
		div#request {background: #eeeeee} \
		</style> \
	</head> \
	<body> \
	<h1>404 Page Not Found</h1> \
	<div id=\"request\">";

char *notfound_footer =
	"</div> \
	</body> \
	</html>";

#define EXIT(MSG) 								\
	do {										\
	ret = (char *) pvPortMalloc(sizeof(MSG)); 	\
	sprintf(ret, MSG); 							\
	goto exit;									\
	} while(0)									\

int generate_json_config(struct json_out *out)
{
	int bytes = 0;

	bytes += json_printf(out, "{ divider:%d, ", jtag_objects[0].tck_divider);
	bytes += json_printf(out, "shifts:%d, ", jtag_objects[0].extra_tdo_shifts);
	bytes += json_printf(out, "status:%d }", jtag_objects[0].status);

	return bytes;
}

/* dynamically generate 404 response:
 *	this inserts the original request string in between the notfound_header & footer strings
 */
int do_404(int sd, char *req, int rlen)
{
	int len, hlen;
	int BUFSIZE = 1024;
	char buf[BUFSIZE];

	len = strlen(notfound_header) + strlen(notfound_footer) + rlen;

	hlen = generate_http_header(buf, "html", len);
	if (lwip_write(sd, buf, hlen) != hlen) {
		xil_printf("error writing http header to socket\r\n");
		xil_printf("http header = %s\n\r", buf);
		return -1;
	}

	lwip_write(sd, notfound_header, strlen(notfound_header));
	lwip_write(sd, req, rlen);
	lwip_write(sd, notfound_footer, strlen(notfound_footer));

	return 0;
}

int do_http_post(int sd, char *req, int rlen)
{
	int BUFSIZE = 1024;
	char *buf;
	int len = 0, res_len = 0;
	char *p;

	char *ret;

	int pret, minor_version;
	char *method, *path;
	struct phr_header headers[20];
	size_t method_len, path_len, prevbuflen=0, num_headers;

	num_headers = sizeof(headers) / sizeof(headers[0]);
	pret = phr_parse_request(req, rlen, (const char **) &method, &method_len, (const char **) &path, &path_len,
			&minor_version, headers, &num_headers, prevbuflen);

	json2_t raw_json;
	raw_json.raw_body = &req[pret];
	raw_json.len = rlen - pret;

	int res;
	char *module = NULL;
	char *command = NULL;

	res = json_scanf(raw_json.raw_body, raw_json.len, "{ module:%Q, command:%Q }", &module, &command);
	if ( res <= 0 ) {

		EXIT("{ \"return\": \"Error in json data\"}");
	}

	if (module == NULL || strcmp(module, "null") == 0) {

		EXIT("{ \"return\": \"Undefined or null 'module' field\"}");
	}

	enum {MAX_FIELDS = 128};
	json_t *pool = NULL;

	pool = pvPortMalloc(sizeof(json_t) * MAX_FIELDS);

	if (pool == NULL)
	{
		EXIT("{ \"return\": \"Not enough memory for json allocation\"}");
	}

	json_t const *s = json_create(raw_json.raw_body, pool, MAX_FIELDS);

	// === Redirect Request to the modules ===

	if (strcmp(module, "finj") == 0) {
		if (*server_qhandlers.finj_qr != NULL &&
			*server_qhandlers.finj_qt != NULL)
		{
			xQueueSend(*server_qhandlers.finj_qr, &s, portMAX_DELAY);
			xQueueReceive(*server_qhandlers.finj_qt, &ret, portMAX_DELAY);
		}
		else
		{
			EXIT("{ \"return\": \"Fault injection is not available\"}");
		}

		goto exit;

	} else if (strcmp(module, "scrubber") == 0) {
		if (*server_qhandlers.scrb_qr != NULL &&
			*server_qhandlers.scrb_qt != NULL)
		{
			xQueueSend(*server_qhandlers.scrb_qr, &s, portMAX_DELAY);
			xQueueReceive(*server_qhandlers.scrb_qt, &ret, portMAX_DELAY);
		}
		else
		{
			EXIT("{ \"return\": \"Scrubber is not available\"}");
		}

		goto exit;

	} else if (strcmp(module, "scrubber_fr") == 0) {
		if (*server_qhandlers.scrbFr_qr != NULL &&
			*server_qhandlers.scrbFr_qt != NULL)
		{
			xQueueSend(*server_qhandlers.scrbFr_qr, &s, portMAX_DELAY);
			xQueueReceive(*server_qhandlers.scrbFr_qt, &ret, portMAX_DELAY);
		}
		else
		{
			EXIT("{ \"return\": \"Scrubber FR is not available\"}");
		}

		goto exit;

	} else if (strcmp(module, "scrubber_blind") == 0) {
		if (*server_qhandlers.scrbBlind_qr != NULL &&
			*server_qhandlers.scrbBlind_qt != NULL)
		{
			xQueueSend(*server_qhandlers.scrbBlind_qr, &s, portMAX_DELAY);
			xQueueReceive(*server_qhandlers.scrbBlind_qt, &ret, portMAX_DELAY);
		}
		else
		{
			EXIT("{ \"return\": \"Scrubber Blind is not available\"}");
		}

		goto exit;

	} else if (strcmp(module, "config") == 0) {

		if (strcmp(command, "get_configs_jtag") == 0) {

			res = create_json(&ret, 64, generate_json_config);

			if (res == -1) {
				EXIT("Not enough memory");
			}

		} else if (strcmp(command, "get_configs_sdcard_files") == 0) {

			EXIT("WIP command\n");
		}

		goto exit;
	}
	else {
		xil_printf("http POST: unsupported command\r\n");
		EXIT("http POST: unsupported command\r\n");
	}

	exit:
		res_len = strlen(ret);
		buf = pvPortMalloc(BUFSIZE + res_len);
		len = generate_http_header(buf, "json", res_len);
		p = buf + len;
		for (int i=0; i < res_len; i++)
			*p++ = ret[i];
		*p = 0;
		vPortFree(ret);
		vPortFree(module);
		vPortFree(command);
		vPortFree(pool);
		len+=res_len;

	if (lwip_write(sd, buf, len) != len) {
#if HTTP_SERVER_DEBUG==1
		xil_printf("error writing http POST response to socket\r\n");
		xil_printf("http header = %s\r\n", buf);
#endif
		vPortFree(buf);
		return -1;
	}

	vPortFree(buf);

	return 0;
}

/* respond for a file GET request */
int do_http_get(int sd, char *req, int rlen)
{

	int BUFSIZE = 1400;
	char filename[MAX_FILENAME];
	char buf[BUFSIZE];
	int fsize, hlen;
	char *fext;

	FIL fp;
	FRESULT fr;
	UINT n;

	/* determine file name from the request*/
	extract_file_name(filename, req, rlen, MAX_FILENAME);

	/* debug statement on UART */
#if HTTP_SERVER_DEBUG==1
		xil_printf("http GET: %s\r\n", filename);
#endif

	f_chdir(WEBSERVER_ROOT);

	/* respond with 404 if not present */
	fr = f_open(&fp, filename, FA_READ);

	if (fr != FR_OK) {
#if HTTP_SERVER_DEBUG==1
		xil_printf("requested file %s not found, returning 404\r\n", filename);
#endif

		fr = f_open(&fp, "404.html", FA_READ);

		if (fr != FR_OK) {
			do_404(sd, req, rlen);
			return -1;
		}

		strcpy(filename, "404.html");

	}

	/* respond with correct file */

	/* get a pointer to file extension */
	fext = get_file_extension(filename);

	/* obtain file size,
	 * note that lseek with offset 0, MFS_SEEK_END does not move file pointer */
	fsize = f_size(&fp);

	/* write the http headers */
	hlen = generate_http_header(buf, fext, fsize);
	if (lwip_write(sd, buf, hlen) != hlen) {
#if HTTP_SERVER_DEBUG==1
		xil_printf("error writing http header to socket\r\n");
		xil_printf("http header = %s\r\n", buf);
#endif
		return -1;
	}

	/* now write the file */
	while (fsize) {
		int w;
		f_read(&fp, &buf, BUFSIZE, &n);

		if ((w = lwip_write(sd, buf, n)) != n) {
#if HTTP_SERVER_DEBUG==1
			xil_printf("error writing file (%s) to socket, remaining unwritten bytes = %d\r\n", filename, fsize - n);
			xil_printf("attempted to lwip_write %d bytes, actual bytes written = %d\r\n", n, w);
#endif
			break;
		}

		fsize -= n;
	}
	f_close(&fp);

	return 0;
}

enum http_req_type { HTTP_GET, HTTP_POST, HTTP_UNKNOWN };
enum http_req_type decode_http_request(char *req, int l)
{
	char *get_str = "GET";
	char *post_str = "POST";

	if (!strncmp(req, get_str, strlen(get_str)))
		return HTTP_GET;

	if (!strncmp(req, post_str, strlen(post_str)))
		return HTTP_POST;

	return HTTP_UNKNOWN;
}

/* generate and write out an appropriate response for the http request */
int generate_response(int sd, char *http_req, int http_req_len)
{
	enum http_req_type request_type = decode_http_request(http_req, http_req_len);

	switch(request_type) {
	case HTTP_GET:
		return do_http_get(sd, http_req, http_req_len);
	case HTTP_POST:
		return do_http_post(sd, http_req, http_req_len);
	default:
		return do_404(sd, http_req, http_req_len);
	}
}

#undef EXIT
