/*
 * fretz_server.c
 *
 *  Created on: Nov 3, 2020
 *      Author: AntGeorge
 */

#include <stdio.h>
#include <string.h>
#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#endif
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwipopts.h"

#include "config_apps.h"
#ifdef __arm__
#include "FreeRTOS.h"
#include "task.h"
#include "xil_printf.h"
#endif

#include "utils/fretz_protocol/protocol/protocol.h"
#include "utils/utils.h"

static unsigned fretz_port = 7000;

void process_fretz_request(int sd)
{

	int read_len, payload_len, response_len = 0;
	char *recv_buf, *response_buf;
	header_t header_buf;

	while(1) {

		/* read the header of the command (first *PARAMS_p* bytes) */
		if ((read_len = read(sd, &header_buf, sizeof(header_t))) <= 0) {
			close(sd);
			goto exit;
		}

#if FRETZ_SERVER_DEBUG==1
		xil_printf("Fretz: received command %d\r\n", header_buf.CMD);
#endif

#if FRETZ_SERVER_TIME == 1
		XTime tStart, tEnd;
		XTime_GetTime(&tStart);
#endif

		payload_len = byte_swap16(header_buf.LEN);

		recv_buf = NULL;

		if (payload_len > 0) {

			int total_read_len = 0;

			recv_buf = (char *) pvPortMalloc(payload_len);

			while (total_read_len < payload_len) {
				/* next read the parameters and data of the command */
				if ((read_len = read(sd, recv_buf + total_read_len, payload_len - total_read_len)) < 0) {
					close(sd);
					goto exit;
				}
				total_read_len += read_len;
			}
		}

		// response_buf is NULL if command is unknown
		execute(header_buf, recv_buf, &response_buf, &response_len);

		vPortFree(recv_buf);

		if (response_buf != NULL) {

			int total_response_len = 0, resp_len = 0;

			while (total_response_len < response_len) {
				/* next read the parameters and data of the command */
				if ((resp_len = write(sd, response_buf + total_response_len, response_len - total_response_len)) < 0) {
					xil_printf("Fretz: error writing fretz response to socket\r\n");
					xil_printf("Fretz: response data size = %d\r\n", response_len);
					vPortFree(response_buf);

#if FRETZ_SERVER_TIME == 1
					XTime_GetTime(&tEnd);
					int t = (tEnd - tStart) / (COUNTS_PER_SECOND / (double) 1000000);
					xil_printf("Fretz: Time to execute fretz command: %u microseconds\r\n", t);
#endif

					return;
				}
				total_response_len += resp_len;
			}

			vPortFree(response_buf);
		}

	}

	exit:
		xil_printf("Fretz: Connection closed\n\r");
#ifdef OS_IS_FREERTOS
		vTaskDelete(NULL);
#endif
		return;
}

int fretz_server_thread()
{
	int sock, new_sd;
	struct sockaddr_in address, remote;
	int size;

	/* create a TCP socket */
	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
#ifdef OS_IS_FREERTOS
	vTaskDelete(NULL);
#endif
		return -1;
	}

	/* bind to port *fretz_port* at any interface */
	address.sin_family = AF_INET;
	address.sin_port = htons(fretz_port);
	address.sin_addr.s_addr = INADDR_ANY;
	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0) {
#ifdef OS_IS_FREERTOS
	vTaskDelete(NULL);
#endif
		return -1;
	}

	/* listen for incoming connections */
	lwip_listen(sock, 0);

	size = sizeof(remote);
	while (1) {
		new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
		/* spawn a separate handler for each connection */
		sys_thread_new("httpd", (void(*)(void*))process_fretz_request, (void*)new_sd,
                        THREAD_STACKSIZE,
                        DEFAULT_THREAD_PRIO);
	}
	return 0;
}

void print_fretz_app_header()
{
    xil_printf("%20s %6d %s\r\n", "Fretz server", fretz_port, "Connect fretz on address 192.168.1.10");
}
