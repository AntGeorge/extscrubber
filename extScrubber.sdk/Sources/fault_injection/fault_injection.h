/** @defgroup fault_injection Fault injection task
 *
 * fault_injection.h
 *
 * @date 21 May 2020
 * @author AntGeorge
 *
 * @details
 *		TBD
 *
 *	@{
 */

#ifndef SRC_FAULT_INJECTION_FAULT_INJECTION_H_
#define SRC_FAULT_INJECTION_FAULT_INJECTION_H_

#include "FreeRTOS.h"
#include "queue.h"

#include "utils/jtag/jtag.h"
#include "platform/platform.h"

#define MAX_PATTERNS 10
#define SBU_PATTERN 0x12000001

QueueHandle_t finj_qr;
QueueHandle_t finj_qt;

extern jtag_t jtag_objects[ACTIVE_PORTS];
extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];

struct injections_s {
	uint32_t frame;
	uint32_t bit;
	uint32_t pattern;
};

enum INJECTION_STATUS_e {
	INJST_OK = 0,
	INJST_FRAD_ERR	= 1,
	INJST_BIT_ERR = 2
};

void fault_injection_thread(void);

/*
 * If the return_injections is equal to 1 the caller must free the returned_injections buffer.
 * Otherwise if the return_injections is 0 no action is required.
 */
enum INJECTION_STATUS_e fault_injection(jtag_t *jtag_object,
		uint32_t from_frad, uint32_t to_frad, uint32_t from_bit, uint32_t to_bit,
		uint32_t flips, uint32_t iterations, uint32_t delay, uint8_t type, uint32_t patterns[MAX_PATTERNS], uint32_t num_of_patterns,
		struct injections_s **returned_injections, uint8_t return_injections, uint32_t *num_of_injections);

#endif /* SRC_FAULT_INJECTION_FAULT_INJECTION_H_ */

/** @}*/
