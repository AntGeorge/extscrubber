/*
 * fault_injection.c
 *
 *  Created on: 21 May 2020
 *      Author: Ageor
 */

#include <stdio.h>
#include <math.h>
#include <utils/jtag/devices/common.h>
#include "fault_injection.h"

#include "platform/platform.h"

#include "utils/jtag/jtag.h"
#include "utils/utils.h"
#include "utils/tiny-json/tiny-json.h"
#include "utils/json-maker/json-maker.h"

#include "xparameters.h"
#include "xil_printf.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "config_apps.h"

static jtag_t *JtagObj;
static SemaphoreHandle_t *mutex;
static char *raw_ret;

#define MALLOC_SIZE 100

#define getPatternSizeX(P) ((P >> 28) & 0x7)
#define getPatternSizeY(P) ((P >> 25) & 0x7)

#define getPatternCol(P, C) ((P >> (C * 5)) & 0x1F)

#define EXIT(MSG) 										\
	raw_ret = (char *) pvPortMalloc(sizeof(MSG) + 1);	\
	sprintf(raw_ret, MSG);								\
	goto exit;

// Allocates an integer to variable named VALUE or goes to exit;
#define getIntOrEXIT(S, FIELD, VALUE)									\
({																		\
	json_t const *f = json_getProperty(s, FIELD);						\
																		\
	if (f == NULL) {													\
		EXIT("{ \"return\": \"Undefined '" FIELD "' field\"}");			\
	}																	\
																		\
	if (json_getType(f) != JSON_INTEGER) {								\
		EXIT("{ \"return\": \"Wrong datatype in '" FIELD "' field\"}"); \
	}																	\
	VALUE = json_getInteger(f);											\
})


#define getIntFromStringOrEXIT(S, FIELD, VALUE)							\
({																		\
	json_t const *f = json_getProperty(s, FIELD);						\
																		\
	if (f == NULL) {													\
		EXIT("{ \"return\": \"Undefined '" FIELD "' field\"}");			\
	}																	\
																		\
	if (json_getType(f) != JSON_TEXT) {									\
		EXIT("{ \"return\": \"Wrong datatype in '" FIELD "' field\"}"); \
	}																	\
																		\
	if (hexString2Int(json_getValue(f), '\0', &VALUE)) {					\
		EXIT("{ \"return\": \"Wrong '" FIELD "' value\"}");				\
	}																	\
})

static int injections_to_json(char *dest, struct injections_s *inj, int c)
{
	char hex[9];

	char *p = dest;
	p = json_objOpen(p, NULL);
	p = json_arrOpen(p, "injections");

	for (int i = 0; i < c; i++) {

		for (int j = 0; j < getPatternSizeX(inj[i].pattern); j++) {

			sprintf(hex, "%x", (unsigned int) inj[i].frame + j);

			int c = getPatternCol(inj[i].pattern, j);

			for (int k = 0; k < 5; k++, c>>=1) {
				if ((c & 0x1) == 1) {
					p = json_objOpen(p, NULL);
					p = json_str(p, "frame", hex);
					p = json_int(p, "bit", inj[i].bit + k);
					p = json_objClose(p);
				}
			}
		}
	}

	p = json_arrClose(p);
	p = json_objClose(p);
	p = json_end(p);
	return p - dest;
}

static int collisionExists(struct injections_s *inj, uint32_t rf, uint32_t rb, uint32_t rp, uint32_t f)
{
	for (int i=0; i < f; i++) {
		if (inj[i].frame < rf + getPatternSizeX(rp) &&
			inj[i].frame + getPatternSizeX(inj[i].pattern) > rf &&
			inj[i].bit < rb + getPatternSizeY(rp) &&
			inj[i].bit + getPatternSizeY(inj[i].pattern) > rb) {
			return 1;
		}
	}
	return 0;
}

static void calculatePatternSize(uint32_t *p)
{
	int i;
	for (i = 0; i < 5; i++) {
		// Calculate size Y
		if ((*p >> i) & 0x108421) {
			*p &= ~(0x7 << 25);
			*p |= (i+1) << 25;
		}
		// Calculate size X
		if ((*p >> i*5) & 0x1F) {
			*p &= ~(0x7 << 28);
			*p |= (i+1) << 28;
		}
	}
}

static int isNull(const json_t *j)
{
	return	j == NULL ||
			strcasecmp(json_getValue(j), "null") == 0 ||
			strcasecmp(json_getValue(j), "") == 0;
}

enum INJECTION_STATUS_e fault_injection(jtag_t *jtag_object,
		uint32_t from_frad, uint32_t to_frad, uint32_t from_bit, uint32_t to_bit,
		uint32_t flips, uint32_t iterations, uint32_t delay, uint8_t type, uint32_t patterns[MAX_PATTERNS], uint32_t num_of_patterns,
		struct injections_s **returned_injections, uint8_t return_injections, uint32_t *num_of_injections) {

	// Accept zero value on to_frad & to_bit.
	if (to_frad == 0) to_frad = from_frad;
	if (to_bit == 0) to_bit = from_bit;

	int num_of_frames = to_frad - from_frad + 1;
	int num_of_bits = to_bit - from_bit + 1;

	*num_of_injections = 0;

	if (num_of_frames <= 0) {

		return INJST_FRAD_ERR;
	}

	if (num_of_bits <= 0)
	{
		return INJST_BIT_ERR;
	}

	frame_t f;

	// Do 'flips' number of bit flips
	if (flips > 0) {

		struct injections_s *injections = NULL;

		if (return_injections != 0)
			injections = pvPortMalloc(sizeof(struct injections_s) * flips * iterations);

		uint32_t rand_frame, rand_bit, rand_pattern;

		unsigned int seed = from_frad ^ to_frad ^ from_bit ^ to_bit ^ flips ^ iterations ^ delay;

		int iter, i;
		for (iter = 0; iter < iterations; iter++) {

			i = iter * flips;
			while (i < (iter + 1) * flips) {
				rand_frame = (rand_r(&seed) % num_of_frames) + from_frad;
				rand_bit = (rand_r(&seed) % num_of_bits) + from_bit;
				rand_pattern = patterns[(rand_r(&seed) % num_of_patterns)];

				if (to_frad - rand_frame < getPatternSizeX(rand_pattern) - 1) {
					rand_frame -= getPatternSizeX(rand_pattern) - (to_frad - rand_frame + 1);
				}

				if (to_bit - rand_bit < getPatternSizeY(rand_pattern) - 1) {
					rand_bit -= getPatternSizeY(rand_pattern) - (to_bit - rand_bit + 1);
				}

				if (collisionExists(injections, rand_frame, rand_bit, rand_pattern, i)) {
					continue;
				}

				if (injections != NULL) {
					injections[i].frame = rand_frame;
					injections[i].bit = rand_bit;
					injections[i].pattern = rand_pattern;
				}

#if DEBUG_FINJECTION==1
				if (rand_pattern == SBU_PATTERN) {
					xil_printf("Fault Injection: inject SBU in frame %08x bit %d \r\n", rand_frame, rand_bit);
				} else {
					xil_printf("Fault Injection: inject MBU %08x in frame %08x bit %d \r\n", rand_pattern, rand_frame, rand_bit);
				}
#endif

				for (int j = 0; j < getPatternSizeX(rand_pattern); j++) {

					read_frames(jtag_object, rand_frame+j, 1, &f, type);

					f.words[rand_bit / 32] ^= (getPatternCol(rand_pattern, j) << (rand_bit % 32));

					if ((rand_bit % 32) + getPatternSizeY(rand_pattern) > 32)
						f.words[(rand_bit / 32) + 1] ^= (getPatternCol(rand_pattern, j) >> (32 - rand_bit));

					write_frames(jtag_object, rand_frame+j, 1, &f, type);

				}

				i++;
			}

			vTaskDelay(delay);
		}

		*returned_injections = injections;
		*num_of_injections = i;

		return INJST_OK;

	// Flip all bits on the frame range
	} else {

		for (int i = from_frad; i <= to_frad; i++) {

			read_frames(jtag_object, i, 1, &f, type);

			for (int j = from_bit; j <= to_bit; j++) {
				f.words[j / 32] ^= (0x1 << (j % 32));
			}

			write_frames(jtag_object, i, 1, &f, type);
		}

		return INJST_OK;
	}
}

// raw_ret allocations must be freed from webserver task.
void fault_injection_thread(void)
{

	json_t *s;

	for(;;) {

		if(xQueueReceive(finj_qr, &s, portMAX_DELAY) == pdPASS) {
			xil_printf("finj module is executing a command\r\n");

			char const *command_v = json_getPropertyValue(s, "command");

			mutex = NULL;

			if (command_v == 0 || strcasecmp(command_v, "null") == 0) {

				EXIT("{ \"return\": \"Undefined or null 'command' field\"}");
			}

			char const *port_v = json_getPropertyValue(s, "port");

			if (port_v == 0 || strcasecmp(port_v, "null") == 0) {

				EXIT("{\"return\": \"Undefined or null 'port' field\"}");

			} else {
				for (int i = 0; i < ACTIVE_PORTS; i++) {
					if (strcmp(port_v, jtag_objects[i].name) == 0) {
						JtagObj = &jtag_objects[i];
						mutex = &jtag_mutex[i];
					}
				}
			}

			// If provided port is not in the jtag_objects
			if (mutex == NULL) {
				EXIT("{\"return\": \"Port name is invalid\"}");
			}

			xSemaphoreTake(*mutex, portMAX_DELAY);

			if (strcmp(command_v, "read_jreg") == 0) {

				uint32_t reg_v, n_v;

				getIntOrEXIT(s, "n", n_v);
				getIntFromStringOrEXIT(s, "reg", reg_v);

				int words_to_read = ceil((double)n_v/32);

				uint32_t b[words_to_read];

				read_jreg(JtagObj, reg_v, n_v, b);

				// This allocation must be freed from webserver task.
				raw_ret = (char *) pvPortMalloc(sizeof(b)*2 + (words_to_read)*3 + 50); // Add some additional space for json syntax and commas.

				char tmp[sizeof(b[0])+1];

				sprintf(raw_ret, "{\"return\":[\"%x\"", (unsigned int)b[0]);

				for (int i = 1; i < words_to_read; i++) {
					sprintf(tmp, ",\"%x\"", (unsigned int) b[i]);
					strcat(raw_ret, tmp);
				}

				strcat(raw_ret, "]}");

			} else if (strcmp(command_v, "write_jreg") == 0) {

				json_t const *data = json_getProperty(s, "data");

				if (data == NULL) {

					EXIT("{ \"return\": \"Undefined 'data' field\"}");
				}

				uint32_t reg_v, n_v;

				getIntFromStringOrEXIT(s, "reg", reg_v);
				getIntOrEXIT(s, "n", n_v);

				int words_to_write = ceil((double)n_v/32);

				uint32_t data_v[words_to_write];

				int i=0;
				for (data = json_getChild(data); data != 0; data = json_getSibling(data)) {
					if (JSON_TEXT == json_getType(data)) {
						if (hexString2Int(json_getValue(data), '\0', &data_v[i++])) {

							EXIT("{ \"return\": \"Wrong 'data' value\"}");
						}
					} else {

						EXIT("{ \"return\": \"Wrong datatype in 'data' field\"}");
					}
				}

				write_jreg(JtagObj, reg_v, n_v, data_v);

				EXIT("{ \"return\": \"OK\"}");

			} else if (strcmp(command_v, "read_creg") == 0) {

				uint32_t reg_v;

				getIntFromStringOrEXIT(s, "reg", reg_v);

				uint32_t b;

				read_creg(JtagObj, reg_v, &b);

				raw_ret = (char *) pvPortMalloc(MALLOC_SIZE);

				sprintf(raw_ret, "{\"return\":\"%x\"}", (unsigned int)b);

			} else if (strcmp(command_v, "write_creg") == 0) {

				uint32_t reg_v, data_v;

				getIntFromStringOrEXIT(s, "reg", reg_v);
				getIntFromStringOrEXIT(s, "data", data_v);

				write_creg(JtagObj, reg_v, data_v);

				EXIT("{\"return\":\"OK\"}");

			} else if (strcmp(command_v, "read_frames") == 0) {

				uint32_t frad_v, n_v, type_v;

				getIntOrEXIT(s, "n", n_v);
				getIntOrEXIT(s, "type", type_v);
				getIntFromStringOrEXIT(s, "frad", frad_v);

				frame_t *f = pvPortMalloc(n_v * sizeof(frame_t));

				read_frames(JtagObj, frad_v, n_v, f, type_v);

				/*
				 * We need space for:
				 * (a) data for n frames (n_v * WPF * 8).
				 * (b) 50 characters for the extras like "address" and "data" words (n_v * 50).
				 * (c) 3 characters, 2 quotes and a comma, for each word in a frame (n_v * WPF * 3).
				 *
				 */
				raw_ret = (char *) pvPortMalloc(n_v * JtagObj->device.wpf * 8 + n_v * 50 + n_v * JtagObj->device.wpf * 3);

				sprintf(raw_ret, "{\"return\":[");

				char tmp[50];

				for (int i = 0; i < n_v; i++) {

					sprintf(tmp, "[\"address\":\"%x\",", (unsigned int)f[i].address);
					strcat(raw_ret, tmp);

					strcat(raw_ret, "\"data\":[");

					sprintf(tmp, "\"%x\"", (unsigned int) f[i].words[0]);
					strcat(raw_ret, tmp);

					for (int j = 1; j < JtagObj->device.wpf; j++) {

						sprintf(tmp, ",\"%x\"", (unsigned int) f[i].words[j]);
						strcat(raw_ret, tmp);
					}

					strcat(raw_ret, "]],");
				}

				// Remove the last comma
				raw_ret[strlen(raw_ret)-1] = 0;

				strcat(raw_ret, "]}");

				vPortFree(f);

			} else if (strcmp(command_v, "write_frames") == 0) {

				json_t const *data = json_getProperty(s, "data");

				if (data == NULL) {

					EXIT("{ \"return\": \"Undefined 'data' field\"}");
				}

				uint32_t frad_v, n_v;

				getIntOrEXIT(s, "n", n_v);
				getIntFromStringOrEXIT(s, "frad", frad_v);

				frame_t *f = pvPortMalloc(n_v * sizeof(frame_t));

				raw_ret = (char *) pvPortMalloc(MALLOC_SIZE);

				json_t const *sublist;

				int i=0, j;
				for(data = json_getChild(data); data != 0; data = json_getSibling(data)) {
					if (JSON_ARRAY == json_getType(data)) {
						sublist = data;
						j=0;
						for(sublist = json_getChild(sublist); sublist != 0; sublist = json_getSibling(sublist)) {
							if (hexString2Int(json_getValue(sublist), '\0', &f[i].words[j++]) ||
									JSON_TEXT != json_getType(sublist)) {

								EXIT("{ \"return\": \"Wrong 'data' value\"}");
							}
						}
					} else {

						EXIT("{ \"return\": \"Wrong 'data' value\"}");
					}
					i++;
				}

				// Check if data length is wrong.
				if (i < n_v || j < JtagObj->device.wpf) {
					sprintf(raw_ret, "{\"return\":\"Wrong data length\"}");
				} else {

					write_frames(JtagObj, frad_v, n_v, f, 0);
					sprintf(raw_ret, "{\"return\":\"OK\"}");
				}

			} else if (strcmp(command_v, "injection") == 0) {

				json_t const *from_frad = json_getProperty(s, "from_frad");
				json_t const *to_frad = json_getProperty(s, "to_frad");
				json_t const *from_bit = json_getProperty(s, "from_bit");
				json_t const *to_bit = json_getProperty(s, "to_bit");

				json_t const *flips = json_getProperty(s, "flips");
				json_t const *iterations = json_getProperty(s, "iterations");
				json_t const *delay = json_getProperty(s, "delay");

				json_t const *type = json_getProperty(s, "type");

				json_t const *patterns = json_getProperty(s, "patterns");

				if (from_frad == NULL || from_bit == NULL) {

					EXIT("{ \"return\": \"Undefined 'from_frad' or 'from_bit' fields\"}");
				}

				uint32_t from_frad_v, to_frad_v, from_bit_v, to_bit_v,
						flips_v, iterations_v, delay_v, type_v, patterns_v[MAX_PATTERNS];

				if (hexString2Int(json_getValue(from_frad), '\0', &from_frad_v) ||
						json_getType(from_bit) != JSON_INTEGER) {

					EXIT("{ \"return\": \"Wrong datatype in 'from_frad' or 'from_bit' fields\"}");
				}
				from_bit_v = json_getInteger(from_bit);

				if (isNull(to_frad)) {
					to_frad_v = from_frad_v;
				} else {
					if (hexString2Int(json_getValue(to_frad), '\0', &to_frad_v)) {

						EXIT("{ \"return\": \"Wrong 'to_frad' value\"}");
					}
				}

				if (isNull(to_bit)) {
					to_bit_v = from_bit_v;
				} else {
					if (json_getType(to_bit) != JSON_INTEGER) {

						EXIT("{ \"return\": \"Wrong 'to_bit' value\"}");
					}
					to_bit_v = json_getInteger(to_bit);
				}

				if (isNull(flips)) {
					flips_v = 1;
				} else {
					if (json_getType(flips) != JSON_INTEGER) {

						EXIT("{ \"return\": \"Wrong 'flips' value\"}");
					}
					flips_v = json_getInteger(flips);
				}

				if (isNull(iterations)) {
					iterations_v = 1;
				} else {
					if (json_getType(iterations) != JSON_INTEGER) {

						EXIT("{ \"return\": \"Wrong 'iterations' value\"}");
					}
					iterations_v = json_getInteger(iterations);
				}

				if (isNull(delay)) {
					delay_v = 0;
				} else {
					if (json_getType(delay) != JSON_INTEGER) {

						EXIT("{ \"return\": \"Wrong 'delay' value\"}");
					}
					delay_v = json_getInteger(delay);
				}

				if (isNull(type)) {
					type_v = 0;
				} else {
					if (json_getType(type) != JSON_INTEGER) {

						EXIT("{ \"return\": \"Wrong 'type' value\"}");
					}
					type_v = json_getInteger(type);
				}

				int num_of_patterns;

				if (patterns == NULL || json_getValue(patterns) == 0) {
					patterns_v[0] = SBU_PATTERN;
					num_of_patterns = 1;
				} else {
					int i=0;
					if (json_getType(patterns) == JSON_ARRAY) {
						for (patterns = json_getChild(patterns); patterns != 0; patterns = json_getSibling(patterns), i++) {
							if (json_getType(patterns) == JSON_TEXT) {
								hexString2Int(json_getValue(patterns), '\0', &patterns_v[i]);
								calculatePatternSize(&patterns_v[i]);
								if (getPatternSizeX(patterns_v[i]) - 1 > to_frad_v - from_frad_v ||
									getPatternSizeY(patterns_v[i]) - 1 > to_bit_v - from_bit_v) {
									EXIT("{ \"return\": \"Some patterns are larger than the defined area\"}");
								}
							} else {
								EXIT("{ \"return\": \"Wrong value in 'patterns' field\"}");
							}
						}
					} else {

						EXIT("{ \"return\": \"Wrong datatype in 'patterns' field\"}");
					}

					num_of_patterns = i;
				}

				struct injections_s *injections;
				uint32_t num_of_injections;

				// Give semaphore because fault_injection
				// manipulates the semaphore (takes/gives) by its own
//				xSemaphoreGive(*mutex);

				int status = fault_injection(JtagObj,
						from_frad_v, to_frad_v, from_bit_v, to_bit_v, flips_v, iterations_v, delay_v, type_v, patterns_v, num_of_patterns,
						&injections, 1, &num_of_injections);


				if (status == INJST_OK) {
					if (num_of_injections > 0) {
						raw_ret = pvPortMalloc(17 * flips_v * iterations_v * 25);
						injections_to_json(raw_ret, injections, num_of_injections);
						vPortFree(injections);
					}
				} else {
					EXIT("{ \"return\": \"'from_frad' must be less than or equal to 'to_frad' value\"}");
				}

			} else {

				EXIT("{ \"return\": \"Unknown command\"}");
			}

		exit:
			if (mutex != NULL)
				xSemaphoreGive(*mutex);
			xQueueSend(finj_qt, &raw_ret, 0);
		}
	}
}

#undef MALLOC_SIZE
#undef EXIT
#undef getIntOrEXIT
#undef getIntFromStringOrEXIT
#undef MAX_PATTERNS
#undef SBU_PATTERN
