
#include <stdio.h>
#include <math.h>

#include "scrubber_fr.h"

#include "utils/utils.h"
#include "utils/OCL/ocl.h"
#include "utils/jtag/jtag.h"
#include "utils/tiny-json/tiny-json.h"
#include "utils/json-maker/json-maker.h"

#include "platform/platform.h"

#include "xil_printf.h"
#include "xil_io.h"
#include "xtime_l.h"
#include "xparameters.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "config_apps.h"

#include "ff.h"

#define EXIT(MSG) 										\
	raw_ret = (char *) pvPortMalloc(sizeof(MSG) + 1);	\
	sprintf(raw_ret, MSG);								\
	goto exit;

static jtag_t *JtagObj;
static SemaphoreHandle_t *mutex;
static frame_t *frameData;
static fifo_entry *entries;

// This is set in init scrubber
static int numberOfFrames = 0;

static void init_scrubber_status()
{
	scrubFrStatus.isEnabled = 0;
	scrubFrStatus.frame_corrections = 0;
	scrubFrStatus.fails_count = 0;
}

static void generate_log_filename(char *filename)
{
	#if SCRUBBER_FR_SDLOGS == 1
		sprintf(filename, "log-%u.txt", get_timestamp());
	#endif
}

static FRESULT logStatus(char *message)
{
	// This function just writes the message to SCRUB_STATUS_LOG file
	FRESULT fr = 0;

#if SCRUBBER_FR_SDLOGS == 1
	static char filename[] = SCRUBBER_FR_STATUS_LOG;

	f_mkdir(SCRUBBER_FR_ROOT);
	f_chdir(SCRUBBER_FR_ROOT);

	FIL fil;

	fr = f_open(&fil, filename, FA_WRITE | FA_OPEN_APPEND);

	if (fr) {

		#if SCRUBBER_FR_DEBUG == 1
			xil_printf("Log failed. FatFs error code %d\r\n", fr);
		#endif

		return fr;
	}

	unsigned int timestamp = get_timestamp();

	f_printf(&fil, "%u %s\n", timestamp, message);
	f_close(&fil);

#endif
	return fr;
}

static FRESULT logCorrection(unsigned int *timestamp, uint32_t frad, uint32_t synd)
{
	enum {
		SUBFOLDER_NAME_SIZE = 10,
		ACTIVE_FILE_NAME_SIZE = 128
	};

	FRESULT fr = 0;

#if SCRUBBER_FR_SDLOGS == 1
	static char active_filename[ACTIVE_FILE_NAME_SIZE];
	static char active_folder[SUBFOLDER_NAME_SIZE];
	static int lines_in_file;

	// Create new file if there is not an active file or has reached the limit of lines in file.
	if (active_filename[0] == '\0' || lines_in_file > SCRUBBER_LOG_MAX_LINES) {
		generate_log_filename(active_filename);
		lines_in_file = 0;
	}

	f_mkdir(SCRUBBER_FR_ROOT);
	f_chdir(SCRUBBER_FR_ROOT);
	f_mkdir(SCRUBBER_FR_LOG_FOLDER);
	f_chdir(SCRUBBER_FR_LOG_FOLDER);

	// Create new folder starting with name 0, if exists create with name 1, .. etc
	if (active_folder[0] == '\0')
	{
		char tmp_folder[SUBFOLDER_NAME_SIZE];
		for (int i = 0; ; i++)
		{
			sprintf(tmp_folder, "%d", i);
			fr = f_mkdir(tmp_folder);
			if (fr == FR_EXIST)
				continue;
			else
				break;
		}
		strncpy(active_folder, tmp_folder, SUBFOLDER_NAME_SIZE);
	}

	// Move to the current active folder
	f_chdir(active_folder);

	FIL fil;

	fr = f_open(&fil, active_filename, FA_WRITE | FA_OPEN_APPEND);

	if (fr)
	{
		#if SCRUBBER_FR_DEBUG == 1
				xil_printf("Log failed. FatFs error code %d\r\n", fr);
		#endif

		#if SCRUBBER_FR_SDLOGS==1
			char tmp[100];
			sprintf(tmp, "Log failed. FatFs error code %d", fr);
			logStatus(tmp);
		#endif

		return fr;
	}

	unsigned int timestamp_tmp = get_timestamp();

	if (timestamp != NULL)
		*timestamp = timestamp_tmp;

	f_printf(&fil, "%u %08lX %08lX\n", timestamp_tmp, frad, synd);
	f_close(&fil);

#endif
	return fr;
}

static int load_frameData()
{
	FIL fd_framedata, fd_framelist;
	FRESULT fr;
	uint32_t frad;
	TCHAR buff[10];
	UINT br;

	f_chdir("/");
	f_mkdir(JTAG_ROOT);
	f_chdir(JTAG_ROOT);

	fr = f_open(&fd_framedata, "frame_data.bit", FA_READ);

	if (fr != FR_OK)
		return -1;

	char filename[100];
	strcpy(filename, JTAG_ROOT);
	strcat(filename, "/");
	strcat(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fd_framelist, filename, FA_READ);

	if (fr) {
		f_close(&fd_framedata);
		f_close(&fd_framelist);
		return -1;
	}

	int i = 0;
	while(1) {
		f_gets(buff, 10, &fd_framelist);

		if (f_eof(&fd_framelist))
			break;

		hexString2Int(buff, '\n', &frad);

		// Read one frame data from file
		fr = f_read(&fd_framedata, frameData[i].words, sizeof(word_t) * JtagObj->device.wpf, &br);

		if (fr) {
			xil_printf("Scrubber Fr: load frame data stopped.\r\n");
			f_close(&fd_framedata);
			f_close(&fd_framelist);
			return -1;
		}

		frameData[i].address = frad;
		i++;
	}

	f_close(&fd_framedata);
	f_close(&fd_framelist);

	return 0;
}

static int scrubStatus_to_json(struct json_out *out)
{
	int bytes = 0;

	bytes += json_printf(out, "{ frame_corrections:%d,", scrubFrStatus.frame_corrections);
	bytes += json_printf(out, "fails_count:%d,", scrubFrStatus.fails_count);
	bytes += json_printf(out, "enabled:%d,", scrubFrStatus.isEnabled);
	bytes += json_printf(out, "status:%d", (uint32_t)scrubFrStatus.statusCode);

	#if HEARTBEAT == 1
		bytes += json_printf(out, ",heartbeat:%d", heartbeat);
	#endif

	bytes += json_printf(out, "}");

	return bytes;
}

static void resetStats()
{
	scrubFrStatus.frame_corrections = 0;
	scrubFrStatus.fails_count = 0;
}

static int get_index_of_frad(word_t frad)
{
	int index = 0;

	for (index = 0; index < numberOfFrames; index++)
	{
		if (frameData[index].address == frad)
			break;
	}

	if (index == numberOfFrames)
		return -1;

	return index;
}

static int get_number_of_frames(jtag_t *JtagObj)
{
	FIL fp;
	FRESULT fr;
	int numberOfFrames = 0;

	f_chdir(JTAG_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fp, filename, FA_READ);

	if (fr != FR_OK) {
#if SCRUBBER_FR_DEBUG==1
		xil_printf("Scrubber Fr: couldn't find %s to get the number of frames\r\n", filename);
#endif
		f_close(&fp);
		return -1;
	}

	TCHAR buff[10];

	while(f_gets(buff, 10, &fp)) {
		numberOfFrames++;
	}

	f_close(&fp);

	return numberOfFrames;
}

static int check_FIFO()
{
	int index;
	int valid_entries;
	int status;

	// Get FIFO entry
	status = read_FIFO(JtagObj, &entries[0]);

	// If read_FIFO failed due to DONE pin not active
	if (status) {
		#if SCRUBBER_FR_SDLOGS==1
			logStatus("Scrubber Fr: reset asserted");
		#endif

		return status;
	}

	if (entries[0].fifo_empty)
		return 0;

	valid_entries = 1;

	// Read all OCL FIFO entries until tha same address or OCL_FIFO_SIZE has been read
	for (int i = 1; i < OCL_FIFO_SIZE; i++)
	{
		// Get FIFO entry
		status = read_FIFO(JtagObj, &entries[i]);

		// If read_FIFO failed due to DONE pin not active
		if (status) {
			#if SCRUBBER_FR_SDLOGS==1
				logStatus("Scrubber Fr: reset asserted");
			#endif

			return status;
		}

		if (entries[i].frad == entries[0].frad ||
			entries[i].fifo_empty)
			break;

		valid_entries++;
	}

	// Correct all frames on entries
	for (int i = 0; i < valid_entries; i++)
	{
		// If there is an entry then search for the frame in frameData
		index = get_index_of_frad(entries[i].frad);

		// If frame address could not be located on the frad list (this should never happen)
		if (index == -1)
		{
			scrubFrStatus.fails_count++;
			#if SCRUBBER_FR_SDLOGS == 1
				char *msg = pvPortMalloc(200);
				sprintf(msg, "Scrubber Fr failed to correct frame %02X", entries[i].frad);
				logStatus(msg);
				vPortFree(msg);
			#endif

			scrubFrStatus.isEnabled = 0;
			scrubFrStatus.statusCode = SCRB_STOPPED;

			return -1;
		}

		// Write the frame back
		write_frames(JtagObj, frameData[index].address, 1, &frameData[index], 0);

		// Log correction
		logCorrection(NULL, entries[i].frad, entries[i].ecc_syndrome);

		scrubFrStatus.frame_corrections++;
	}

	// This is done to avoid correction of the same error.
	resetFIFO(JtagObj);

	return 0;
}

int init_scrubber_fr()
{
	init_scrubber_status();

	JtagObj = &jtag_objects[0];
	mutex = &jtag_mutex[0];

	// Get the number of frames from the frame address list
	numberOfFrames = get_number_of_frames(JtagObj);

	// Allocate memory for the frame data buffer
	frameData = pvPortMalloc(sizeof(frame_t) * numberOfFrames);

	// Allocate memory for the FIFO entries buffer
	entries = pvPortMalloc(sizeof(fifo_entry) * OCL_FIFO_SIZE);

	xSemaphoreTake(*mutex, portMAX_DELAY);

	// If initialization error occurred
	if (frameData == NULL ||
		entries == NULL	||
		init_frame_data_file(JtagObj) ||
		load_frameData())
	{
		scrubFrStatus.isEnabled = 0;
		scrubFrStatus.statusCode = SCRB_IFAIL;

		#if SCRUBBER_FR_DEBUG==1
			xil_printf("Scrubber Fr task terminated.\r\n");
		#endif

		#if SCRUBBER_FR_SDLOGS==1
			logStatus("Could not allocate buffers.");
			logStatus("Scrubber Fr task terminated.");
		#endif

		vPortFree(frameData);
		vPortFree(entries);

		xSemaphoreGive(*mutex);

		return -1;
	}

	xSemaphoreGive(*mutex);

	return 0;
}

// Scrubber using frame replacement technique. The erroneous frame is detected by the
// OCL (On-Chip-Logic). After the detection the scrubber writes the golden frame back to the device.
void scrubber_fr_thread(void)
{

	json_t *s;

	char *raw_ret;

	int req_wait = 0;

	for(;;) {

		// If the scrubber is disabled then wait until a request is received.
		// There is no need to continue to the loop if scrubber is disabled
		// Only when scrubber is enabled continue the loop.
		if (scrubFrStatus.isEnabled == 0)
			req_wait = portMAX_DELAY;
		else
			req_wait = 0;

		// Response to a queue request.
		if(xQueueReceive(scrbFr_qr, &s, req_wait) == pdPASS) {

			char const *command_v = json_getPropertyValue(s, "command");

			if (command_v == 0) {

				EXIT("{ \"return\": \"Undefined 'command' field\"}");

			} else if (strcmp(command_v, "enable") == 0) {

				json_t const *value = json_getProperty(s, "value");

				if (value == NULL) {

					EXIT("{ \"return\": \"Undefined 'value' field\"}");
				}

				if (json_getType(value) != JSON_INTEGER) {

					if (json_getType(value) != JSON_BOOLEAN) {
						EXIT("{ \"return\": \"Wrong 'value' value\"}");
					} else {
						scrubFrStatus.isEnabled = json_getBoolean(value);
					}

				} else {
					scrubFrStatus.isEnabled = json_getInteger(value);
				}

				raw_ret = (char *) pvPortMalloc(50);

				if (scrubFrStatus.isEnabled)
				{
					char *msg = pvPortMalloc(200);
					sprintf(msg, "Scrubber Fr enabled");
					logStatus(msg);
					vPortFree(msg);

					sprintf(raw_ret, "{ \"return\": \"Scrubber Fr is enabled\"}");
				}
				else
				{
					char *msg = pvPortMalloc(200);
					sprintf(msg, "Scrubber Fr disabled");
					logStatus(msg);
					vPortFree(msg);

					sprintf(raw_ret, "{ \"return\": \"Scrubber Fr is disabled\"}");
				}
			} else if(strcmp(command_v, "status") == 0) {

				int res = create_json(&raw_ret, sizeof(scrubFrStatus), scrubStatus_to_json);
				if (res == -1) {
					EXIT("Not enough memory");
				}
				goto exit;

			} else if (strcmp(command_v, "reset_stats") == 0) {

				resetStats();
				EXIT("{ \"return\": \"OK\"}");

			} else {

				EXIT("{ \"return\": \"Unknown command\"}");
			}

			exit:
				xQueueSend(scrbFr_qt, &raw_ret, 0);

			// Give execution to webserver in order to return the message to user.
			taskYIELD();

			//xil_printf("Free heap: %d\r\n", xPortGetFreeHeapSize());
		}

		if (scrubFrStatus.statusCode != SCRB_OK)
			scrubFrStatus.isEnabled = 0;

		if (!scrubFrStatus.isEnabled)
			continue;

		// Read OCL FIFO for any entry
		// If entry exists, write the golden data on the frame address provided by the OCL FIFO
		xSemaphoreTake(*mutex, portMAX_DELAY);
		check_FIFO();
		xSemaphoreGive(*mutex);
	}
}
