/** @defgroup scrubber Scrubber Task
 *
 * scrubber.h
 *
 * @date 27 January 2020
 * @author AntGeorge
 *
 * @details
 *		This file contains the public functions
 *		to initialize the scrubber. It also has
 *		static functions for each operation in order
 *		to scrubbing the configuration memory of the
 *		connected FPGA.
 *
 *		It uses the @ref jtag library to communicate with
 *		configuration logic of the DUT.
 *
 *		The implemented scrubber is a simple frame replacement, which means
 *		that when an erroneous frame is detected from the OCL the golden data
 *		will be written back to the configuration memory.
 *	@{
 */

#ifndef SRC_SCRUBBER_FR_H_
#define SRC_SCRUBBER_FR_H_

#include "scrubber_types.h"

#include "utils/utils.h"
#include "utils/OCL/ocl.h"
#include "utils/jtag/jtag.h"

#include "platform/platform.h"

#include "config_apps.h"

#include "FreeRTOS.h"
#include "queue.h"

#define DEBUG_TIME	0

QueueHandle_t scrbFr_qr;
QueueHandle_t scrbFr_qt;


struct scrbFrstatus_s {
	SCRBRESULT statusCode;
	uint32_t frame_corrections;
	uint32_t fails_count;
	uint32_t isEnabled: 1;
} scrubFrStatus;

extern jtag_t jtag_objects[ACTIVE_PORTS];
extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];
extern uint32_t heartbeat;

// The scrubber fr task initialization
int init_scrubber_fr();

// The scrubber fr task function
void scrubber_fr_thread(void);


#endif /* SRC_SCRUBBER_FR_H_ */

/** @}*/
