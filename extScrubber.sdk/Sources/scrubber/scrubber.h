/** @defgroup scrubber Scrubber Task
 *
 * scrubber.h
 *
 * @date 27 January 2020
 * @author AntGeorge
 *
 * @details
 *		This file contains the public functions
 *		to initialize the scrubber. It also has
 *		static functions for each operation in order
 *		to scrubbing the configuration memory of the
 *		connected FPGA.
 *
 *		It uses the @ref jtag library to communicate with
 *		configuration logic of the DUT.
 *
 *		The implemented scrubber is the mixed 2D scrubber using parity frames
 *		and the OCL to detect erroneous frames.
 *	@{
 */

#ifndef SRC_SCRUBBER_H_
#define SRC_SCRUBBER_H_

#include "utils/utils.h"
#include "utils/jtag/jtag.h"

#include "platform/platform.h"

#include "config_apps.h"

#include "FreeRTOS.h"
#include "queue.h"

#define DEBUG_TIME	0

QueueHandle_t scrb_qr;
QueueHandle_t scrb_qt;

typedef struct frad_list_s frad_list_t;
struct frad_list_s {
	uint32_t before_addr;	///< Address value before the gap.
	uint32_t after_addr;	///< Address value after the gap.
	frad_list_t *next_gap;	///< The address of the next gap.
};

typedef struct parity_s parity_t;
struct parity_s {
	frame_t parity_frames[SCRUBBER_NUMBER_OF_SUBGROUPS];
};

typedef struct group_list_s group_list_t;
struct group_list_s {
	uint32_t gaddr;				///< Group address, first frad in group
	uint32_t nframes_to_read;	///< Number of frames to read
	group_list_t *next_group;	///< Next group
	parity_t golden_parity;		///< Golden Parity of the group
};

/** Defines a group of #MAX_FRAMES_GROUP frames. */
typedef struct group_s group_t;
struct group_s {
	word_t group_type;					///< Group type based on column type or block type
	word_t num_frames;					///< Number of frames in group
	frame_t frames[SCRUBBER_FRAMES_PER_GROUP];	///< Frames data
};

struct scrbstatus_s {
#if SCRUBBER_DEBUG == 1
	char failed_frames[9][SCRUBBER_MAX_FAILED_LOG];
	char failed_syndromes[9][SCRUBBER_MAX_FAILED_LOG];
	int failed_index;
#endif
	uint32_t frame_corrections;
	uint32_t bit_corrections;
	uint32_t fails_count;
	uint32_t isEnabled: 1;
} scrubStatus;

extern jtag_t jtag_objects[ACTIVE_PORTS];
extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];
extern uint32_t heartbeat;

// The scrubber task initialization
int init_scrubber();

void scrubber_thread(void);
uint32_t next_frad(uint32_t addr);

#endif /* SRC_SCRUBBER_H_ */

/** @}*/
