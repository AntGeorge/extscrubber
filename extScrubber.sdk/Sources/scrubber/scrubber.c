/*
 * scrubber.c
 *
 *  Created on: 27 January 2020
 *      Author: AntGeorge
 */

#include <stdio.h>
#include <math.h>

#include "scrubber.h"

#include "utils/utils.h"
#include "utils/OCL/ocl.h"
#include "utils/jtag/jtag.h"
#include "utils/tiny-json/tiny-json.h"
#include "utils/json-maker/json-maker.h"

#include "platform/platform.h"

#include "xil_printf.h"
#include "xil_io.h"
#include "xtime_l.h"
#include "xparameters.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "config_apps.h"

#include "ff.h"

#define EXIT(MSG) 										\
	raw_ret = (char *) pvPortMalloc(sizeof(MSG) + 1);	\
	sprintf(raw_ret, MSG);								\
	goto exit;

static group_t *group_buffer;
static parity_t *parity_buffer;

static frad_list_t *fradlist;
static group_list_t *grouplist;

static uint32_t (*syndromes_buffer)[OCL_FIFO_SIZE];
static uint32_t (*frads_buffer)[OCL_FIFO_SIZE];

static jtag_t *JtagObj;
static SemaphoreHandle_t *mutex;


static void generate_log_filename(char *filename)
{
	#if SCRUBBER_SDLOGS == 1
		sprintf(filename, "log-%u.txt", get_timestamp());
	#endif
}

#if SCRUBBER_SDLOGS_FRAMEDATA == 1
static void generate_logframedata_filename(char *filename, unsigned int timestamp, uint32_t frad)
{
	sprintf(filename, "log_framedata-%u_%08lX.bin", timestamp, frad);
}
#endif

static FRESULT logStatus(char *message)
{
	// This function just writes the message to SCRUB_STATUS_LOG file
	FRESULT fr = 0;

#if SCRUBBER_SDLOGS == 1
	static char filename[] = SCRUBBER_STATUS_LOG;

	f_mkdir(SCRUBBER_ROOT);
	f_chdir(SCRUBBER_ROOT);

	FIL fil;

	fr = f_open(&fil, filename, FA_WRITE | FA_OPEN_APPEND);

	if (fr) {

		#if SCRUBBER_DEBUG == 1
			xil_printf("Log failed. FatFs error code %d\r\n", fr);
		#endif

		return fr;
	}

	unsigned int timestamp = get_timestamp();

	f_printf(&fil, "%u %s\n", timestamp, message);
	f_close(&fil);

#endif
	return fr;
}

static FRESULT logFrameData(unsigned int timestamp, frame_t *frame)
{
	FRESULT fr = 0;

#if SCRUBBER_SDLOGS_FRAMEDATA == 1
	f_mkdir(SCRUBBER_ROOT);
	f_chdir(SCRUBBER_ROOT);
	f_mkdir(SCRUBBER_LOG_FRAMEDATA_FOLDER);
	f_chdir(SCRUBBER_LOG_FRAMEDATA_FOLDER);

	FIL fil;
	UINT br;

	char filename[128];

	generate_logframedata_filename(filename, timestamp, frame->address);

	fr = f_open(&fil, filename, FA_WRITE | FA_CREATE_ALWAYS);

	if (fr) {
#if SCRUBBER_DEBUG == 1
		xil_printf("Framedata log failed. FatFs error code %d\r\n", fr);
#endif
		return fr;
	}

	f_write(&fil, &frame->words, sizeof(word_t) * JtagObj->device.wpf, &br);

	if (br < sizeof(word_t) * JtagObj->device.wpf) {
		xil_printf("Scrubber: log frame data stopped, SD card is full.\r\n");
		f_close(&fil);
		return fr;
	}

	f_close(&fil);

#endif
	return fr;
}

static FRESULT logCorrection(unsigned int *timestamp, uint32_t frad, uint32_t synd, int type)
{
	enum {
		SUBFOLDER_NAME_SIZE = 10,
		ACTIVE_FILE_NAME_SIZE = 128
	};

	FRESULT fr = 0;

#if SCRUBBER_SDLOGS == 1
	static char active_filename[ACTIVE_FILE_NAME_SIZE];
	static char active_folder[SUBFOLDER_NAME_SIZE];
	static int lines_in_file;

	// Create new file if there is not an active file or has reached the limit of lines in file.
	if (active_filename[0] == '\0' || lines_in_file > SCRUBBER_LOG_MAX_LINES) {
		generate_log_filename(active_filename);
		lines_in_file = 0;
	}

	f_mkdir(SCRUBBER_ROOT);
	f_chdir(SCRUBBER_ROOT);
	f_mkdir(SCRUBBER_LOG_FOLDER);
	f_chdir(SCRUBBER_LOG_FOLDER);

	// Create new folder starting with name 0, if exists create with name 1, .. etc
	if (active_folder[0] == '\0')
	{
		char tmp_folder[SUBFOLDER_NAME_SIZE];
		for (int i = 0; ; i++)
		{
			sprintf(tmp_folder, "%d", i);
			fr = f_mkdir(tmp_folder);
			if (fr == FR_EXIST)
				continue;
			else
				break;
		}
		strncpy(active_folder, tmp_folder, SUBFOLDER_NAME_SIZE);
	}

	// Move to the current active folder
	f_chdir(active_folder);

	FIL fil;

	fr = f_open(&fil, active_filename, FA_WRITE | FA_OPEN_APPEND);

	if (fr) {

		#if SCRUBBER_DEBUG == 1
				xil_printf("Log failed. FatFs error code %d\r\n", fr);
		#endif

		#if SCRUBBER_SDLOGS==1
			char tmp[100];
			sprintf(tmp, "Log failed. FatFs error code %d", fr);
			logStatus(tmp);
		#endif

		return fr;
	}

	unsigned int timestamp_tmp = get_timestamp();

	if (timestamp != NULL)
		*timestamp = timestamp_tmp;

	f_printf(&fil, "%u %08lX %08lX %d\n", timestamp_tmp, frad, synd, type);
	f_close(&fil);

#endif
	return fr;
}

static void calculate_parity(group_t *g, parity_t *buff)
{
	int f, w;
	for (w = 0; w < JtagObj->device.wpf; w++) {

		buff->parity_frames[0].words[w] = 0;
		buff->parity_frames[1].words[w] = 0;

		for (f = 0; f < g->num_frames; f+=2) {
			buff->parity_frames[0].words[w] ^= g->frames[f].words[w];
			buff->parity_frames[1].words[w] ^= g->frames[f+1].words[w];
		}
	}
}

static void calculate_parity_differences(parity_t *a, parity_t *b, parity_t *r)
{
	for (int i = 0; i < JtagObj->device.wpf; i++) {
		r->parity_frames[0].words[i] = a->parity_frames[0].words[i] ^ b->parity_frames[0].words[i];
		r->parity_frames[1].words[i] = a->parity_frames[1].words[i] ^ b->parity_frames[1].words[i];
	}
}

static group_list_t *get_group_from_frad(uint32_t f)
{
	frad_t *frad = (frad_t *) &f;
	switch (frad->block_type) {
	case BLOCK_TYPE_CFG:
		frad->minor_addr = 0;
		break;
	case BLOCK_TYPE_CFG_CLB:
	case BLOCK_TYPE_UNKNOWN:
		frad->minor_addr = 0;
		frad->column_addr = 0;
		break;
	default:
		#if SCRUBBER_DEBUG == 1
				xil_printf("Scrubber: get_nframes_from_frad: Unexpected frad\r\n");
		#endif

		#if SCRUBBER_SDLOGS==1
			logStatus("Scrubber: get_nframes_from_frad: Unexpected frad");
		#endif

		return NULL;
	}

	group_list_t *g;
	for (g = grouplist; g != NULL; g = g->next_group) {
		if (g->gaddr == f)
			return g;
	}

	#if SCRUBBER_DEBUG == 1
		xil_printf("Scrubber: get_nframes_from_frad: Couldn't find frad in group list\r\n");
	#endif

	#if SCRUBBER_SDLOGS==1
		logStatus("Scrubber: get_nframes_from_frad: Couldn't find frad in group list");
	#endif

	return NULL;
}

static int get_frame_in_group(uint32_t frame_address, uint32_t *p)
{
	group_list_t * ret = get_group_from_frad(frame_address);

	if (ret == NULL)
		return -1;

	*p = frame_address - ret->gaddr;

	return 0;
}

static uint32_t get_subgroup_from_frad(uint32_t f)
{
	uint32_t p;
	get_frame_in_group(f, &p);
	return p % SCRUBBER_NUMBER_OF_SUBGROUPS;
}

static uint32_t count_ones(uint32_t w)
{
	uint32_t c=0;
	while (w != 0) {
		w &= (w-1);
		c++;
	}
	return c;
}

static int reconfigureFrame(frame_t *f)
{
	FIL fd;
	FRESULT fr;
	UINT br;

	xil_printf("Reconfigure the frame\r\n");

	f_chdir(JTAG_ROOT);

	fr = f_open(&fd, "frame_data.bit", FA_READ);
	if (fr)
		return (int) fr;

	uint32_t frame_index = 0, p;

	group_list_t *gl = get_group_from_frad(f->address);
	group_list_t *g = grouplist;

	while (g != gl) {
		frame_index += g->nframes_to_read;
		g = g->next_group;
	}

	get_frame_in_group(f->address, &p);
	frame_index += p;

	f_lseek(&fd, sizeof(word_t) * JtagObj->device.wpf * frame_index);

	f_read(&fd, &f->words, sizeof(word_t) * JtagObj->device.wpf, &br);

	if (br < sizeof(word_t) * JtagObj->device.wpf) {
		xil_printf("Scrubber: reconfigure Frame stopped, read frame error.\r\n");
		f_close(&fd);
		vTaskDelete(NULL);
	}

	write_frames(JtagObj, f->address, 1, f, 0);

	f_close(&fd);

	return 0;
}

/*
 * Terminology based from paper 2D-EDC-Algorithm.
 *
 * M: Number of erroneous frames.
 * F: List of erroneous frame addresses.
 * S: List of syndromes.
 * B: Golden parity XOR group parity.
 */
static void run_2D_EDC(uint32_t *F, uint32_t M, frame_t *B, uint32_t *S)
{

	if (M == 0) return;

	uint32_t frame_in_group;
	get_frame_in_group(F[0], &frame_in_group);

	int i;

	// If there is only one erroneous frame.
	if (M == 1) {

		for (i = 0; i < JtagObj->device.wpf; i++) {
			group_buffer->frames[frame_in_group].words[i] ^= B->words[i];
			scrubStatus.bit_corrections += count_ones(B->words[i]);
		}

		write_frames(JtagObj, F[0], 1, &group_buffer->frames[frame_in_group], 0);
		logCorrection(NULL, F[0], S[0], 0);
		scrubStatus.frame_corrections++;

	} else {

		uint8_t frame_is_corrected[M];
		for (i = 0; i < M; i++) {
			frame_is_corrected[i] = 0;
		}

		// For every frame...
		for (i = 0; i < M; i++) {

			if (frame_is_corrected[i])
				continue;

			int32_t syndbit, syndword, syndparity;

			extract_syndrome(S[i], &syndbit, &syndword, &syndparity);

			get_frame_in_group(F[i], &frame_in_group);

			// ...if parity matches syndrome.
			if (syndparity == 1 && (B->words[syndword] & (0x1 << syndbit)) != 0) {

				group_buffer->frames[frame_in_group].words[syndword] ^= (0x1 << syndbit);
				scrubStatus.bit_corrections++;

				write_frames(JtagObj, F[i], 1, &group_buffer->frames[frame_in_group], 0);
				logCorrection(NULL, F[i], S[i], 1);
				scrubStatus.frame_corrections++;

			// ...else
			} else if (syndparity == 1) {
				int j;

				bool found_same_synd = false;

				// For every next frame...
				for (j = i+1; j < M; j++) {

					// ...if the 2 frames have the same syndrome.
					if (S[i] == S[j]) {

						uint32_t frame2_in_group;
						get_frame_in_group(F[j], &frame2_in_group);

						group_buffer->frames[frame2_in_group].words[syndword] ^= (0x1 << syndbit);
						scrubStatus.bit_corrections++;

						write_frames(JtagObj, F[j], 1, &group_buffer->frames[frame2_in_group], 0);
						logCorrection(NULL, F[j], S[j], 2);
						scrubStatus.frame_corrections++;

						found_same_synd = true;

						frame_is_corrected[j] = 1;
					}
				}

				if (found_same_synd == true) {
					group_buffer->frames[frame_in_group].words[syndword] ^= (0x1 << syndbit);
					scrubStatus.bit_corrections++;

					write_frames(JtagObj, F[i], 1, &group_buffer->frames[frame_in_group], 0);
					logCorrection(NULL, F[i], S[i], 2);
					scrubStatus.frame_corrections++;

				} else {
					scrubStatus.fails_count++;
					xil_printf("Scrubber: couldn't find frame with same syndrome\r\n");

					unsigned int timestamp;
					logCorrection(&timestamp, F[i], S[i], 3);

					uint32_t frame_in_group_tmp;

					// Save erroneous frames data in sd card.
					for (int t = 0; t < M; t++) {
						get_frame_in_group(F[t], &frame_in_group_tmp);
						logFrameData(timestamp, &group_buffer->frames[frame_in_group_tmp]);
					}

					#if SCRUBBER_DEBUG == 1
						if (scrubStatus.failed_index < SCRUBBER_MAX_FAILED_LOG - 1) {
							scrubStatus.failed_index++;

							char hex1[9], hex2[9];

							sprintf(hex1, "%x", (unsigned int) group_buffer->frames[frame_in_group].address);
							sprintf(hex2, "%x", (unsigned int) S[i]);

							strcpy(scrubStatus.failed_frames[scrubStatus.failed_index], hex1);
							strcpy(scrubStatus.failed_syndromes[scrubStatus.failed_index], hex2);
						}
					#endif

					#if SCRUBBER_SDLOGS==1
						logStatus("Scrubber failed to correct error");
					#endif

					// Currently on unsuccessful correction scrubber task creates the log and disables itself.
					scrubStatus.isEnabled = 0;

//					reconfigureFrame(&group_buffer->frames[frame_in_group]);
				}
			}

		}
	}
}

static int check_FIFO()
{
	int status;

	fifo_entry entry;
	uint32_t subgroup, subgroup_zero;
	uint32_t M[SCRUBBER_NUMBER_OF_SUBGROUPS];

	status = read_FIFO(JtagObj, &entry);

	// If read_FIFO failed due to DONE pin not active
	if (status) {
		#if SCRUBBER_SDLOGS==1
			logStatus("Scrubber: reset asserted");
		#endif

		return status;
	}

	if (entry.fifo_empty)
		return 0;

#if DEBUG_TIME==1
	xil_printf("Scrubbing started\r\n");
	XTime tStart, tEnd,
		  tStartParity, tEndParity,
		  tStart2D_EDC, tEnd2D_EDC,
		  tStartGroupRead, tEndGroupRead,
		  tStartParityDiffs, tEndParityDiffs,
		  tStartREAD_FIFO, tEndREAD_FIFO;
	XTime_GetTime(&tStart);
#endif

	// if CRC only
	if (entry.crc_error && !entry.ecc_error) {
		xil_printf("Scrubber: CRC only\r\n");
		return 0;
	}

	for (int i = 0; i < SCRUBBER_NUMBER_OF_SUBGROUPS; i++) {
		M[i] = 0;
	}

	subgroup = get_subgroup_from_frad(entry.frad);
	subgroup_zero = subgroup;
	syndromes_buffer[subgroup][0] = entry.ecc_syndrome;
	frads_buffer[subgroup][0] = entry.frad;
	M[subgroup]++;

	frad_t * frad_zero = (frad_t *) &frads_buffer[subgroup][0];

#if DEBUG_TIME==1
	XTime_GetTime(&tStartREAD_FIFO);
#endif

	int i;
	for (i = 1; i < OCL_FIFO_SIZE; i++) {

		status = read_FIFO(JtagObj, &entry);

		// If read_FIFO failed due to DONE pin not active
		if (status) {
			#if SCRUBBER_SDLOGS==1
				logStatus("Scrubber: reset asserted");
			#endif

			return status;
		}

		uint32_t t = entry.frad;
		frad_t *frad = (frad_t *) &t;

		// Different group
		if ((frad_zero->block_type	== BLOCK_TYPE_CFG_CLB ||
			frad_zero->block_type	== BLOCK_TYPE_UNKNOWN) &&
			frad_zero->row_addr		!= frad->row_addr)
			break;
		else if (frad_zero->block_type	== BLOCK_TYPE_CFG &&
				 frad_zero->column_addr	!= frad->column_addr)
			break;

		if (entry.fifo_empty)
			break;

		// End Of Device
		if (entry.frad == frads_buffer[subgroup_zero][0])
			break;

		subgroup = get_subgroup_from_frad(entry.frad);
		syndromes_buffer[subgroup][M[subgroup]] = entry.ecc_syndrome;
		frads_buffer[subgroup][M[subgroup]] = entry.frad;
		M[subgroup]++;

		// End Of Device
		if (entry.crc_error)
			break;
	}

	group_list_t *g = get_group_from_frad(frads_buffer[subgroup_zero][0]);

	group_buffer->num_frames = g->nframes_to_read;

#if DEBUG_TIME==1
	XTime_GetTime(&tEndREAD_FIFO);
	XTime_GetTime(&tStartGroupRead);
#endif

	read_frames(JtagObj, g->gaddr, group_buffer->num_frames, group_buffer->frames, 0);

#if DEBUG_TIME==1
	XTime_GetTime(&tEndGroupRead);
	XTime_GetTime(&tStartParity);
#endif

	calculate_parity(group_buffer, parity_buffer);

#if DEBUG_TIME==1
	XTime_GetTime(&tEndParity);
	XTime_GetTime(&tStartParityDiffs);
#endif

	parity_t diffs;

	calculate_parity_differences(parity_buffer, &g->golden_parity, &diffs);

#if DEBUG_TIME==1
	XTime_GetTime(&tEndParityDiffs);
	XTime_GetTime(&tStart2D_EDC);
#endif

	for (i = 0; i < SCRUBBER_NUMBER_OF_SUBGROUPS; i++) {
		run_2D_EDC(frads_buffer[i], M[i], &diffs.parity_frames[i], syndromes_buffer[i]);
	}

#if DEBUG_TIME==1
	XTime_GetTime(&tEnd2D_EDC);
#endif

	resetFIFO(JtagObj);

#if DEBUG_TIME==1

	while(BUSY(Xil_In32(JtagObj->periph_addr + STATUS_OFFSET)));

	XTime_GetTime(&tEnd);

	int t = (tEnd - tStart) / (COUNTS_PER_SECOND / (double) 1000000);
	int tParity = (tEndParity - tStartParity) / (COUNTS_PER_SECOND / (double) 1000000);
	int t2D_EDC = (tEnd2D_EDC - tStart2D_EDC) / (COUNTS_PER_SECOND / (double) 1000000);
	int tGroupRead = (tEndGroupRead - tStartGroupRead) / (COUNTS_PER_SECOND / (double) 1000000);
	int tParityDiffs = (tEndParityDiffs - tStartParityDiffs) / (COUNTS_PER_SECOND / (double) 1000000);
	int tREAD_FIFO = (tEndREAD_FIFO - tStartREAD_FIFO) / (COUNTS_PER_SECOND / (double) 1000000);

	xil_printf("Scrubbing finished\r\n");
	xil_printf("Time to execute read fifo entries: %u microseconds\r\n", tREAD_FIFO);
	xil_printf("Time to execute group read: %u microseconds\r\n", tGroupRead);
	xil_printf("Time to execute parity calculation: %u microseconds\r\n", tParity);
	xil_printf("Time to execute parity diffs calculation: %u microseconds\r\n", tParityDiffs);
	xil_printf("Time to execute 2D EDC algorithm: %u microseconds\r\n", t2D_EDC);
	xil_printf("Total time to execute scrubbing: %u microseconds\r\n", t);
#endif

	return 0;
}

static int init_frad_list()
{
	FIL fp;
	FRESULT fr;

	f_chdir(JTAG_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fp, filename, FA_READ);

	if (fr != FR_OK) {
#if SCRUBBER_DEBUG==1
		xil_printf("Scrubber: couldn't find %s to initialize frad list\r\n", filename);
#endif
		f_close(&fp);
		return -1;
	}

	TCHAR buff[10];

	uint32_t num, prev_addr = 0;
	frad_list_t **prev_next = &fradlist;

	while(f_gets(buff, 10, &fp)) {

		hexString2Int(buff, '\n', &num);

		// prev_addr == num, only for the first 2 lines which have 0.
		if (!(prev_addr == num - 1 || prev_addr == num)) {
			*prev_next = pvPortMalloc(sizeof(frad_list_t));
			(**prev_next).before_addr = prev_addr;
			(**prev_next).after_addr = num;
			(**prev_next).next_gap = NULL;
			prev_next = &(*prev_next)->next_gap;
		}

		prev_addr = num;
	}

	f_close(&fp);

	return 0;
}

static int init_group_list()
{
	FIL fp;
	FRESULT fr;

	f_chdir(JTAG_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fp, filename, FA_READ);

	if (fr != FR_OK) {
		#if SCRUBBER_DEBUG==1
				xil_printf("Scrubber: couldn't find %s to initialize group list\r\n", filename);
		#endif

		#if SCRUBBER_SDLOGS==1
			char tmp[200];
			sprintf(tmp, "Scrubber couldn't find %s to initialize group list", filename);
			logStatus(tmp);
		#endif

		f_close(&fp);
		return -1;
	}

	TCHAR buff[10];

	group_list_t **prev_next_group = &grouplist;

	uint32_t group_addr;
	frad_t *frad;

	while(f_gets(buff, 10, &fp)) {

		hexString2Int(buff, '\n', &group_addr);
		frad = (frad_t *) &group_addr;

		if (frad->block_type != BLOCK_TYPE_CFG 		&&
			frad->block_type != BLOCK_TYPE_CFG_CLB	&&
			frad->block_type != BLOCK_TYPE_UNKNOWN)
			continue;

		*prev_next_group = pvPortMalloc(sizeof(group_list_t));
		(**prev_next_group).gaddr = group_addr;
		(**prev_next_group).nframes_to_read = 1;

		// One group per column
		if (frad->block_type == BLOCK_TYPE_CFG) {

			frad_t *frad_before, *frad_after;
			uint32_t addr_before, addr_after;

			addr_before = group_addr;
			frad_before = (frad_t *) &addr_before;

			while(f_gets(buff, 10, &fp)) {

				hexString2Int(buff, '\n', &addr_after);
				frad_after = (frad_t *) &addr_after;

				if (addr_after == addr_before)
					continue;

				// We jumped in different group/column.
				if (frad_before->column_addr != frad_after->column_addr) {
					break;
				}

				// We are in the same group/column.
				if (frad_before->minor_addr == frad_after->minor_addr - 1)
					(**prev_next_group).nframes_to_read++;

				addr_before = addr_after;
				frad_before = (frad_t *) &addr_before;
			}
		// One group per row
		} else if (frad->block_type == BLOCK_TYPE_CFG_CLB ||
				   frad->block_type == BLOCK_TYPE_UNKNOWN) {

			frad_t *frad_before, *frad_after;
			uint32_t addr_before, addr_after;

			addr_before = group_addr;

			while(f_gets(buff, 10, &fp)) {

				hexString2Int(buff, '\n', &addr_after);
				frad_after = (frad_t *) &addr_after;

				if (addr_after == addr_before)
					continue;

				// We jumped in different group/row.
				if (frad_before->row_addr != frad_after->row_addr) {
					break;
				}

				// We are in the same group/row.
				if (frad_before->row_addr == frad_after->row_addr)
					(**prev_next_group).nframes_to_read++;

				addr_before = addr_after;
				frad_before = (frad_t *) &addr_before;
			}

		}

		// Go back 1 address.
		f_lseek(&fp, f_tell(&fp) - 9);
		prev_next_group = &(*prev_next_group)->next_group;
	}

	f_close(&fp);
	return 0;
}

static int load_group_data(jtag_t *JtagObj, word_t frad, unsigned int n, frame_t buff[])
{
	FIL fp, fp2;
	FRESULT fr, fr2;

	f_chdir("/");
	f_mkdir(JTAG_ROOT);
	f_chdir(JTAG_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fp, filename, FA_READ);

	fr2 = f_open(&fp2, "frame_data.bit", FA_READ);

	if (fr != FR_OK || fr2 != FR_OK) {
		#if SCRUBBER_DEBUG==1
			xil_printf("Scrubber: couldn't find %s to load the frame group\r\n", filename);
		#endif
		f_close(&fp);
		return -1;
	}

	TCHAR tmpbuff[10];
	int group_position = 0;

	uint32_t num;

	// Find index of frad in frame address list file
	while(f_gets(tmpbuff, 10, &fp)) {

		hexString2Int(tmpbuff, '\n', &num);
		if (num == frad) break;
		group_position++;
	}

	f_close(&fp);

	int bytes_in_frame = JtagObj->device.bpf / 8;
	uint bytes_read = 0;

	// Go to group position in frame data file
	f_lseek(&fp2, group_position * bytes_in_frame);

	// Read the group from frame data file
	for (int i = 0; i < n; i++) {
		fr = f_read(&fp2, buff[i].words, bytes_in_frame, &bytes_read);

		if (fr != FR_OK || bytes_read != bytes_in_frame) {
			return -1;
		}

		buff[i].address = frad;
	}

	f_close(&fp2);

	return 0;
}

static int init_golden_parities()
{
	group_list_t *g;
	int result = 0;

	for (g = grouplist; g != NULL; g = g->next_group) {
		read_frames(JtagObj, g->gaddr, g->nframes_to_read, group_buffer->frames, 0);

		// TODO: Fix the performance of the load_group_data,
		// By using the load_group_data the scrubber will initialize the golden parities
		// using the frame data file, this is better than reading the frames from the JTAG (currently this is the way)
		// but this makes the boot very slow.
		//result = load_group_data(JtagObj, g->gaddr, g->nframes_to_read, group_buffer->frames);
		//if (result) return -1;

		group_buffer->num_frames = g->nframes_to_read;
		calculate_parity(group_buffer, &g->golden_parity);
	}

	return result;
}

static void resetStats()
{
	scrubStatus.bit_corrections = 0;
	scrubStatus.frame_corrections = 0;
	scrubStatus.fails_count = 0;
}

uint32_t next_frad(uint32_t addr)
{
	// If fradlist has not been initialised,
	// use addr+1 for the next frad
	if (fradlist == NULL)
		return addr + 1;

	frad_list_t *flp = fradlist;
	while(flp != NULL) {
		if (addr < flp->before_addr) {
			return addr + 1;
		} else if (addr == flp->before_addr) {
			return flp->after_addr;
		}
		flp = flp->next_gap;
	}
	return 0;
}

static void init_scrubber_status()
{
	scrubStatus.isEnabled = 0;
	scrubStatus.frame_corrections = 0;
	scrubStatus.bit_corrections = 0;
	scrubStatus.fails_count = 0;
#if SCRUBBER_DEBUG==1
	scrubStatus.failed_index = -1;
#endif
}

static int scrubStatus_to_json(struct json_out *out)
{
	int bytes = 0;

	bytes += json_printf(out, "{ frame_corrections:%d,", scrubStatus.frame_corrections);
	bytes += json_printf(out, "bit_corrections:%d,", scrubStatus.bit_corrections);
	bytes += json_printf(out, "fails_count:%d,", scrubStatus.fails_count);
	bytes += json_printf(out, "enabled:%d", scrubStatus.isEnabled);

#if HEARTBEAT == 1
	bytes += json_printf(out, ",heartbeat:%d", heartbeat);
#endif

#if SCRUBBER_DEBUG==1
	bytes += json_printf(out, ",failed_frames: [");
	for (int i = 0; i < scrubStatus.failed_index; i++) {
		bytes += json_printf(out, "%Q,", scrubStatus.failed_frames[i]);
	}
	if (scrubStatus.failed_index != -1)
		bytes += json_printf(out, "%Q", scrubStatus.failed_frames[scrubStatus.failed_index]);
	bytes += json_printf(out, "],");

	bytes += json_printf(out, "failed_syndromes: [");
	for (int i = 0; i < scrubStatus.failed_index; i++) {
		bytes += json_printf(out, "%Q,", scrubStatus.failed_syndromes[i]);
	}
	if (scrubStatus.failed_index != -1)
		bytes += json_printf(out, "%Q", scrubStatus.failed_syndromes[scrubStatus.failed_index]);
	bytes += json_printf(out, "]");

	scrubStatus.failed_index = -1;
#endif

	bytes += json_printf(out, "}");

	return bytes;
}

int init_scrubber()
{
	init_scrubber_status();

	JtagObj = &jtag_objects[0];
	mutex = &jtag_mutex[0];

	// Allocate buffers.
	group_buffer =  (group_t *) pvPortMalloc(sizeof(group_t));
	parity_buffer = (parity_t *) pvPortMalloc(sizeof(parity_t));
	frads_buffer = pvPortMalloc(sizeof(uint32_t[SCRUBBER_NUMBER_OF_SUBGROUPS][OCL_FIFO_SIZE]));
	syndromes_buffer = pvPortMalloc(sizeof(uint32_t[SCRUBBER_NUMBER_OF_SUBGROUPS][OCL_FIFO_SIZE]));

	// Check malloc pointers for null
	if (group_buffer == NULL ||
		parity_buffer == NULL ||
		frads_buffer == NULL ||
		syndromes_buffer == NULL)
	{
		#if SCRUBBER_DEBUG==1
			xil_printf("Scrubber task terminated.\r\n");
		#endif

		#if SCRUBBER_SDLOGS==1
			logStatus("Could not allocate buffers.");
			logStatus("Scrubber task terminated.");
		#endif

		vPortFree(group_buffer);
		vPortFree(parity_buffer);
		vPortFree(frads_buffer);
		vPortFree(syndromes_buffer);

		return -1;
	}

	xSemaphoreTake(*mutex, portMAX_DELAY);

	if (init_frad_list() 			  ||
		init_group_list()			  ||
		init_frame_data_file(JtagObj) ||
		init_golden_parities(JtagObj)) {

		#if SCRUBBER_DEBUG==1
			xil_printf("Scrubber task terminated.\r\n");
		#endif

		#if SCRUBBER_SDLOGS==1
			logStatus("Could not initialize frame or group list.");
			logStatus("Scrubber task terminated.");
		#endif

		vPortFree(group_buffer);
		vPortFree(parity_buffer);
		vPortFree(grouplist);
		vPortFree(fradlist);
		vPortFree(frads_buffer);
		vPortFree(syndromes_buffer);

		xSemaphoreGive(*mutex);

		return -1;
	}

	xSemaphoreGive(*mutex);

	return 0;
}

void scrubber_thread(void)
{

	json_t *s;

	char *raw_ret;

	int req_wait = 0;

	for(;;) {

		// If the scrubber is disabled then wait until a request is received.
		// There is no need to continue to the loop if scrubber is disabled
		// Only when scrubber is enabled continue the loop.
		if (scrubStatus.isEnabled == 0)
			req_wait = portMAX_DELAY;
		else
			req_wait = 0;

		// Response to a queue request.
		if(xQueueReceive(scrb_qr, &s, req_wait) == pdPASS) {

			char const *command_v = json_getPropertyValue(s, "command");

			if (command_v == 0) {

				EXIT("{ \"return\": \"Undefined 'command' field\"}");

			} else if (strcmp(command_v, "enable") == 0) {

				json_t const *value = json_getProperty(s, "value");

				if (value == NULL) {

					EXIT("{ \"return\": \"Undefined 'value' field\"}");
				}

				if (json_getType(value) != JSON_INTEGER) {

					if (json_getType(value) != JSON_BOOLEAN) {
						EXIT("{ \"return\": \"Wrong 'value' value\"}");
					} else {
						scrubStatus.isEnabled = json_getBoolean(value);
					}

				} else {
					scrubStatus.isEnabled = json_getInteger(value);
				}

				raw_ret = (char *) pvPortMalloc(50);

				if (scrubStatus.isEnabled)
				{
					char *msg = pvPortMalloc(200);
					sprintf(msg, "Scrubber enabled");
					logStatus(msg);
					vPortFree(msg);

					sprintf(raw_ret, "{ \"return\": \"Scrubber is enabled\"}");
				}
				else
				{
					char *msg = pvPortMalloc(200);
					sprintf(msg, "Scrubber disabled");
					logStatus(msg);
					vPortFree(msg);

					sprintf(raw_ret, "{ \"return\": \"Scrubber is disabled\"}");
				}
			} else if(strcmp(command_v, "status") == 0) {

//				xil_printf("Free heap: %d\r\n", xPortGetFreeHeapSize());

				int res = create_json(&raw_ret, sizeof(scrubStatus), scrubStatus_to_json);
				if (res == -1) {
					EXIT("Not enough memory");
				}
				goto exit;

			} else if (strcmp(command_v, "reset_stats") == 0) {

				resetStats();
				EXIT("{ \"return\": \"OK\"}");

			} else {

				EXIT("{ \"return\": \"Unknown command\"}");
			}

			exit:
				xQueueSend(scrb_qt, &raw_ret, 0);

			// Give execution to webserver in order to return the message to user.
			taskYIELD();
		}

		if (!scrubStatus.isEnabled)
			continue;

		xSemaphoreTake(*mutex, portMAX_DELAY);
		check_FIFO();
		xSemaphoreGive(*mutex);
	}
}

#undef DEBUG_TIME
#undef EXIT
