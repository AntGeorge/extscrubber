
#include <stdio.h>
#include <math.h>

#include "scrubber_blind.h"

#include "utils/utils.h"
#include "utils/OCL/ocl.h"
#include "utils/jtag/jtag.h"
#include "utils/tiny-json/tiny-json.h"
#include "utils/json-maker/json-maker.h"

#include "platform/platform.h"

#include "xil_printf.h"
#include "xil_io.h"
#include "xtime_l.h"
#include "xparameters.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "config_apps.h"

#include "ff.h"

#define EXIT(MSG) 										\
	raw_ret = (char *) pvPortMalloc(sizeof(MSG) + 1);	\
	sprintf(raw_ret, MSG);								\
	goto exit;

static jtag_t *JtagObj;
static SemaphoreHandle_t *mutex;
static frame_t *frameData;

// This is set in init scrubber
static int numberOfFrames = 0;

static void init_scrubber_status()
{
	scrubBlindStatus.isEnabled = 0;
}

static FRESULT logStatus(char *message)
{
	// This function just writes the message to SCRUB_STATUS_LOG file
	FRESULT fr = 0;

#if SCRUBBER_BLIND_SDLOGS == 1
	static char filename[] = SCRUBBER_BLIND_STATUS_LOG;

	f_mkdir(SCRUBBER_BLIND_ROOT);
	f_chdir(SCRUBBER_BLIND_ROOT);

	FIL fil;

	fr = f_open(&fil, filename, FA_WRITE | FA_OPEN_APPEND);

	if (fr) {

		#if SCRUBBER_BLIND_DEBUG == 1
			xil_printf("Log failed. FatFs error code %d\r\n", fr);
		#endif

		return fr;
	}

	unsigned int timestamp = get_timestamp();

	f_printf(&fil, "%u %s\n", timestamp, message);
	f_close(&fil);

#endif
	return fr;
}

static int load_frameData_type0()
{
	FIL fd_framedata, fd_framelist;
	FRESULT fr;
	uint32_t frad;
	TCHAR buff[10];
	UINT br;

	f_chdir("/");
	f_mkdir(SCRUBBER_BLIND_ROOT);
	f_chdir(SCRUBBER_BLIND_ROOT);

	fr = f_open(&fd_framedata, "frame_data_dummy.bit", FA_READ);

	if (fr != FR_OK)
		return -1;

	char filename[100];
	strcpy(filename, SCRUBBER_BLIND_ROOT);
	strcat(filename, "/");
	strcat(filename, JtagObj->name);
	strcat(filename, "_frame_addresses_dummy.txt");

	fr = f_open(&fd_framelist, filename, FA_READ);

	if (fr) {
		f_close(&fd_framedata);
		f_close(&fd_framelist);
		return -1;
	}

	int i = 0;
	while(1) {
		f_gets(buff, 10, &fd_framelist);

		if (f_eof(&fd_framelist))
			break;

		hexString2Int(buff, '\n', &frad);

		// If frad reached the end of type 0 frames
		if ((frad & (0x1 << 23)))
			break;

		// Read one frame data from file
		fr = f_read(&fd_framedata, frameData[i].words, sizeof(word_t) * JtagObj->device.wpf, &br);
		frameData[i].address = frad;

		if (fr) {
			xil_printf("Scrubber Blind: load frame data stopped.\r\n");
			f_close(&fd_framedata);
			f_close(&fd_framelist);
			return -1;
		}

		i++;
	}

	f_close(&fd_framedata);
	f_close(&fd_framelist);

	return 0;
}

static int scrubStatus_to_json(struct json_out *out)
{
	int bytes = 0;

	bytes += json_printf(out, "{ enabled:%d,", scrubBlindStatus.isEnabled);
	bytes += json_printf(out, "status:%d", (uint32_t)scrubBlindStatus.statusCode);
	bytes += json_printf(out, "}");

	return bytes;
}

static int get_number_of_frames_type0(jtag_t *JtagObj)
{
	FIL fp;
	FRESULT fr;
	uint32_t frad;

	f_chdir("/");
	f_chdir(SCRUBBER_BLIND_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses_dummy.txt");

	fr = f_open(&fp, filename, FA_READ);

	if (fr != FR_OK) {
#if SCRUBBER_FR_DEBUG==1
		xil_printf("Scrubber: couldn't find %s to get the number of frames\r\n", filename);
#endif
		f_close(&fp);
		return -1;
	}

	TCHAR buff[10];

	while(f_gets(buff, 10, &fp)) {

		hexString2Int(buff, '\n', &frad);

		// If frad reached the end of type 0 frames
		if ((frad & (0x1 << 23)))
			break;

		numberOfFrames++;
	}

	f_close(&fp);

	return numberOfFrames;
}

static int generate_frame_data_type0(jtag_t *JtagObj)
{
	FIL fd_framedata, fd_framelist;
	FRESULT fr;
	uint32_t frad;
	TCHAR buff[10];
	UINT br;
	frame_t frame;

	// Check if JTAG is initialized
	if (JtagObj->status != JOK)
		return -1;

	f_chdir("/");
	f_mkdir(SCRUBBER_BLIND_ROOT);
	f_chdir(SCRUBBER_BLIND_ROOT);

	fr = f_open(&fd_framedata, "frame_data_dummy.bit", FA_WRITE | FA_CREATE_NEW);

	// If frame data file exists do not overwrite
	if (fr == FR_EXIST)
		return 0;

	char filename[100];
	strcpy(filename, SCRUBBER_BLIND_ROOT);
	strcat(filename, "/");
	strcat(filename, JtagObj->name);
	strcat(filename, "_frame_addresses_dummy.txt");

	fr = f_open(&fd_framelist, filename, FA_READ);

	if (fr) {
		f_close(&fd_framedata);
		f_close(&fd_framelist);
		return -1;
	}

	while(1) {
		f_gets(buff, 10, &fd_framelist);

		if (f_eof(&fd_framelist))
			break;

		hexString2Int(buff, '\n', &frad);

		// If frad reached the end of type 0 frames
		if ((frad & (0x1 << 23)))
			break;

		read_frames(JtagObj, frad, 1, &frame, 0);
		f_write(&fd_framedata, &frame.words, sizeof(word_t) * JtagObj->device.wpf, &br);

		if (br < sizeof(word_t) * JtagObj->device.wpf) {
			xil_printf("Scrubber: init frame data file stopped, SD card is full.\r\n");
			f_close(&fd_framedata);
			f_close(&fd_framelist);
			return -1;
		}
	}

	f_close(&fd_framedata);
	f_close(&fd_framelist);

	return 0;
}

static int create_frad_list_file_dummy(jtag_t *JtagObj)
{
	FIL fp;
	FRESULT fr;

	// Check if JTAG is initialized
	if (JtagObj->status != JOK)
		return -1;

	f_chdir("/");
	f_mkdir(SCRUBBER_BLIND_ROOT);
	f_chdir(SCRUBBER_BLIND_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses_dummy.txt");

	fr = f_open(&fp, filename, FA_WRITE | FA_CREATE_NEW);

	if (fr == FR_EXIST) {
		f_close(&fp);
		return 0;
	}

	word_t far = 0;
	word_t prev_far = -1;

	int isRowBoundary = 0;

	xSemaphoreTake(*mutex, portMAX_DELAY);

	resetTAP(JtagObj);
	cfg_sync(JtagObj);

	// Write FAR with starting FRAD 0.
	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_FAR << 13) | 1);
	shiftDR32(JtagObj, far);

	// While far != Type ! address
	while (!(far & (0x1 << 23))) {

		// Command configuration module to read configuration data
		shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
		shiftDR32(JtagObj, CFG_CMD_RCFG);

		// Command configuration module to read n+2 frames
		shiftDR32(JtagObj, TYPE1 | OP_RD | (CFG_FDRO << 13) | 0);
		shiftDR32(JtagObj, TYPE2 | OP_RD | (((isRowBoundary+2) * JtagObj->device.wpf) + JtagObj->device.pipeline_words));
		shiftDR32(JtagObj, CFG_NOOP);
		shiftDR32(JtagObj, CFG_NOOP);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_OUT);

		// Read first frame, the dummy, this will also make the FAR to auto-increment
		shiftDR(JtagObj, NULL, JtagObj->device.bpf + (JtagObj->device.pipeline_words * JtagObj->device.bpw), 0);

		// If we reached the row boundary then read an extra frame
		if (isRowBoundary == 1)
			shiftDR(JtagObj, NULL, JtagObj->device.bpf, 0);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_IN);

		shiftDR32(JtagObj, TYPE1 | OP_RD | (CFG_FAR << 13) | 1);
		shiftDR32(JtagObj, CFG_NOOP);
		shiftDR32(JtagObj, CFG_NOOP);

		// This will also write 2 times the address on row boundaries
		f_printf(&fp, "%08x\n", far);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_OUT);
		shiftDR(JtagObj, &far, 32, 0);

		if (isRowBoundary)
			f_printf(&fp, "%08x\n", far);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_IN);

		if (prev_far == far) {
			isRowBoundary = 1;
		} else {
			isRowBoundary = 0;
		}

		prev_far = far;
	}

	cfg_desync(JtagObj);

	resetTAP(JtagObj);

	xSemaphoreGive(*mutex);

	f_close(&fp);

	return 0;
}

int init_scrubber_blind()
{
	int result;

	init_scrubber_status();

	JtagObj = &jtag_objects[0];
	mutex = &jtag_mutex[0];

	result = create_frad_list_file_dummy(JtagObj);

	// Get the number of frames from the frame address list
	numberOfFrames = get_number_of_frames_type0(JtagObj);

	// Allocate memory for the frame data buffer
	frameData = pvPortMalloc(sizeof(frame_t) * numberOfFrames);

	xSemaphoreTake(*mutex, portMAX_DELAY);

	// If initialization error occurred
	if (frameData == NULL ||
		result == -1||
		generate_frame_data_type0(JtagObj) ||
		load_frameData_type0())
	{
		scrubBlindStatus.isEnabled = 0;
		scrubBlindStatus.statusCode = SCRB_IFAIL;

		#if SCRUBBER_BLIND_DEBUG==1
			xil_printf("Scrubber Blind task terminated.\r\n");
		#endif

		#if SCRUBBER_BLIND_SDLOGS==1
			logStatus("Could not allocate buffers.");
			logStatus("Scrubber Blind task terminated.");
		#endif

		vPortFree(frameData);

		xSemaphoreGive(*mutex);

		return -1;
	}

	xSemaphoreGive(*mutex);

	return 0;
}

// Scrubber using blind scrubbing technique which just writes the frames back to back.
void scrubber_blind_thread(void)
{

	json_t *s;

	char *raw_ret;

	int req_wait = 0;
	int done_log = 1;

	for(;;) {

		// If the scrubber is disabled then wait until a request is received.
		// There is no need to continue to the loop if scrubber is disabled
		// Only when scrubber is enabled continue the loop.
		if (scrubBlindStatus.isEnabled == 0)
			req_wait = portMAX_DELAY;
		else
			req_wait = 0;

		// Response to a queue request.
		if(xQueueReceive(scrbBlind_qr, &s, req_wait) == pdPASS) {

			char const *command_v = json_getPropertyValue(s, "command");

			if (command_v == 0) {

				EXIT("{ \"return\": \"Undefined 'command' field\"}");

			} else if (strcmp(command_v, "enable") == 0) {

				json_t const *value = json_getProperty(s, "value");

				if (value == NULL) {

					EXIT("{ \"return\": \"Undefined 'value' field\"}");
				}

				if (json_getType(value) != JSON_INTEGER) {

					if (json_getType(value) != JSON_BOOLEAN) {
						EXIT("{ \"return\": \"Wrong 'value' value\"}");
					} else {
						scrubBlindStatus.isEnabled = json_getBoolean(value);
					}

				} else {
					scrubBlindStatus.isEnabled = json_getInteger(value);
				}

				raw_ret = (char *) pvPortMalloc(50);

				if (scrubBlindStatus.isEnabled)
				{
					char *msg = pvPortMalloc(200);
					sprintf(msg, "Scrubber Blind enabled");
					logStatus(msg);
					vPortFree(msg);

					sprintf(raw_ret, "{ \"return\": \"Scrubber Blind is enabled\"}");
				}
				else
				{
					char *msg = pvPortMalloc(200);
					sprintf(msg, "Scrubber Blind disabled");
					logStatus(msg);
					vPortFree(msg);

					sprintf(raw_ret, "{ \"return\": \"Scrubber Blind is disabled\"}");
				}
			} else if(strcmp(command_v, "status") == 0) {

				int res = create_json(&raw_ret, sizeof(scrubBlindStatus), scrubStatus_to_json);
				if (res == -1) {
					EXIT("Not enough memory");
				}
				goto exit;

			} else {

				EXIT("{ \"return\": \"Unknown command\"}");
			}

			exit:
				xQueueSend(scrbBlind_qt, &raw_ret, 0);

			// Give execution to webserver in order to return the message to user.
			taskYIELD();
		}

		if (scrubBlindStatus.statusCode == SCRB_IFAIL)
			scrubBlindStatus.isEnabled = 0;

		if (!scrubBlindStatus.isEnabled)
			continue;

		// Write all configuration frames from address 0, if DONE is active
		if (!is_done_active())
		{
			scrubBlindStatus.statusCode = SCRB_STOPPED;

			if (done_log) {
				logStatus("Done Pin deasserted");
				done_log = 0;
			}

			break;
		} else {
			scrubBlindStatus.statusCode = SCRB_OK;
			done_log = 1;
		}

		xSemaphoreTake(*mutex, portMAX_DELAY);

		write_frames(JtagObj, 0, numberOfFrames, frameData, 0);

		xSemaphoreGive(*mutex);
	}
}
