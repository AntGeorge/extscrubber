# 2D EDC Scrubber Pseudocode
```python
if single erroneous frame:
    correct based on parity (type 0)
else:
    for every frame Fi in erroneous frames:
        if syndrome matches parity:
            corrent based on syndrome (type 1)
        else:
            for every Fj in F j=i+1...M-1:
                if Si = Sj:
                    correct Fj based on syndrome (type 2)
                    found_same_synd = True
            if found_same_synd:
                correct Fi (type 2)
            else:
                unsuccessful correction (type 3)
```

# Notes

1. The logs have the following format.

    timestamp frame_address syndrome type 

2. In case of type 3, frame data of erroneous frames are also saved in sd card under the folder /scrubber/logs_framedata/ with the following format "log_framedata-\<timestamp\>_\<frame address\>.bin".
