/** @defgroup scrubber Scrubber Task
 *
 * scrubber.h
 *
 * @date 12 May 2022
 * @author AntGeorge
 *
 * @details
 *		This file contains the public functions
 *		to initialize the scrubber. It also has
 *		static functions for each operation in order
 *		to scrubbing the configuration memory of the
 *		connected FPGA.
 *
 *		It uses the @ref jtag library to communicate with
 *		configuration logic of the DUT.
 *
 *		The implemented scrubber is a simple blind scrubber.
 *		The frames are written to the configuration memory
 *		with a defined frequency.
 *	@{
 */

#ifndef SRC_SCRUBBER_BLIND_FR_H_
#define SRC_SCRUBBER_BLIND_FR_H_

#include "scrubber_types.h"

#include "utils/utils.h"
#include "utils/OCL/ocl.h"
#include "utils/jtag/jtag.h"

#include "platform/platform.h"

#include "config_apps.h"

#include "FreeRTOS.h"
#include "queue.h"

#define DEBUG_TIME	0

QueueHandle_t scrbBlind_qr;
QueueHandle_t scrbBlind_qt;

struct scrbBlindstatus_s {
	SCRBRESULT statusCode;
	uint32_t isEnabled: 1;
} scrubBlindStatus;

extern jtag_t jtag_objects[ACTIVE_PORTS];
extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];

// The scrubber blind task initialization
int init_scrubber_blind();

// The scrubber blind task function
void scrubber_blind_thread(void);


#endif /* SRC_SCRUBBER_BLIND_FR_H_ */

/** @}*/
