/** @defgroup scrubber Scrubber Types
 *
 * scrubber_types.h
 *
 * @date 12 May 2022
 * @author AntGeorge
 *
 * @details
 * 		In this file the common types used by the scrubbers
 * 		are defined.
 *	@{
 */

#ifndef SRC_SCRUBBER_TYPES_H_
#define SRC_SCRUBBER_TYPES_H_

typedef enum
{
	SCRB_OK = 0,	// Scrubber is operating normally
	SCRB_IFAIL,		// Scrubber initialization failed
	SCRB_STOPPED	// Scrubber stopped unexpectedly
} SCRBRESULT;


#endif /* SRC_SCRUBBER_TYPES_H_ */

/** @}*/
