/*
 * Copyright (C) 2016 - 2018 Xilinx, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 */

#include "config_apps.h"

const volatile static char version[] = "MasterSystem Version " version_xstr(VERSION_MAJOR.VERSION_MINOR.VERSION_PATCH.VERSION_BUILD);

#include <stdio.h>
#include <sleep.h>

#include "xparameters.h"
#include "netif/xadapter.h"
#include "xstatus.h"
#include "xil_printf.h"
#include "xtime_l.h"

#include "platform/platform.h"
#include "config_apps.h"

#include "FreeRTOS.h"
#include "queue.h"

// Include interfaces for each task
#include "scrubber/scrubber.h"
#include "scrubber/scrubber_fr.h"
#include "scrubber/scrubber_blind.h"

#include "webserver/webserver.h"
#include "fault_injection/fault_injection.h"

#include "utils/frozen/frozen.h"

#include "utils/utils.h"
#include "utils/jtag/jtag.h"

#if LWIP_IPV6==1
#include "lwip/ip.h"
#else
#if LWIP_DHCP==1
#include "lwip/dhcp.h"
#endif
#endif

#ifdef XPS_BOARD_ZCU102
#ifdef XPAR_XIICPS_0_DEVICE_ID
int IicPhyReset(void);
#endif
#endif

jtag_t jtag_objects[ACTIVE_PORTS];
SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];
RTC_t rtc = {0, 0};
uint32_t heartbeat;

int main_thread();

void print_web_app_header();
void print_fretz_app_header();

int web_application_thread(void *);
int fretz_server_thread(void *);

void lwip_init();

#if LWIP_IPV6==0
#if LWIP_DHCP==1
extern volatile int dhcp_timoutcntr;
err_t dhcp_start(struct netif *netif);
#endif
#endif

static struct netif server_netif;

#if LWIP_IPV6==1
void print_ip6(char *msg, ip_addr_t *ip)
{
	print(msg);
	xil_printf(" %x:%x:%x:%x:%x:%x:%x:%x\n\r",
			IP6_ADDR_BLOCK1(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK2(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK3(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK4(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK5(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK6(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK7(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK8(&ip->u_addr.ip6));
}

#else
void
print_ip(char *msg, ip_addr_t *ip)
{
	xil_printf(msg);
	xil_printf("%d.%d.%d.%d\n\r", ip4_addr1(ip), ip4_addr2(ip),
			ip4_addr3(ip), ip4_addr4(ip));
}

void
print_ip_settings(ip_addr_t *ip, ip_addr_t *mask, ip_addr_t *gw)
{

	print_ip("Board IP: ", ip);
	print_ip("Netmask : ", mask);
	print_ip("Gateway : ", gw);
}

#endif

static int create_frame_address_list_file(jtag_t *JtagObj, SemaphoreHandle_t *jtag_mutex)
{
	FIL fp;
	FRESULT fr;

	// Check if JTAG is initialized
	if (JtagObj->status != JOK)
		return -1;

	f_mkdir(JTAG_ROOT);
	f_chdir(JTAG_ROOT);

	char filename[100];
	strcpy(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fp, filename, FA_WRITE | FA_CREATE_NEW);

	if (fr == FR_EXIST) {
		f_close(&fp);
		return 0;
	}

	word_t far = 0;
	word_t prev_far = -1;

	int isRowBoundary = 0;

	// Write address 0
	f_printf(&fp, "%08x\n", far);

	xSemaphoreTake(*jtag_mutex, portMAX_DELAY);

	resetTAP(JtagObj);
	cfg_sync(JtagObj);

	// Write FAR with starting FRAD 0.
	shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_FAR << 13) | 1);
	shiftDR32(JtagObj, far);

	// While far != Type 4 address
	while (!(far & (0x1 << 25))) {

		// Command configuration module to read configuration data
		shiftDR32(JtagObj, TYPE1 | OP_WR | (CFG_CMD << 13) | 1);
		shiftDR32(JtagObj, CFG_CMD_RCFG);

		// Command configuration module to read n+2 frames
		shiftDR32(JtagObj, TYPE1 | OP_RD | (CFG_FDRO << 13) | 0);
		shiftDR32(JtagObj, TYPE2 | OP_RD | (((isRowBoundary+2) * JtagObj->device.wpf) + JtagObj->device.pipeline_words));
		shiftDR32(JtagObj, CFG_NOOP);
		shiftDR32(JtagObj, CFG_NOOP);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_OUT);

		// Read first frame, the dummy, this will also make the FAR to auto-increment
		shiftDR(JtagObj, NULL, JtagObj->device.bpf + (JtagObj->device.pipeline_words * JtagObj->device.bpw), 0);

		// If we reached the row boundary then read an extra frame
		if (isRowBoundary == 1)
			shiftDR(JtagObj, NULL, JtagObj->device.bpf, 0);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_IN);

		shiftDR32(JtagObj, TYPE1 | OP_RD | (CFG_FAR << 13) | 1);
		shiftDR32(JtagObj, CFG_NOOP);
		shiftDR32(JtagObj, CFG_NOOP);

		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_OUT);
		shiftDR(JtagObj, &far, 32, 0);


		shiftIR(JtagObj, JtagObj->device.bsi.BSI_CFG_IN);

		if (prev_far == far) {
			isRowBoundary = 1;
		} else {
			isRowBoundary = 0;
			f_printf(&fp, "%08x\n", far);
		}

		prev_far = far;

	}

	cfg_desync(JtagObj);

	resetTAP(JtagObj);

	xSemaphoreGive(*jtag_mutex);

	f_close(&fp);

	return 0;
}

static void start_servers()
{
#if HTTP_SERVER==1
	print_web_app_header();
	xil_printf("\r\n");
	sys_thread_new("httpd", (void(*)(void*)) web_application_thread, 0,
		THREAD_STACKSIZE,
		DEFAULT_THREAD_PRIO);
#endif
#if FRETZ_SERVER==1
	print_fretz_app_header();
	sys_thread_new("fretz", (void(*)(void*)) fretz_server_thread, 0,
		THREAD_STACKSIZE,
		DEFAULT_THREAD_PRIO);
#endif
}

static int load_bsi_parameter_from_file(char *key, uint32_t *param, char *content, int str_len)
{
	int count_scanned = 0;
	char *tmp_hex = 0;

	char search_str[50];
	sprintf(search_str, "{BSI_e: {%s: %%Q}}", key);

	count_scanned += json_scanf(content, str_len, search_str, &tmp_hex);
	hexString2Int(tmp_hex, '\0', param);
	vPortFree(tmp_hex);

	return count_scanned;
}

static int load_device_parameters_from_file(jtag_t *JtagObj)
{
	f_chdir("/");

	char *content = json_fread("Device.json");

	if (content == NULL)
		return -1;

	char *tmp_hex;
	int str_len = strlen(content);
	int count_scanned = 0;

	count_scanned += json_scanf(content, str_len, "{IDCODE: %Q}", &tmp_hex);
	hexString2Int(tmp_hex, '\0', &JtagObj->device_idcode);
	vPortFree(tmp_hex);

	count_scanned += json_scanf(content, str_len, "{ARCH: %d}", &JtagObj->device.arch);
	count_scanned += json_scanf(content, str_len, "{WPF: %d}", &JtagObj->device.wpf);
	count_scanned += json_scanf(content, str_len, "{BPW: %d}", &JtagObj->device.bpw);
	count_scanned += json_scanf(content, str_len, "{PIPELINE: %d}", &JtagObj->device.pipeline_words);
	count_scanned += json_scanf(content, str_len, "{IR_LEN: %d}", &JtagObj->device.ir_len);
	count_scanned += json_scanf(content, str_len, "{JC_BEFORE: %d}", &JtagObj->device.jc_before);
	count_scanned += json_scanf(content, str_len, "{JC_AFTER: %d}", &JtagObj->device.jc_after);
	count_scanned += json_scanf(content, str_len, "{JMODE: %d}", &JtagObj->mode);

	count_scanned += load_bsi_parameter_from_file("BSI_EXTEST", &JtagObj->device.bsi.BSI_EXTEST, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_EXTEST_PULSE", &JtagObj->device.bsi.BSI_EXTEST_PULSE, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_EXTEST_TRAIN", &JtagObj->device.bsi.BSI_EXTEST_TRAIN, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_SAMPLE", &JtagObj->device.bsi.BSI_SAMPLE, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_USER1", &JtagObj->device.bsi.BSI_USER1, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_USER2", &JtagObj->device.bsi.BSI_USER2, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_USER3", &JtagObj->device.bsi.BSI_USER3, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_USER4", &JtagObj->device.bsi.BSI_USER4, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_CFG_OUT", &JtagObj->device.bsi.BSI_CFG_OUT, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_CFG_IN", &JtagObj->device.bsi.BSI_CFG_IN, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_USERCODE", &JtagObj->device.bsi.BSI_USERCODE, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_IDCODE", &JtagObj->device.bsi.BSI_IDCODE, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_HIGHZ_IO", &JtagObj->device.bsi.BSI_HIGHZ_IO, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_JPROGRAM", &JtagObj->device.bsi.BSI_JPROGRAM, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_JSTART", &JtagObj->device.bsi.BSI_JSTART, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_JSHUTDOWN", &JtagObj->device.bsi.BSI_JSHUTDOWN, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_ISC_ENABLE", &JtagObj->device.bsi.BSI_ISC_ENABLE, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_ISC_PROGRAM", &JtagObj->device.bsi.BSI_ISC_PROGRAM, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_XSC_DNA", &JtagObj->device.bsi.BSI_XSC_DNA, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_FUSE_DNA", &JtagObj->device.bsi.BSI_FUSE_DNA, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_ISC_NOOP", &JtagObj->device.bsi.BSI_ISC_NOOP, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_ISC_DISABLE", &JtagObj->device.bsi.BSI_ISC_DISABLE, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_BYPASS", &JtagObj->device.bsi.BSI_BYPASS, content, str_len);
	count_scanned += load_bsi_parameter_from_file("BSI_JTAG_CTRL", &JtagObj->device.bsi.BSI_JTAG_CTRL, content, str_len);

	vPortFree(content);

	if (count_scanned < 6)
		return -2;

	return 0;
}

void heartbeat_thread(void)
{
	#if HEARTBEAT==1
		uint32_t b;
		while(1) {

			xSemaphoreTake(jtag_mutex[0], portMAX_DELAY);

			read_jreg(&jtag_objects[0], jtag_objects[0].device.bsi.BSI_USER2, 1, &b);
			b = (b >> 15) & 0x1;

			#if HEARTBEAT_DEBUG == 1
				xil_printf("Heartbeat value: %08x\r\n", b);
			#endif

			heartbeat = b;

			xSemaphoreGive(jtag_mutex[0]);
			vTaskDelay(pdMS_TO_TICKS(5000));
		}
	#endif
}

int main()
{

	// Wait 5 seconds to print messages on UART.
	// This is IMPORTANT in order to connect with the UART!
	sleep(5);

	xil_printf("Heap stage start: %d\r\n", xPortGetFreeHeapSize());

	// Initialize platform
	if (init_platform() != 0) {
		xil_printf("ERROR initializing platform.\r\n");
		return -1;
	}

	JRESULT res;

	int r = load_device_parameters_from_file(&jtag_objects[0]);

	if (r != 0) {
		xil_printf("Failed to load device parameters from file \"Device.json\"\r\n");

		if (r == -1) {
			xil_printf("No such file.\r\n");
		} else if (r == -2) {
			xil_printf("Missing device parameter or error on file format.\r\n");
		}

		xil_printf("Loading default device parameters\r\n");
		res = jtag_init(&jtag_objects[0], XPAR_AXI2JTAG_0_BASEADDR, "JC", next_frad, 1);
	} else {
		res = jtag_init(&jtag_objects[0], XPAR_AXI2JTAG_0_BASEADDR, "JC", next_frad, 0);
	}

	// Allocate mutexes for jtag_objects
	for (int i = 0; i < ACTIVE_PORTS; i++) {
		jtag_mutex[i] = xSemaphoreCreateMutex();
	}

	// Generate frame address list file if not exists
	if (res == JOK) {
		xil_printf("JTAG: initialization completed\r\n");
		create_frame_address_list_file(&jtag_objects[0], &jtag_mutex[0]);
	} else {
		xil_printf("JTAG: initialization failed with error code %d\r\n", res);
	}

	// If scrubber is disabled then assign NULL pointers
	scrb_qr = NULL;
	scrb_qt = NULL;

	// If scrubber_fr is disabled then assign NULL pointers
	scrbFr_qr = NULL;
	scrbFr_qt = NULL;

	// If scrubber_blind is disabled then assign NULL pointers
	scrbBlind_qr = NULL;
	scrbBlind_qt = NULL;

	// If fault injection is disabled then assign NULL pointers
	finj_qr = NULL;
	finj_qt = NULL;

	server_qhandlers.scrb_qr = &scrb_qr;
	server_qhandlers.scrb_qt = &scrb_qt;

	server_qhandlers.scrbFr_qr = &scrbFr_qr;
	server_qhandlers.scrbFr_qt = &scrbFr_qt;

	server_qhandlers.scrbBlind_qr = &scrbBlind_qr;
	server_qhandlers.scrbBlind_qt = &scrbBlind_qt;

	server_qhandlers.finj_qr = &finj_qr;
	server_qhandlers.finj_qt = &finj_qt;

	xil_printf("Starting up application's thread.\r\n");

#if MAIN_SERVER==1
	sys_thread_new("main_thrd", (void(*)(void*))main_thread, 0,
	                THREAD_STACKSIZE,
	                0);
#endif

	/* Call initialization of each task before the creation of the task
	 * This could be done on each task separately but to
	 * avoid deadlocks of shared resources like the FatFs
	 * this is done on main before the creation of the tasks
	 */

#if SCRUBBER==1

	init_scrubber();

	xil_printf("Heap stage scrubber: %d\r\n", xPortGetFreeHeapSize());

	// Setup receive/transmit queues for server - scrubber communication
	scrb_qr = xQueueCreate(1, sizeof(char*));
	scrb_qt = xQueueCreate(1, sizeof(char*));

	sys_thread_new("scrubber", (void(*)(void*))scrubber_thread, 0,
					THREAD_STACKSIZE,
					0);

#endif

#if SCRUBBER_FR==1

	init_scrubber_fr();

	xil_printf("Heap stage scrubber_fr: %d\r\n", xPortGetFreeHeapSize());

	// Setup receive/transmit queues for server - scrubber fr communication
	scrbFr_qr = xQueueCreate(1, sizeof(char*));
	scrbFr_qt = xQueueCreate(1, sizeof(char*));

	sys_thread_new("scrubber Fr", (void(*)(void*))scrubber_fr_thread, 0,
					THREAD_STACKSIZE,
					0);

#endif

#if SCRUBBER_BLIND==1

	init_scrubber_blind();

	xil_printf("Heap stage scrubber_blind: %d\r\n", xPortGetFreeHeapSize());

	// Setup receive/transmit queues for server - scrubber blind communication
	scrbBlind_qr = xQueueCreate(1, sizeof(char*));
	scrbBlind_qt = xQueueCreate(1, sizeof(char*));

	sys_thread_new("scrubber Blind", (void(*)(void*))scrubber_blind_thread, 0,
					THREAD_STACKSIZE,
					0);

#endif

#if FINJECTION==1

	// Setup receive/transmit queues for server - fault injection communication
	finj_qr = xQueueCreate(1, sizeof(char*));
	finj_qt = xQueueCreate(1, sizeof(char*));

	sys_thread_new("finj", (void(*)(void*))fault_injection_thread, 0,
					THREAD_STACKSIZE,
					0);
#endif

#if HEARTBEAT==1
	sys_thread_new("heartbeat", (void(*)(void*))heartbeat_thread, 0,
			THREAD_STACKSIZE,
			0);
#endif

	xil_printf("Heap stage final: %d\r\n", xPortGetFreeHeapSize());

	vTaskStartScheduler();
	while(1);
	return 0;
}

void network_thread(void *p)
{
    struct netif *netif;
    /* the mac address of the board. this should be unique per board */
    unsigned char mac_ethernet_address[] = { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };
#if LWIP_IPV6==0
    ip_addr_t ipaddr, netmask, gw;
#if LWIP_DHCP==1
    int mscnt = 0;
#endif
#endif

    netif = &server_netif;

    xil_printf("\r\n\r\n");
    xil_printf("-----lwIP Socket Mode Echo server Demo Application ------\r\n");

#if LWIP_IPV6==0
#if LWIP_DHCP==0
    /* initliaze IP addresses to be used */
    IP4_ADDR(&ipaddr,  192, 168, 1, 10);
    IP4_ADDR(&netmask, 255, 255, 255,  0);
    IP4_ADDR(&gw,      192, 168, 1, 1);
#endif

    /* print out IP settings of the board */

#if LWIP_DHCP==0
    print_ip_settings(&ipaddr, &netmask, &gw);
    /* print all application headers */
#endif

#if LWIP_DHCP==1
	ipaddr.addr = 0;
	gw.addr = 0;
	netmask.addr = 0;
#endif
#endif

#if LWIP_IPV6==0
    /* Add network interface to the netif_list, and set it as default */
    if (!xemac_add(netif, &ipaddr, &netmask, &gw, mac_ethernet_address, PLATFORM_EMAC_BASEADDR)) {
	xil_printf("Error adding N/W interface\r\n");
	return;
    }
#else
    /* Add network interface to the netif_list, and set it as default */
    if (!xemac_add(netif, NULL, NULL, NULL, mac_ethernet_address, PLATFORM_EMAC_BASEADDR)) {
	xil_printf("Error adding N/W interface\r\n");
	return;
    }

    netif->ip6_autoconfig_enabled = 1;

    netif_create_ip6_linklocal_address(netif, 1);
    netif_ip6_addr_set_state(netif, 0, IP6_ADDR_VALID);

    print_ip6("\n\rBoard IPv6 address ", &netif->ip6_addr[0].u_addr.ip6);
#endif

    netif_set_default(netif);

    /* specify that the network if is up */
    netif_set_up(netif);

    /* start packet receive thread - required for lwIP operation */
    sys_thread_new("xemacif_input_thread", (void(*)(void*))xemacif_input_thread, netif,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);

#if LWIP_IPV6==0
#if LWIP_DHCP==1
    dhcp_start(netif);
    while (1) {
		vTaskDelay(DHCP_FINE_TIMER_MSECS / portTICK_RATE_MS);
		dhcp_fine_tmr();
		mscnt += DHCP_FINE_TIMER_MSECS;
		if (mscnt >= DHCP_COARSE_TIMER_SECS*1000) {
			dhcp_coarse_tmr();
			mscnt = 0;
		}
	}
#else
    xil_printf("\r\n");
    xil_printf("%20s %6s %s\r\n", "Server", "Port", "Connect With..");
    xil_printf("%20s %6s %s\r\n", "--------------------", "------", "--------------------");

    start_servers();

    vTaskDelete(NULL);
#endif
#else

    start_servers();

    vTaskDelete(NULL);
#endif
    return;
}

int main_thread()
{
#if LWIP_DHCP==1
	int mscnt = 0;
#endif

#ifdef XPS_BOARD_ZCU102
	IicPhyReset();
#endif

	/* initialize lwIP before calling sys_thread_new */
    lwip_init();

    /* any thread using lwIP should be created using sys_thread_new */
    sys_thread_new("NW_THRD", network_thread, NULL,
		THREAD_STACKSIZE, DEFAULT_THREAD_PRIO);

#if LWIP_IPV6==0
#if LWIP_DHCP==1
    while (1) {
	vTaskDelay(DHCP_FINE_TIMER_MSECS / portTICK_RATE_MS);
		if (server_netif.ip_addr.addr) {
			xil_printf("DHCP request success\r\n");
			print_ip_settings(&(server_netif.ip_addr), &(server_netif.netmask), &(server_netif.gw));

			start_servers();
			break;
		}
		mscnt += DHCP_FINE_TIMER_MSECS;
		if (mscnt >= 10000) {
			xil_printf("ERROR: DHCP request timed out\r\n");
			xil_printf("Configuring default IP of 192.168.1.10\r\n");
			IP4_ADDR(&(server_netif.ip_addr),  192, 168, 1, 10);
			IP4_ADDR(&(server_netif.netmask), 255, 255, 255,  0);
			IP4_ADDR(&(server_netif.gw),  192, 168, 1, 1);
			print_ip_settings(&(server_netif.ip_addr), &(server_netif.netmask), &(server_netif.gw));
			/* print all application headers */
			xil_printf("\r\n");
			xil_printf("%20s %6s %s\r\n", "Server", "Port", "Connect With..");
			xil_printf("%20s %6s %s\r\n", "--------------------", "------", "--------------------");

			start_servers();
			break;
		}

	}
#endif
#endif

    vTaskDelete(NULL);
    return 0;
}
