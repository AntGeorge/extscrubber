/*
 * Copyright (c) 2009 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __CONFIG_APPS_H_
#define __CONFIG_APPS_H_

#define version_xstr(s) version_str(s)
#define version_str(s) #s

#define VERSION_MAJOR 				0	// Master system version
#define VERSION_MINOR 				5	// Fretz protocol version
#define VERSION_PATCH 				3	// JTAG library major version
#define VERSION_BUILD 				3	// JTAG library minor version

#define THREAD_STACKSIZE			2048 // Empirically chosen

// Keep this 1 if you are using any server (http or fretz)
#define MAIN_SERVER					1

/**
 *	Modules Configuration
 *	Format: <MODULE NAME>_<CONFIG NAME>		<VALUE>
 */
#define HTTP_SERVER					1
#define HTTP_SERVER_DEBUG			1

// Heartbeat used by all scrubbers
#define HEARTBEAT						0
#define HEARTBEAT_DEBUG					0

// Scrubber configuration values
// This configuration is used in the scrubber task
// which implements the golden parities technique
#define SCRUBBER						0
#define SCRUBBER_DEBUG					1
#define SCRUBBER_SDLOGS					1
#define SCRUBBER_SDLOGS_FRAMEDATA		0
#define SCRUBBER_ROOT					"/scrubber"
#define SCRUBBER_STATUS_LOG				SCRUBBER_ROOT "/status.log"
#define SCRUBBER_LOG_FOLDER				SCRUBBER_ROOT "/logs_corrections"
#define SCRUBBER_LOG_FRAMEDATA_FOLDER	SCRUBBER_ROOT "/logs_framedata"
#define SCRUBBER_LOG_MAX_LINES			1000
// Scrubber Algorithm definitions.
#define SCRUBBER_FRAMES_PER_GROUP		74
#define SCRUBBER_NUMBER_OF_SUBGROUPS 	2
#define SCRUBBER_MAX_FAILED_LOG			5

// Scrubber FR configuration values
// This configuration is used in the scrubber_fr task
// which implements the frame replacement technique
#define SCRUBBER_FR						0
#define SCRUBBER_FR_DEBUG				1
#define SCRUBBER_FR_SDLOGS				1
#define SCRUBBER_FR_ROOT				"/scrubber_fr"
#define SCRUBBER_FR_STATUS_LOG			SCRUBBER_FR_ROOT "/status.log"
#define SCRUBBER_FR_LOG_FOLDER			SCRUBBER_FR_ROOT "/logs_corrections"
#define SCRUBBER_FR_LOG_MAX_LINES		1000

// Scrubber Blind configuration values
// This configuration is used in the scrubber_blind task
// which implements the blind scrubbing technique
#define SCRUBBER_BLIND					1
#define SCRUBBER_BLIND_DEBUG			1
#define SCRUBBER_BLIND_SDLOGS			1
#define SCRUBBER_BLIND_ROOT				"/scrubber_blind"
#define SCRUBBER_BLIND_STATUS_LOG		SCRUBBER_BLIND_ROOT "/status.log"

// Fault injection configuration values
#define FINJECTION					1
#define FINJECTION_DEBUG			1

// Fretz server configuration values
#define FRETZ_SERVER				1
#define FRETZ_SERVER_DEBUG			0
#define FRETZ_SERVER_TIME			0
#define FRETZ_ROOT					"/fretz"
#define FRETZ_RBFiles				FRETZ_ROOT "/readback-files"
#define FRETZ_RBCFiles				FRETZ_ROOT "/readback-capture-files"

// Jtag library configuration values
#define JTAG_ROOT					"/jtag"

// GPIO Settings on PMOD JD
#define PIN_OFFSET 	54 				// Base pin offset, do not edit this
#define DONE_PIN	PIN_OFFSET + 0	// Pin 1, T14
#define RESET_PIN	PIN_OFFSET + 1  // Pin 2, T15
#define RESET_TIME	1000			// Keep 1 sec the reset asserted

#endif
