/*
 * Copyright (c) 2009 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __PLATFORM_H_
#define __PLATFORM_H_

#define PLATFORM_EMAC_BASEADDR XPAR_XEMACPS_0_BASEADDR
#define PLATFORM_ZYNQ

#include "utils/jtag/jtag.h"

#include "FreeRTOS.h"
#include "semphr.h"

#define ACTIVE_PORTS	1
#define TOP_HALF		0
#define BOTTOM_HALF		1

extern jtag_t jtag_objects[ACTIVE_PORTS];
extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];

enum BLOCK_TYPE_e {
	BLOCK_TYPE_CFG		= 0x0,
	BLOCK_TYPE_BRAM		= 0x1,
	BLOCK_TYPE_CFG_CLB	= 0x2,
	BLOCK_TYPE_UNKNOWN	= 0x3
};

/** Defines the 26-bit Frame Address Register Description.*/
typedef struct frad_s frad_t;
struct frad_s {
	uint32_t minor_addr : 7;
	uint32_t column_addr : 10;
	uint32_t row_addr : 5;
	uint32_t top_bot : 1;
	uint32_t block_type: 3;
};

/** Defines the 13-bit ECC Syndrome Description*/
typedef struct ecc_synd_s ecc_syndrome_t;
struct ecc_synd_s {
	uint16_t syndrome : 12;
	uint16_t parity_bit : 1;
};

int init_platform();
void cleanup_platform();

#endif
