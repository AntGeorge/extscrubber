/*
 * platform_fs.c
 *
 *  Created on: 26 January 2019
 *      Author: AntGeorge
 */

#ifndef __PLATFORM_FS_H_
#include "platform_fs.h"
#endif

#include "xil_printf.h"

#ifndef __CONFIG_APPS_H_
#include "config_apps.h"
#endif

#include "ff.h"
static FATFS fatfs;

#include "xstatus.h"

int platform_init_fs()
{
	FRESULT fr;
	TCHAR *Path = "0:/";

	fr = f_mount(&fatfs, Path, 0);

	if (fr != FR_OK) {
		return XST_FAILURE;
	}

	FILINFO fno;

	f_chdir("/webfiles");

	fr = f_stat("index.html", &fno);

	switch (fr) {

	case FR_OK:
		xil_printf("Found index.html file\r\nSize: %lu\r\n", fno.fsize);

		xil_printf("SD filesystem initialization succeed\r\n");
		return 0;

	case FR_NO_FILE:
		xil_printf("Cannot find index.html\r\n");
		break;

	default:
		xil_printf("An error occurred. (%d)\r\n", fr);
	}

	xil_printf("SD filesystem initialization failed\r\n");
	return -1;
}

