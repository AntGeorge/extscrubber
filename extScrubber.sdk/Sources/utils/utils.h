/*
 * utils.h
 *
 *  Created on: 28 May 2020
 *      Author: Ageor
 */

#ifndef SRC_UTILS_UTILS_H_
#define SRC_UTILS_UTILS_H_

#include "xtime_l.h"
#include "utils/frozen/frozen.h"
#include "utils/jtag/jtag.h"

typedef struct {
	XTime synced_GT_value;
	uint64_t host_time;
} RTC_t;

extern RTC_t rtc;

typedef struct {
	char * raw_body;
	unsigned int len;
} json2_t;

int hexString2Int(char const *b, char EOS, uint32_t *num);
uint32_t byte_swap32(uint32_t buf);
uint16_t byte_swap16(uint16_t buf);
int create_json(char **buf, int base_size, int (*json_printf_callback)(struct json_out *buf));

/** Read the data of all the frames provided by the frame address list file
 *  and store them in frame_data.bit file
 *
 *	NOTE: The caller is responsible for the JTAG resource lock
 *
 * @return 0 for success otherwise error

 */
int init_frame_data_file(jtag_t *JtagObj);

unsigned int get_timestamp();

uint8_t is_done_active();
int reset_target();

#endif /* SRC_UTILS_UTILS_H_ */
