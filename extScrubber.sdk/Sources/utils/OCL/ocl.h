/** @defgroup OCL On-Chip-Logic API
 *
 * ocl.h
 *
 * @date 21 April 2022
 * @author AntGeorge
 *
 * @details
 *		This file contains the public functions
 *		to communicate with the OCL logic.
 *	@{
 */

#ifndef SRC_OCL_H_
#define SRC_OCL_H_

#include "utils/jtag/jtag.h"

#define OCL_FIFO_SIZE	512

// This is read by all scrubber and written by the heartbeat_thread
extern uint32_t heartbeat;

// Setup structures for command fifo interface.
/** Defines FIFO commands. */
enum FIFO_COMMANDS_e {
	READ_FIFO	= 0x6,
	RESET_FIFO	= 0x9,
	NOOP 		= 0x0
};

/** Defines the 48-bit FIFO entry as described in OCL. */
typedef struct fifo_s fifo_entry;
struct fifo_s {
	uint64_t ecc_valid : 1;
	uint64_t ecc_error : 1;
	uint64_t crc_error : 1;
	uint64_t ecc_syndrome : 13;
	uint64_t frad : 26;
	uint64_t fifo_empty : 1;
	uint64_t fifo_almfull : 1;
	uint64_t fifo_full : 1;
	uint64_t reserved : 3;
};

/** Reads the first entry of the OCL FIFO.
 *
 * This function checks if DONE pin is asserted using the GPIO.
 *
 * @return 0 if DONE pin is active else -1.
 */
int read_FIFO(jtag_t *JtagObj, fifo_entry * fe);

/** Resets the internal FIFO of the OCL logic
 *
 * @return void
 */
void resetFIFO(jtag_t *JtagObj);

/** Extracts syndrome fields from syndrome
 *
 * @return void
 */
void extract_syndrome(uint32_t syndrome, int32_t *synbit, int32_t *synword, int32_t *parity);

/** Thread that reads the heartbeat from the OCL
 *
 * @return void
 */
void heartbeat_thread(void);

#endif /* SRC_OCL_H_ */
