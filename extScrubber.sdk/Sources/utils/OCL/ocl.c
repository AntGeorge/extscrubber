
#include "utils/jtag/jtag.h"
#include "utils/utils.h"

#include "ocl.h"


int read_FIFO(jtag_t *JtagObj, fifo_entry * fe)
{
	if (!is_done_active())
		return -1;

	uint32_t buff[2];

	write_jreg32(JtagObj, JtagObj->device.bsi.BSI_USER1, 4, READ_FIFO);
	read_jreg(JtagObj, JtagObj->device.bsi.BSI_USER1, 48, buff);

	buff[0] = reverseBits(buff[0], 32);
	buff[1] = reverseBits(buff[1], 16);

	fe->ecc_valid = (buff[0] >> 0);
	fe->ecc_error = (buff[0] >> 1);
	fe->crc_error = (buff[0] >> 2);
	fe->ecc_syndrome = (buff[0] >> 3);
	fe->frad = ((buff[0] >> 16) | (((uint64_t) buff[1]) << 16));
	fe->fifo_empty = (buff[1] >> 10);
	fe->fifo_almfull = (buff[1] >> 11);
	fe->fifo_full = (buff[1] >> 12);

	return 0;
}

void resetFIFO(jtag_t *JtagObj)
{
	write_jreg32(JtagObj, JtagObj->device.bsi.BSI_USER1, 4, RESET_FIFO);
	write_jreg32(JtagObj, JtagObj->device.bsi.BSI_USER1, 4, NOOP);
}

// Returns the bit position and word position based on the syndrome.
void extract_syndrome(uint32_t syndrome, int32_t *synbit, int32_t *synword, int32_t *parity)
{
	*synbit = syndrome & 0x1F; // syndrome[0:4]
	uint32_t synd_12_5 = (syndrome >> 5) & 0xFF; // syndrome[5:12]

	if (synd_12_5 >= 153 && synd_12_5 < 160)
		*synword = synd_12_5 - 153;
	else if (synd_12_5 >= 160 && synd_12_5 < 192)
		*synword = synd_12_5 - 154;
	else if (synd_12_5 >= 192 && synd_12_5 < 255)
		*synword = synd_12_5 - 155;

	*parity = (syndrome >> 12) & 0x1;
}
