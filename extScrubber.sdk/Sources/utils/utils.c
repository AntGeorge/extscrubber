/*
 * utils.c
 *
 *  Created on: 28 May 2020
 *      Author: Ageor
 */

#include <stdio.h>
#include <sleep.h>

#include <utils/frozen/frozen.h>
#include "FreeRTOS.h"
#include "utils/jtag/jtag.h"
#include "config_apps.h"

#include "xtime_l.h"
#include "xparameters.h"
#include "xgpiops.h"
#include "xstatus.h"

#include "utils.h"



static int char2int(char ch)
{
    if (ch >= '0' && ch <= '9')
        return ch - '0';
    if (ch >= 'A' && ch <= 'F')
        return ch - 'A' + 10;
    if (ch >= 'a' && ch <= 'f')
        return ch - 'a' + 10;
    return -1;
}

int hexString2Int(char const *b, char EOS, uint32_t *num)
{
	*num = 0;
	int tmp;
	for (int i = 0; b[i] != EOS; i++) {
		*num<<=4;
		if ((tmp = char2int(b[i])) == -1)
			return -1;
		*num += tmp;
	}
	return 0;
}

uint32_t byte_swap32(uint32_t buf)
{
	return (((buf>>24)&0xff) |			// move byte 3 to byte 0
	        ((buf<<8)&0xff0000) |		// move byte 1 to byte 2
	        ((buf>>8)&0xff00) |			// move byte 2 to byte 1
	        ((buf<<24)&0xff000000));	// byte 0 to byte 3
}

uint16_t byte_swap16(uint16_t buf)
{
	return (((buf>>8)&0x00ff) | // move byte 1 to byte 0
	        ((buf<<8)&0xff00)); // byte 0 to byte 1
}

int create_json(char **buf, int base_size, int (*json_printf_callback)(struct json_out *buf))
{
	int buf_size = base_size / 2,
		written_bytes,
		ret = 0;

	*buf = NULL;

	do {
		buf_size *= 2;

		vPortFree(*buf);

		*buf = (char *) pvPortMalloc(buf_size);

		if (*buf == NULL) {
			ret = -1;
			break;
		}

		struct json_out out = JSON_OUT_BUF(*buf, buf_size);
		written_bytes = json_printf_callback(&out);
	} while(written_bytes >= buf_size);

	return ret;
}

int init_frame_data_file(jtag_t *JtagObj)
{
	FIL fd_framedata, fd_framelist;
	FRESULT fr;
	uint32_t frad;
	TCHAR buff[10];
	UINT br;
	frame_t frame;

	// Check if JTAG is initialized
	if (JtagObj->status != JOK)
		return -1;

	f_chdir("/");
	f_mkdir(JTAG_ROOT);
	f_chdir(JTAG_ROOT);

	fr = f_open(&fd_framedata, "frame_data.bit", FA_WRITE | FA_CREATE_NEW);

	// If frame data file exists do not overwrite
	if (fr == FR_EXIST)
		return 0;

	char filename[100];
	strcpy(filename, JTAG_ROOT);
	strcat(filename, "/");
	strcat(filename, JtagObj->name);
	strcat(filename, "_frame_addresses.txt");

	fr = f_open(&fd_framelist, filename, FA_READ);

	if (fr) {
		f_close(&fd_framedata);
		f_close(&fd_framelist);
		return -1;
	}

	while(1) {
		f_gets(buff, 10, &fd_framelist);

		if (f_eof(&fd_framelist))
			break;

		hexString2Int(buff, '\n', &frad);

		read_frames(JtagObj, frad, 1, &frame, 0);
		f_write(&fd_framedata, &frame.words, sizeof(word_t) * JtagObj->device.wpf, &br);

		if (br < sizeof(word_t) * JtagObj->device.wpf) {
			xil_printf("Scrubber: init frame data file stopped, SD card is full.\r\n");
			f_close(&fd_framedata);
			f_close(&fd_framelist);
			return -1;
		}
	}

	f_close(&fd_framedata);
	f_close(&fd_framelist);

	return 0;
}

unsigned int get_timestamp()
{
	XTime current_time;
	XTime_GetTime(&current_time);
	return rtc.host_time + ((current_time - rtc.synced_GT_value) / COUNTS_PER_SECOND);
}

uint8_t is_done_active()
{
	static int initialized = 0;

	uint8_t value;

	int Status;
	static XGpioPs Gpio;	/* The driver instance for GPIO Device. */
	XGpioPs_Config *ConfigPtr;

	/* Initialize the GPIO driver. */
	if (initialized == 0)
	{
		ConfigPtr = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID);
		Status = XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

		if (Status != XST_SUCCESS)
		{
			xil_printf("Done pin initialization failed\r\n");
			return XST_FAILURE;
		}

		XGpioPs_SetDirectionPin(&Gpio, DONE_PIN, 0);
		XGpioPs_SetOutputEnablePin(&Gpio, DONE_PIN, 0);

		initialized = 1;
	}

	value = XGpioPs_ReadPin(&Gpio, DONE_PIN); // Get value of pin

	return value;
}

int reset_target()
{
	int Status;
	XGpioPs Gpio;	/* The driver instance for GPIO Device. */
	XGpioPs_Config *ConfigPtr;

	/* Initialize the GPIO driver. */
	ConfigPtr = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID);
	Status = XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XGpioPs_SetDirectionPin(&Gpio, RESET_PIN, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, RESET_PIN, 1);

	XGpioPs_WritePin(&Gpio, RESET_PIN, 1); // Assert reset pin

	sleep(RESET_TIME / 1000);

	XGpioPs_WritePin(&Gpio, RESET_PIN, 0); // Deassert reset pin

	return 0;
}

// From FatFs documentation.
//FRESULT scan_files (
//    char* path        /* Start node to be scanned (***also used as work area***) */
//)
//{
//    FRESULT res;
//    DIR dir;
//    UINT i;
//    static FILINFO fno;
//
//
//    res = f_opendir(&dir, path);                       /* Open the directory */
//    if (res == FR_OK) {
//        for (;;) {
//            res = f_readdir(&dir, &fno);                   /* Read a directory item */
//            if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
//            if (fno.fattrib & AM_DIR) {                    /* It is a directory */
//                i = strlen(path);
//                sprintf(&path[i], "/%s", fno.fname);
//                res = scan_files(path);                    /* Enter the directory */
//                if (res != FR_OK) break;
//                path[i] = 0;
//            } else {                                       /* It is a file. */
//                printf("%s/%s\n", path, fno.fname);
//            }
//        }
//        f_closedir(&dir);
//    }
//
//    return res;
//}
