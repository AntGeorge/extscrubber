/*
 * finn.c
 *
 *  Created on: Nov 21, 2021
 *      Author: george
 */

#include <stdlib.h>

#include <utils/fretz_protocol/protocol/protocol.h>
#include <xil_printf.h>
#include <fault_injection/fault_injection.h>
#include <utils/fretz_protocol/custom.h>

#define LOOP_STOP	1000

static jtag_t *JtagObj = &jtag_objects[0];

static uint32_t read_bscan(uint32_t reg)
{
	uint32_t ret;
	read_jreg(JtagObj, reg, 32, &ret);
	ret = reverseBits(ret, 32);
	return ret;
}

static void write_bscan(uint32_t reg, uint32_t data)
{
	write_jreg32(JtagObj, reg, 32, reverseBits(data, 32));
}

static int inject(uint32_t frameAddress, uint32_t bitIndex)
{
	uint32_t patterns[MAX_PATTERNS];
	patterns[0] = SBU_PATTERN;

	const int flips = 1;
	const int iterations = 1;
	const int delay = 0;
	const int type = 0;
	const int num_of_patterns = 1;
	void *returned_injections = NULL;
	const int return_injections = FALSE;
	void *num_of_injections = NULL;

	return fault_injection(JtagObj,
			frameAddress, frameAddress, bitIndex, bitIndex,
			flips, iterations, delay, type, patterns, num_of_patterns,
			returned_injections, return_injections, num_of_injections);
}

static void get_frame(uint32_t frameAddress, frame_t *frame_data)
{
	read_frames(JtagObj, frameAddress, 1, frame_data, 0);
}

static void set_frame(uint32_t frameAddress, frame_t *frame_data)
{
	write_frames(JtagObj, frameAddress, 1, frame_data, 0);
}

void example(uint32_t frameAddress, uint32_t bitIndex)
{
	// Read BSCAN register
	// Details: Reads user3 and stores the value to 'reg'
	uint32_t reg = read_bscan(BSI_USER3);

	uint32_t reg2 = 4;

	// Write BSCAN register
	// Details: Writes from 'reg2' to user4
	write_bscan(BSI_USER4, reg2);

	// Inject bit flip on the given frame address and bit position
	// Return values
	// 0 - OK
	// 1 - Error on frame address parameter
	// 2 - Error on bit index parameter
	int ret = inject(frameAddress, bitIndex);

	// === Example bit flip using read/write frame ===
		// Read frame
		// Details: Reads 1 frame and saves the data to 'frame' variable
		frame_t *frame;
		frame = pvPortMalloc(sizeof(frame_t));
		get_frame(frameAddress, frame);

		// Bit flip on word 15 bit 23
		frame->words[15] ^= (0x1 << 23);

		// Write frame
		// Details: Writes 1 frame on 'frameAddress'
		set_frame(frameAddress, frame);

		// Free allocated space
		vPortFree(frame);
	// === END OF EXAMPLE ===
}

uint8_t finn(uint32_t frameAddress, uint32_t bitIndex)
{
	int reset, loopn;
	uint32_t bscanRegister, status = FRETZ_OK;

	if (frameAddress<65536)
	{
		reset = FALSE;
//		f=open(self.path+"/results.txt","a+")
//		f.write(str(tests)+" "+str(hex(frameAddress))+" "+str(bitIndex)+" ")
//		f.close()
		// Write BSCAN4 VALUE 4
		//write_bscan(BSI_USER4, 0x4);
		//uint32_t a = read_bscan(BSI_IDCODE);
		//xil_printf("BSCAN VALUE 4 SENT, WAITING VALUE 4\r\n");
		loopn=LOOP_STOP;
		while(TRUE)
		{
			// Read BSCAN3 VALUE 8
			bscanRegister = read_bscan(BSI_USER3);
			if(bscanRegister == 4) {
				//xil_printf("BSCAN VALUE RECEIVED 4\r\n");
				break;
			}
			else {
				loopn -= 1;
				if (loopn > 0) {
					write_bscan(BSI_USER4, 0x4);
					//xil_printf("WAITING VALUE 4,RECEIVED VALUE %d\r\n", bscanRegister);
				}
				else {
					reset = TRUE;
					//xil_printf("Resetting the board\r\n");
					break;
				}
			}
		}
		//xil_printf("Exited Loop\r\n");
		if (reset == FALSE)
		{
			// INJECT FAULT
			status = inject(frameAddress, bitIndex);
			//xil_printf("fault inject status: %d\r\n", status);
			if (status == 0)
			{
				// Write BSCAN4 VALUE 1
				write_bscan(BSI_USER4, 0x1);
				//xil_printf("BSCAN VALUE 1 SENT, WAITING VALUE 1\r\n");
				loopn=LOOP_STOP;
				while(TRUE)
				{
					// Read BSCAN3 VALUE 0
					bscanRegister = read_bscan(BSI_USER3);
					if (bscanRegister == 1) {
						//xil_printf("BSCAN VALUE RECEIVED 1\r\n");
						break;
					}
					else {
						loopn -= 1;
						if (loopn > 0) {
							write_bscan(BSI_USER4, 0x1);
							//xil_printf("WAITING VALUE 1,RECEIVED VALUE %d\r\n", bscanRegister);
						}
						else {
							reset = TRUE;
							//xil_printf("Resetting the board\r\n");
							break;
						}
					}
				}

				//xil_printf("Exited Loop\r\n");
				if (reset == FALSE)
				{
					// CORRECT FAULT
					status = inject(frameAddress, bitIndex);

					if (status == 0)
					{
						// Write BSCAN4 VALUE 2
						write_bscan(BSI_USER4, 0x2);
						//xil_printf("BSCAN VALUE 2 SENT, WAITING VALUE 2\r\n");
						loopn = LOOP_STOP;
						while (TRUE)
						{
							// Read BSCAN3 VALUE 2
							bscanRegister = read_bscan(BSI_USER3);
							if(bscanRegister == 2) {
								//xil_printf("BSCAN VALUE RECEIVED 2\r\n");
//								f=open(self.path+"/results.txt","a+")
//								f.write("OK\n")
//								f.close()
								break;
							}
							else {
								loopn -= 1;
								if (loopn > 0) {
									write_bscan(BSI_USER4, 0x2);
									//xil_printf("WAITING VALUE 2,RECEIVED VALUE %d\r\n", bscanRegister);
								}
								else {
									reset = TRUE;
									//xil_printf("Resetting the board\r\n");
									break;
								}
							}
						}
						//xil_printf("Exited Loop\r\nNext test\r\n");
					}
				}
			}
		}
	} else {
		status = FRETZ_FRAME_ADDRESS_ERROR;
	}
	return status;
}

uint8_t finn_usefile(uint32_t iterations)
{
	uint32_t status = FRETZ_OK;
	FIL fp;
	static int seek = 0;
	FRESULT fr;

	f_chdir("/");

	char filename[100];
	strcpy(filename, "fault_injection_items.txt");

	fr = f_open(&fp, filename, FA_READ);

	if (fr != FR_OK) {
		xil_printf("FINN: Unknown filename %s\r\n", filename);
		f_close(&fp);
		return -1;
	}

	TCHAR buff[100];

	uint32_t frame_address, bit_index;
	char *word;
	int i = iterations;

	// Continue to previous line
	f_lseek(&fp, seek);

	while(((i > 0) || iterations == 0) && f_gets(buff, INT32_MAX, &fp)) {

		// First call, split and store first word address
		// Ignore first number from line.
		word = strtok(buff, " \n,");

		// Second call, get second number from line.
		word = strtok(NULL, " \r\n\t,");
		bit_index = atoi(word);

		// Third call, get third number from line.
		word = strtok(NULL, " \r\n\t,");
		frame_address = atoi(word);

		status = finn(frame_address, bit_index);

		if (status != FRETZ_OK)
			break;

		i--;
	}

	// If end of file reset seek.
	if (f_eof(&fp))
		seek = 0;
	else
		seek = f_tell(&fp);

	f_close(&fp);

	return status;
}
