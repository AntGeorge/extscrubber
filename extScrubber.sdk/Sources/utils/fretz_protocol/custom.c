/*
 * custom.c
 *
 *  Created on: Nov 21, 2021
 *      Author: george
 */

#include <utils/fretz_protocol/protocol/commands.h>
#include <utils/utils.h>

#include <FreeRTOS.h>
#include <utils/fretz_protocol/custom.h>

static SemaphoreHandle_t *Jtag_mutex = &jtag_mutex[0];

uint32_t custom_handler(uint32_t command, char *data, char **resp)
{
	uint32_t resp_len = 0;

	struct __attribute__((__packed__)) response_t {
		uint32_t command;
		uint8_t status;
		char **data;
	} *response;

	switch(command)
	{
	case FINN:
	{
		struct __attribute__((__packed__)) request_t {
			uint32_t frameAddress;
			uint32_t bitIndex;
		} *request_data = (struct request_t *) &data;

		resp_len = init_response(PS_CUSTOM, sizeof(*response), resp);

		response = (struct response_t *) (*resp + HEADER_SIZE);

		response->command = command;

		xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
		response->status = finn(request_data->frameAddress, request_data->bitIndex);
		xSemaphoreGive(*Jtag_mutex);

		break;
	}
	case FINN_FILE:
	{
		struct __attribute__((__packed__)) request_t {
			uint32_t iterations;
		} *request_data = (struct request_t *) &data;

		resp_len = init_response(PS_CUSTOM, sizeof(*response), resp);

		response = (struct response_t *) (*resp + HEADER_SIZE);

		response->command = command;

		xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
		response->status = finn_usefile(request_data->iterations);
		xSemaphoreGive(*Jtag_mutex);

		break;
	}
	default:
		resp_len = init_response(PS_CUSTOM, sizeof(*response), resp);
		response = (struct response_t *) (*resp + HEADER_SIZE);
		response->command = command;
		response->status = FRETZ_UNDEFINED_ERROR;
	}

	return resp_len;
}
