#include <string.h>

#include <xil_printf.h>

#include <utils/jtag/devices/common.h>
#include <fault_injection/fault_injection.h>

#include <utils/jtag/jtag.h>
#include <utils/utils.h>

#include <config_apps.h>

#include <utils/fretz_protocol/custom.h>
#include <utils/fretz_protocol/protocol/commands.h>
#include <utils/fretz_protocol/protocol/protocol.h>

#define SINGLE_BOARD_MODE 0

/*
 * Terminology
 *
 * parameters: The list of values defined as PARAM[X] in the communication protocol
 * data: The data section defined as DATA in the communication protocol
 * request: Parameters and Data of the request.
 * response: Parameters and Data of the response.
 */

static jtag_t *JtagObj = &jtag_objects[0];
static SemaphoreHandle_t *Jtag_mutex = &jtag_mutex[0];

static void set_header(uint8_t cmd, uint16_t resp_len, char *resp)
{
	header_t *r = (header_t *) resp;
	r->START = START_TAG;
	r->CMD = cmd;
	r->RSV = 0;
	r->LEN = byte_swap16(resp_len);
}

// Allocate memory for the response buffer (including the payload size).
int init_response(int cmd, int payload_size, char **resp)
{
	*resp = (char *) pvPortMalloc(HEADER_SIZE + payload_size);
	set_header(cmd, payload_size, *resp);

	return payload_size + HEADER_SIZE;
}

int read_id_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) response_t {
		uint32_t SN;
		struct __attribute__((__packed__)) {
			uint8_t MAJOR;
			uint8_t MINOR;
			uint8_t PATCH;
			uint8_t BUILD;
		} SW_version;
	} *response;

	int resp_len = init_response(READ_ID, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

#if SINGLE_BOARD_MODE==1
	response->SN = JtagObj->device_idcode;
#elif SINGLE_BOARD_MODE==0
	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
	read_creg(JtagObj, CFG_IDCODE, (word_t *) &response->SN);
	xSemaphoreGive(*Jtag_mutex);
#endif

	response->SN = byte_swap32((word_t) response->SN);

	response->SW_version.MAJOR = VERSION_MAJOR;
	response->SW_version.MINOR = VERSION_MINOR;
	response->SW_version.PATCH = VERSION_PATCH;
	response->SW_version.BUILD = VERSION_BUILD;

	return resp_len;
}

int get_status_f(char *param, char *data, char **resp)
{
	return 0;
}

int set_status_f(char *param, char *data, char **resp)
{
	return 0;
}

int write_frame_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) request_t {
		uint32_t frad;
		uint8_t n;
		uint32_t data[0];
	} *request = (struct request_t *) param;

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
	} *response;

	request->frad = byte_swap32(request->frad);

	int resp_len = init_response(WRITE_FRAME, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	frame_t *f = (frame_t *) pvPortMalloc(sizeof(frame_t) * request->n);

	for (int i = 0; i < request->n; i++) {
		for (int j = 0; j < JtagObj->device.wpf; j++) {
			(f + i)->words[j] = byte_swap32(request->data[i*JtagObj->device.wpf+j]);
		}
	}

#if SINGLE_BOARD_MODE==0
	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
	write_frames(JtagObj, request->frad, request->n, f, 0);
	xSemaphoreGive(*Jtag_mutex);
#endif

	vPortFree(f);

	response->status = FRETZ_OK;

	return resp_len;
}

int read_frame_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) request_t {
		uint32_t frad;
		uint8_t n;
	} *request = (struct request_t *) param;

	request->frad = byte_swap32(request->frad);

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
		uint32_t frad;
		uint8_t n;
		uint32_t data[0]; // variable size array
	} *response;

	int resp_len = init_response(READ_FRAME, sizeof(*response) + request->n * JtagObj->device.wpf * sizeof(response->data[0]), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	frame_t *f = (frame_t *) pvPortMalloc(sizeof(frame_t) * request->n);

#if SINGLE_BOARD_MODE==1
	for (int j = 0; j < request->n; j++) {
		for (int i = 0; i < JtagObj->device.wpf; i++) {
			f[j].words[i] = i;
		}
	}
#elif SINGLE_BOARD_MODE==0
	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
	read_frames(JtagObj, request->frad, request->n, f, 0);
	xSemaphoreGive(*Jtag_mutex);
#endif

	response->frad = byte_swap32(request->frad);
	response->n = request->n;
	response->status = FRETZ_OK;

	for (int j = 0; j < request->n; j++) {
		for (int i = 0; i < JtagObj->device.wpf; i++) {
			response->data[j*JtagObj->device.wpf+i] = byte_swap32((f + j)->words[i]);
		}
	}

	vPortFree(f);

	return resp_len;
}

int write_register_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) request_t {
		uint8_t type;
		uint8_t reg_addr;
		uint32_t reg_data;
	} *request = (struct request_t *) param;

	request->reg_data = byte_swap32(request->reg_data);

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
	} *response;

	int resp_len = init_response(WRITE_REGISTER, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	response->status = FRETZ_OK;

	//TODO: Check for valid register address (both read and write)
#if SINGLE_BOARD_MODE==1
	if (request->type != 1 && request->type != 2) {
		response->status = 3;
	}
#elif SINGLE_BOARD_MODE==0
	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);

	if (request->type == 1) {
		write_creg(JtagObj, request->reg_addr, request->reg_data);
	} else if (request->type == 2) {
		request->reg_data = reverseBits(request->reg_data, 32);
		write_jreg32(JtagObj, request->reg_addr, 32, request->reg_data);
	} else {
		response->status = FRETZ_REGISTER_ERROR;
	}

	xSemaphoreGive(*Jtag_mutex);
#endif

	return resp_len;
}

int read_register_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) request_t {
		uint8_t type;
		uint8_t reg_addr;
	} *request = (struct request_t *) param;

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
		uint8_t type;
		uint8_t reg_addr;
		uint32_t data;
	} *response;

	int resp_len = init_response(READ_REGISTER, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	response->status = FRETZ_OK;

#if SINGLE_BOARD_MODE==1
	if (request->type == 1) {
		response->data = 0x12345678;
	} else if (request->type == 2) {
		response->data = 0x87654321;
	} else {
		response->status = 3;
	}
#elif SINGLE_BOARD_MODE==0
	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);

	if (request->type == 1) {
		read_creg(JtagObj, request->reg_addr, &response->data);
	} else if (request->type == 2) {
		read_jreg(JtagObj, request->reg_addr, 32, &response->data);
		response->data = reverseBits(response->data, 32);
	} else {
		response->status = FRETZ_REGISTER_ERROR;
	}

	xSemaphoreGive(*Jtag_mutex);
#endif

	response->data = byte_swap32(response->data);
	response->reg_addr = request->reg_addr;
	response->type = request->type;

	return resp_len;
}

// TODO: This function is not implemented, it just returns OK to fretz client.
int configure_device_f(char *param, char *data, char **resp)
{
	// This struct is not used yet.
	// Bitstream id is the integer that specifies the bitstream filename to use.
	struct __attribute__((__packed__)) request_t {
		uint8_t bitstream_id;
	} *request = (struct request_t *) param;

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
	} *response;

	int resp_len = init_response(CONFIGURE_DEVICE, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	response->status = FRETZ_OK;

	return resp_len;
}

int readback_device_f(char *param, char *data, char **resp)
{
//	struct __attribute__((__packed__)) request_t {
//		uint32_t number_of_frames;
//	} *request = (struct request_t *) param;

//	request->number_of_frames = byte_swap32(request->number_of_frames);

	int number_of_frames = 10009;

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
	} *response;

	int resp_len = init_response(READBACK_DEVICE, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	frame_t *f = (frame_t *) pvPortMalloc(sizeof(frame_t) * number_of_frames);

#if SINGLE_BOARD_MODE==1
	for (int j = 0; j < request->number_of_frames; j++) {
		for (int i = 0; i < JtagObj->device.wpf; i++) {
			f[j].words[i] = i;
		}
	}
#elif SINGLE_BOARD_MODE==0
	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
	read_frames(JtagObj, 0, number_of_frames, f, 0);
	xSemaphoreGive(*Jtag_mutex);
#endif

	response->status = FRETZ_OK;

	f_chdir("/");

	// Create if not exists fretz folder in SD card
	f_mkdir(FRETZ_ROOT);
	f_chdir(FRETZ_ROOT);

	// Create if not exists readback files folder in SD card
	f_mkdir(FRETZ_RBFiles);
	f_chdir(FRETZ_RBFiles);

	FIL fd_readback;
	FRESULT fr;
	UINT br;

	fr = f_open(&fd_readback, "frame_data.bit", FA_WRITE | FA_CREATE_ALWAYS);

	if (fr == FR_OK)
	{
		for (int j = 0; j < number_of_frames; j++)
		{
			// Write to file.
			f_write(&fd_readback, &(f + j)->words, sizeof(word_t) * JtagObj->device.wpf, &br);

			if (br < sizeof(word_t) * JtagObj->device.wpf)
			{
				xil_printf("Fretz server: readback stopped, SD card is full.\r\n");
				response->status = FRETZ_UNDEFINED_ERROR;
				break;
			}
		}
	}
	else
	{
		response->status = FRETZ_INVALID_FILENAME_ERROR;
	}

	f_close(&fd_readback);
	vPortFree(f);

	return resp_len;
}

int readback_capture_device_f(char *param, char *data, char **resp)
{
	return 0;
}

int inject_faults_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) request_t {
		uint32_t frad_start;
		uint32_t frad_end;
		uint16_t bit_start;
		uint16_t bit_end;
		uint8_t flips;
		uint8_t type;
	} *request = (struct request_t *) param;

	request->frad_start = byte_swap32(request->frad_start);
	request->frad_end = byte_swap32(request->frad_end);
	request->bit_start = byte_swap16(request->bit_start);
	request->bit_end = byte_swap16(request->bit_end);

	struct __attribute__((__packed__)) response_t {
		uint8_t status;
	} *response;

	int resp_len = init_response(INJECT_FAULTS, sizeof(*response), resp);

	response = (struct response_t *) (*resp + HEADER_SIZE);

	uint32_t patterns[MAX_PATTERNS];

	patterns[0] = SBU_PATTERN;

#if SINGLE_BOARD_MODE==0

	const int iterations = 1;
	const int delay = 0;
	const int num_of_patterns = 1;
	void *returned_injections = NULL;
	const int return_injections = FALSE;
	void *num_of_injections = NULL;

	xSemaphoreTake(*Jtag_mutex, portMAX_DELAY);
	int res = fault_injection(JtagObj,
			request->frad_start, request->frad_end, request->bit_start, request->bit_end,
			request->flips, iterations, delay, request->type, patterns, num_of_patterns,
			returned_injections, return_injections, num_of_injections);
	xSemaphoreGive(*Jtag_mutex);

	switch(res)
	{
	case INJST_OK:
		response->status = FRETZ_OK;
		break;
	case INJST_FRAD_ERR:
		response->status = FRETZ_FRAME_ADDRESS_ERROR;
		break;
	case INJST_BIT_ERR:
		response->status = FRETZ_HOST_SW_WRONG_PARAMETERS;
		break;
	default:
		response->status = FRETZ_UNDEFINED_ERROR;
	}
#endif

	return resp_len;
}

int read_dut_interface_f(char *param, char *data, char **resp)
{
	return 0;
}

int write_dut_interface_f(char *param, char *data, char **resp)
{
	return 0;
}

int ps_write_f(char *param, char *data, char **resp)
{
	return 0;
}

int ps_read_f(char *param, char *data, char **resp)
{
	return 0;
}

int ps_custom_f(char *param, char *data, char **resp)
{
	struct __attribute__((__packed__)) request_t {
		uint32_t command;
		char *data;
	} *request = (struct request_t *) param;

	request->command = reverseBits(request->command, 32);
	request->command = reverseBits(request->command, 8);

	int resp_len = custom_handler(request->command, request->data, resp);

	return resp_len;
}
