#ifndef SRC_UTILS_FRETZ_PROTOCOL_PROTOCOL_PROTOCOL_H_
#define SRC_UTILS_FRETZ_PROTOCOL_PROTOCOL_PROTOCOL_H_

#include "platform/platform.h"

#include "FreeRTOS.h"

#define TBD				0
#define START_TAG		0xAA
#define HEADER_SIZE	sizeof(header_t)

extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];

enum COMMANDS_e {
	READ_ID 				= 0x01,
	GET_STATUS 				= 0x02,
	SET_STATUS				= TBD,
	WRITE_FRAME				= 0x04,
	READ_FRAME				= 0x05,
	READ_REGISTER			= 0x06,
	WRITE_REGISTER			= 0x07,
	CONFIGURE_DEVICE		= 0x08,
	READBACK_DEVICE			= 0x09,
	READBACK_CAPTURE_DEVICE	= 0x0A,
	INJECT_FAULTS			= 0x0B,
	READ_DUT_INTERFACE		= TBD,
	WRITE_DUT_INTERFACE		= TBD,
	PS_WRITE				= 0x10,
	PS_READ					= 0x11,
	PS_CUSTOM				= 0x80
};

enum POSITIONS_e {
	AA_p		= 0,
	CMD_p		= 1,
	RSV_p		= 2,
	LEN_p		= 6,
	PARAMS_p	= 8
};

enum EXECSTATUS_e {
	FRETZ_OK = 0x00,
	FRETZ_FRAME_ADDRESS_ERROR = 0x01,
	FRETZ_FRAME_DATA_ERROR = 0x02,
	FRETZ_REGISTER_ERROR = 0x03,
	FRETZ_REGISTER_ADDRESS_ERROR = 0x04,
	FRETZ_BITSTREAM_NOT_FOUND_ERROR = 0x05,
	FRETZ_CONFIGURATION_ERROR = 0x06,
	FRETZ_INVALID_FILENAME_ERROR = 0x07,
	FRETZ_READBACK_ERROR = 0x08,
	FRETZ_HOST_SW_WRONG_PARAMETERS = 0xFC,
	FRETZ_HOST_SW_ERROR = 0xFD,
	FRETZ_TIMEOUT = 0xFE,
	FRETZ_UNDEFINED_ERROR = 0xFF
};

typedef struct __attribute__((__packed__)) {
	uint8_t START;
	uint8_t CMD;
	uint32_t RSV;
	uint16_t LEN;
} header_t;

int execute(header_t header, char *command, char **response, int *response_len);
void get_parameter(enum COMMANDS_e cmd, char *params, int parameter);

#endif /* SRC_UTILS_FRETZ_PROTOCOL_PROTOCOL_PROTOCOL_H_ */
