#ifndef SRC_UTILS_FRETZ_PROTOCOL_PROTOCOL_COMMANDS_H_
#define SRC_UTILS_FRETZ_PROTOCOL_PROTOCOL_COMMANDS_H_

#include <utils/fretz_protocol/protocol/protocol.h>
#include "utils/jtag/jtag.h"

#include "platform/platform.h"

extern jtag_t jtag_objects[ACTIVE_PORTS];
extern SemaphoreHandle_t jtag_mutex[ACTIVE_PORTS];

int init_response(int cmd, int payload_size, char **resp);

int read_id_f(char *param, char *data, char **resp);
int get_status_f(char *param, char *data, char **resp);
int set_status_f(char *param, char *data, char **resp);
int write_frame_f(char *param, char *data, char **resp);
int read_frame_f(char *param, char *data, char **resp);
int write_register_f(char *param, char *data, char **resp);
int read_register_f(char *param, char *data, char **resp);
int configure_device_f(char *param, char *data, char **resp);
int readback_device_f(char *param, char *data, char **resp);
int readback_capture_device_f(char *param, char *data, char **resp);
int inject_faults_f(char *param, char *data, char **resp);
int read_dut_interface_f(char *param, char *data, char **resp);
int write_dut_interface_f(char *param, char *data, char **resp);
int ps_write_f(char *param, char *data, char **resp);
int ps_read_f(char *param, char *data, char **resp);
int ps_custom_f(char *param, char *data, char **resp);

#endif /* SRC_UTILS_FRETZ_PROTOCOL_PROTOCOL_COMMANDS_H_ */
