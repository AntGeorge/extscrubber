#include <stdio.h>

#include <utils/fretz_protocol/protocol/commands.h>
#include <utils/fretz_protocol/protocol/protocol.h>

#include "FreeRTOS.h"
#include "task.h"

#include "xil_printf.h"
#include "xtime_l.h"
#include "utils/utils.h"

// Parameters description for each command (requests).
// For details read Communication Protocol document

// COMMAND_ID, sizeof(PARAM[1]), sizeof(PARAM[2]), ..., 0, function pointer
static int read_id[] = {READ_ID, 0, (int) &read_id_f};
static int get_status[] = {GET_STATUS, 0, (int) &get_status_f};
static int set_status[] = {SET_STATUS, 0, (int) &set_status_f};
static int write_frame[] = {WRITE_FRAME, 4, 1, 0, (int) &write_frame_f};
static int read_frame[] = {READ_FRAME, 4, 1, 0, (int) &read_frame_f};
static int read_register[] = {READ_REGISTER, 1, 1, 0, (int) &read_register_f};
static int write_register[] = {WRITE_REGISTER, 1, 1, 0, (int) &write_register_f};
static int configure_device[] = {CONFIGURE_DEVICE, 0, (int) &configure_device_f};
static int readback_device[] = {READBACK_DEVICE, 0, (int) &readback_device_f};
static int readback_capture_device[] = {READBACK_CAPTURE_DEVICE, 0, (int) &readback_capture_device_f};
static int inject_faults[] = {INJECT_FAULTS, 4, 4, 2, 2, 1, 1, 0, (int) &inject_faults_f};
static int read_dut_interface[] = {}; // TBD
static int write_dut_interface[] = {}; // TBD
static int ps_write[] = {PS_WRITE, 16, 4, 4, 0, (int) &ps_write_f};
static int ps_read[] = {PS_READ, 16, 4, 0, (int) &ps_read_f};
static int ps_custom[] = {PS_CUSTOM, 4, 0, (int) &ps_custom_f};

static int *commands_list[] = {read_id, write_frame, read_frame, read_register, write_register,
		readback_device, readback_capture_device, inject_faults, configure_device,
		ps_custom, 0};


static int *get_command_descriptor(uint8_t cmd)
{
	int i = 0;
	int *index = commands_list[i];

	while(index != 0) {
		if (cmd == index[0])
			break;
		i += 1;
		index = commands_list[i];
	}

	return index;
}

static int get_param_len(int *cmd_desc)
{
	int tmp = 0;
	int i = 1;

	while(cmd_desc[i] != 0) {
		tmp += cmd_desc[i];
		i += 1;
	}

	return tmp;
}

int execute(header_t header, char *payload, char **response, int *response_len)
{

	if (header.START != START_TAG) {
		xil_printf("Fretz Server: Unknown packet's start byte\r\n");
		*response = NULL;
		return -1;
	}

	uint8_t cmd = header.CMD;

	int *cmd_desc = get_command_descriptor(cmd);

	// Check for cmd_desc == 0, in case of unknown command.
	if (cmd_desc == 0) {
		xil_printf("Fretz Server: Unknown command\r\n");
		*response = NULL;
		return -1;
	}

	int param_len = get_param_len(cmd_desc);

	// Locate the command function
	int i = 1;
	while(cmd_desc[i] != 0)
		i++;

	int (*f_ptr)(char *, char *, char **) = (void *) cmd_desc[i+1];
	*response_len = (*f_ptr)(payload, (payload + param_len), response);

	return 0;
}
