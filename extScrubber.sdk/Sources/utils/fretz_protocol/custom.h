/*
 * custom.h
 *
 *  Created on: Nov 21, 2021
 *      Author: george
 */

#ifndef SRC_INCLUDE_CUSTOM_H_
#define SRC_INCLUDE_CUSTOM_H_

#include <FreeRTOS.h>
#include <platform/platform.h>


 enum CUSTOM_COMMANDS_e {
	FINN		= 0,
	FINN_FILE	= 1
};

uint32_t custom_handler(uint32_t command, char *data, char **resp);

// Implemented commands
uint8_t finn(uint32_t frameAddress, uint32_t bitIndex);
uint8_t finn_usefile(uint32_t iterations);

#endif /* SRC_INCLUDE_CUSTOM_H_ */
