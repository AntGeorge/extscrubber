---------------------------------- VHDL Code ----------------------------------
-- package      = N/A
-- 
-- File         = fwft_fifo.vhd
-- 
-- Purpose      = Generic FIFO with FWFT timings
-- 
-- Library      = work
-- 
-- Dependencies = log2_pkg
-- 
-- Author       = V. Vlagkoulis
--
-- Copyright    = University of Piraeus 2020
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.log2_pkg.all;

entity fwft_fifo is
  generic
    (
    async_reset        : boolean := true;
    sync_reset         : boolean := true;
    width              : integer := 32;
    depth              : integer := 256;
    progfull_threshold : integer := 128;
    progfull_threshold2: integer := 128;
    progempty_threshold: integer := 128
    );
  port
    (
    clk                : in  std_logic;
    areset_n           : in  std_logic;
    sreset             : in  std_logic;
    write_mem          : in  std_logic;
    read_mem           : in  std_logic;
    data_in            : in  std_logic_vector(width - 1 downto 0);
    data_out           : out std_logic_vector(width - 1 downto 0);
    data_count         : out std_logic_vector(log2(depth) downto 0);
    empty              : out std_logic;
    full               : out std_logic;
    progempty          : out std_logic;
    progfull           : out std_logic;
    progfull2          : out std_logic
    );
end fwft_fifo;

architecture rtl of fwft_fifo is

component dualportmem
   generic
     (
     addr_width  : integer := 6;
     data_width  : integer := 8
     );
   port
     (
     clk         : in  std_logic;
     wr_en       : in  std_logic;
     wr_addr     : in  std_logic_vector(addr_width - 1 downto 0);
     data_in     : in  std_logic_vector(data_width - 1 downto 0);
     rd_en       : in  std_logic;
     rd_addr     : in  std_logic_vector(addr_width - 1 downto 0);
     data_out    : out std_logic_vector(data_width - 1 downto 0)
     );
end component;

-- MEMORY PRIMITIVE SIGNALS
signal write_pointer   : std_logic_vector((log2(depth) - 1) downto 0) := (others => '0');
signal read_pointer    : std_logic_vector((log2(depth) - 1) downto 0) := (others => '0');
signal dpram_data_count: std_logic_vector((log2(depth))     downto 0) := (others => '0');
signal i_data_count    : std_logic_vector((log2(depth))     downto 0) := (others => '0');
signal mem_data        : std_logic_vector((width - 1) downto 0);
-- END OF MEMORY PRIMITIVE SIGNALS

-- INTERNAL LOGIC SIGNALS
signal mem_empty       : std_logic := '1';
signal read_memory     : std_logic;
-- INTERNAL LOGIC SIGNALS

-- FWFT LOGIC SIGNALS
type   fifo_top_t      is array (1 downto 0) of std_logic_vector((width-1) downto 0);
signal fifo_top        : fifo_top_t := (others => (others => '0'));
signal top_fill_level  : std_logic_vector(1 downto 0) := (others => '0');
signal load_control    : std_logic_vector(1 downto 0) := (others => '0');
signal sample_fifo     : std_logic := '0'; 
-- FWFT LOGIC SIGNALS

begin

-------------------------------------------------------------------------------
-- SIGNAL ASSIGNMENTS
-------------------------------------------------------------------------------
data_count            <= i_data_count;

-- In case no data are contained in the output registers of the Verify Buffer,
-- the empry signal is asserted.
empty                 <= '1' when (top_fill_level = "00") else '0';
read_memory           <= '1' when ((load_control(1) = '0' or read_mem = '1') and mem_empty = '0') else '0';
data_out              <= fifo_top(1);

-------------------------------------------------------------------------------
-- END OF SIGNAL ASSIGNMENTS
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- PROCESSES
-------------------------------------------------------------------------------
fwft_logic_p:
  process(areset_n, clk)
    begin
      if areset_n = '0' and async_reset = TRUE then
        load_control   <= (others => '0');
        top_fill_level <= (others => '0');
--        empty          <= '1';
        sample_fifo    <= '0';
        
      elsif clk'event and clk = '1' then

        if sreset = '1' and sync_reset = TRUE then
          load_control <= (others => '0');
        elsif read_mem = '0' and read_memory = '1' then
          load_control <= load_control + 1;
        elsif read_mem = '1' and read_memory = '0' then
          load_control <= load_control - 1;
        end if;

        if sreset = '1' and sync_reset = TRUE then
          top_fill_level <= (others => '0');
        elsif read_mem = '0' and sample_fifo = '1' then
          top_fill_level <= top_fill_level + 1;
        elsif read_mem = '1' and sample_fifo = '0' then
          top_fill_level <= top_fill_level - 1;
        end if;

--        if sreset = '1' and sync_reset = TRUE then
--          empty          <= '1';
--        elsif top_fill_level = 1 and read_mem = '1' and write_mem = '0' and sample_fifo = '0' then
--          empty          <= '1';
--        elsif sample_fifo = '1' then
--          empty          <= '0';
----        elsif read_mem = '1' and i_data_count = 1 and write_mem = '0' then
----          empty          <= '1';
----        elsif write_mem = '1' then 
----          empty          <= '0';
--        end if;

        if sreset = '1' and sync_reset = TRUE then
          sample_fifo <= '0';
        else
          sample_fifo <= read_memory;
        end if;

        if sample_fifo = '1' and top_fill_level = "00" then
          fifo_top(1) <= mem_data;
        elsif read_mem = '1' and top_fill_level = "01" then
          fifo_top(1) <= mem_data;
        elsif read_mem = '1' and top_fill_level = "10" then
          fifo_top(1) <= fifo_top(0);
        end if;

        if (sample_fifo = '1' or read_mem = '1') and top_fill_level = "01" then
          fifo_top(0) <= mem_data;
        end if;

      end if;
    end process fwft_logic_p;

pointer_update_p:
  process(areset_n, clk)
    begin
      if areset_n = '0' and async_reset = TRUE then
        write_pointer   <= (others => '0');
        read_pointer    <= (others => '0');
        dpram_data_count<= (others => '0');
        mem_empty       <= '1';
        full            <= '0';
        progfull        <= '0';
        progfull2       <= '0';
        progempty       <= '1';
      elsif clk'event and clk = '1' then

        if sreset = '1' and sync_reset = TRUE then
          write_pointer <= (others => '0');
        elsif write_mem = '1' then
          write_pointer <= write_pointer + 1;
        end if;

        if sreset = '1' and sync_reset = TRUE then
          read_pointer <= (others => '0');
        elsif read_memory = '1' then
          read_pointer <= read_pointer + 1;
        end if;

        if sreset = '1' and sync_reset = TRUE then
          dpram_data_count <= (others => '0');
        elsif write_mem = '1' and read_memory = '0' then
          dpram_data_count <= dpram_data_count + 1;
        elsif write_mem = '0' and read_memory = '1' then
          dpram_data_count <= dpram_data_count - 1;
        end if;

        if sreset = '1' and sync_reset = TRUE then
          i_data_count <= (others => '0');
        elsif write_mem = '1' and read_mem = '0' then
          i_data_count <= i_data_count + 1;
        elsif write_mem = '0' and read_mem = '1' then
          i_data_count <= i_data_count - 1;
        end if;

        if sreset = '1' and sync_reset = TRUE then
          mem_empty <= '1';
        elsif dpram_data_count = 1 and read_memory = '1' and write_mem = '0' then
          mem_empty <= '1';
        elsif write_mem = '1' then
          mem_empty <= '0';
        end if;

        if sreset = '1' and sync_reset = TRUE then
          full <= '0';
        elsif dpram_data_count = (depth-1) and read_memory = '0' and write_mem = '1' then
          full <= '1';
        elsif read_memory = '1' then
          full <= '0';
        end if;

        if sreset = '1' and sync_reset = TRUE then
          progfull <= '0';
        elsif i_data_count = (progfull_threshold - 1) and write_mem = '1' and read_mem = '0' then
          progfull <= '1';
        elsif i_data_count = (progfull_threshold) and write_mem = '0' and read_mem = '1' then
          progfull <= '0';
        end if;
        
        if sreset = '1' and sync_reset = TRUE then
          progfull2<= '0';
        elsif i_data_count = (progfull_threshold2 - 1) and write_mem = '1' and read_mem = '0' then
          progfull2<= '1';
        elsif i_data_count = (progfull_threshold2) and write_mem = '0' and read_mem = '1' then
          progfull2<= '0';
        end if;
        
        if sreset = '1' and sync_reset = TRUE then
          progempty<= '1';
        elsif i_data_count = (progempty_threshold - 1) and write_mem = '1' and read_mem = '0' then
          progempty<= '0';
        elsif i_data_count = (progempty_threshold) and write_mem = '0' and read_mem = '1' then
          progempty<= '1';
        end if;
      end if;
    end process pointer_update_p;
-------------------------------------------------------------------------------
-- END OF PROCESSES
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- COMPONENT INSTANTIATIONS
-------------------------------------------------------------------------------
  generic_dual_port_macro : dualportmem
     generic map
       (
       addr_width  => (log2(depth)),
       data_width  => width
       )
     port map
       (
       clk         => clk,
       wr_en       => write_mem,
       wr_addr     => write_pointer,
       data_in     => data_in,
       rd_en       => read_memory,
       rd_addr     => read_pointer,
       data_out    => mem_data
       );
-------------------------------------------------------------------------------
-- END OF COMPONENT INSTANTIATIONS
-------------------------------------------------------------------------------

end rtl;
