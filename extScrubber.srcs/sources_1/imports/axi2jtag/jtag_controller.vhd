---------------------------------- VHDL Code ----------------------------------
-- File         = fifo2jtag.vhd
-- 
-- Purpose      = This block implements FIFOs to JTAG I/F.
-- 
-- Note         = 
--
-- Limitations  = None
--
-- Library      = work
-- 
-- Dependencies = 
-- 
-- Author       = V. Vlagkoulis (University of Piraeus)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


entity jtag_controller is
  generic
    (
    inverse_data             : boolean := true
    );
  port
    (
    ---------------------------------------------------------------------------
    -- Global Signals
    ---------------------------------------------------------------------------
    clk                      : in  std_logic;
    resetn                   : in  std_logic;
    calib_reg                : in  std_logic_vector(31 downto 0);
    calib_reg_updated        : in  std_logic;
    jtag_busy                : out std_logic;
    uploaded_length          : out std_logic_vector(19 downto 0);
    ---------------------------------------------------------------------------
    -- CONTROL FIFO Signals
    ---------------------------------------------------------------------------
    ctrlfifo_read            : out std_logic;
    ctrlfifo_rddata          : in  std_logic_vector(31 downto 0);
    ctrlfifo_empty           : in  std_logic;
    ---------------------------------------------------------------------------
    -- DATA FIFO Signals
    ---------------------------------------------------------------------------
    axi2fifo_read            : out std_logic;
    axi2fifo_rddata          : in  std_logic_vector(31 downto 0);
    axi2fifo_empty           : in  std_logic;
    fifo2axi_write           : out std_logic;
    fifo2axi_wrdata          : out std_logic_vector(31 downto 0);
    fifo2axi_reset           : out std_logic;
    ---------------------------------------------------------------------------
    -- JTAG Signals
    ---------------------------------------------------------------------------
    tck                      : out std_logic;
    tms                      : out std_logic;
    tdi                      : out std_logic;
    tdo                      : in  std_logic
    );
end jtag_controller;

architecture rtl of jtag_controller is


type   states_t              is (IDLE_S, TRANSFER_S);
signal current_state         : states_t;
signal start_transfer        : std_logic := '0';
signal transfer_started      : std_logic := '0';
signal transfer_done         : std_logic := '0';
signal transfer_length       : std_logic_vector(4 downto 0) := (others => '0');
signal tms_vector            : std_logic_vector(15 downto 0) := (others => '0');
signal tdi_vector            : std_logic_vector(15 downto 0) := (others => '0');
signal tdo_vector            : std_logic_vector(15 downto 0);
signal remaining_length      : std_logic_vector(19 downto 0) := (others => '0');
signal i_uploaded_length     : std_logic_vector(19 downto 0) := (others => '0');
signal select_word           : std_logic := '0';
signal last_transfer         : std_logic := '0';
signal i_fifo2axi_write      : std_logic := '0';
signal i_fifo2axi_reset      : std_logic := '0';
signal ignore_reply_data     : std_logic := '0';
signal i_axi2fifo_read       : std_logic := '0';



begin

-------------------------------------------------------------------------------
-- OUTPUT SIGNALS
-------------------------------------------------------------------------------
axi2fifo_read                <= i_axi2fifo_read;
fifo2axi_write               <= i_fifo2axi_write;
fifo2axi_reset               <= i_fifo2axi_reset;
uploaded_length              <= i_uploaded_length;
-------------------------------------------------------------------------------
-- END OF OUTPUT SIGNALS
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- CONTROL SIGNALS PROCESS
-------------------------------------------------------------------------------
control_signals_p: process(resetn, clk)
  begin
    if resetn = '0' then
      current_state               <= IDLE_S;
      start_transfer              <= '0';
      remaining_length            <= (others => '0');
      i_uploaded_length           <= (others => '0');
      select_word                 <= '0';
      tms_vector                  <= (others => '0');
      tdi_vector                  <= (others => '0');
      i_fifo2axi_write            <= '0';
      i_fifo2axi_reset            <= '0';
      ignore_reply_data           <= '0';
      last_transfer               <= '0';
      jtag_busy                   <= '0';
      transfer_started            <= '0';
      i_axi2fifo_read             <= '0';
      fifo2axi_wrdata             <= (others => '0');
      transfer_length             <= (others => '0');

    elsif (clk'event and clk = '1') then
      start_transfer              <= i_axi2fifo_read;

      if transfer_done = '1' and ignore_reply_data = '0' and 
         (select_word = '1' or last_transfer = '1') then
        i_fifo2axi_write          <= '1';
      else
        i_fifo2axi_write          <= '0';
      end if;

      if i_fifo2axi_write = '1' then
        fifo2axi_wrdata           <= (others => '0');
      end if;
      

      if i_fifo2axi_reset = '1' then
        i_uploaded_length         <= (others => '0');
      end if;


      case current_state is

        when IDLE_S =>
          select_word             <= '0';
          transfer_started        <= '0';
          last_transfer           <= '0';
          i_axi2fifo_read         <= '0';

          if ctrlfifo_empty = '0' then
            current_state         <= TRANSFER_S;
            remaining_length      <= ctrlfifo_rddata(19 downto 0);
            ignore_reply_data     <= ctrlfifo_rddata(30);
            i_fifo2axi_reset      <= ctrlfifo_rddata(31);
            ctrlfifo_read         <= '1';
            jtag_busy             <= '1';
          else
            current_state         <= IDLE_S;
            ctrlfifo_read         <= '0';
            jtag_busy             <= '0';
          end if;


        when TRANSFER_S =>
          ctrlfifo_read           <= '0';
          i_fifo2axi_reset        <= '0';
          jtag_busy               <= '1';

          if transfer_done = '1' then
            transfer_started      <= '0';
          elsif axi2fifo_empty = '0' then
            transfer_started      <= '1';
          end if;

          if remaining_length > 16 then
            transfer_length       <= "10000";
            last_transfer         <= '0';
          else
            transfer_length       <= remaining_length(4 downto 0);
            last_transfer         <= '1';
          end if;

          if last_transfer = '1' and transfer_done = '1' then
            current_state         <= IDLE_S;
          else
            current_state         <= TRANSFER_S;
          end if;

          if transfer_done = '1' then
            remaining_length      <= remaining_length - transfer_length;
            i_uploaded_length     <= i_uploaded_length + transfer_length;
            select_word           <= not select_word;
          end if;

          if axi2fifo_empty = '0' and transfer_started = '0' then
            i_axi2fifo_read       <= '1';
          else
            i_axi2fifo_read       <= '0';
          end if;

          if i_axi2fifo_read = '1' then
            for i in 0 to 15 loop
              if inverse_data = true then
                tms_vector(i)     <= axi2fifo_rddata(15-i);
                tdi_vector(i)     <= axi2fifo_rddata(31-i);
              else
                tms_vector(i)     <= axi2fifo_rddata(i);
                tdi_vector(i)     <= axi2fifo_rddata(i+16);
              end if;
            end loop;
          end if;


          if transfer_done = '1' and (select_word = '1' or last_transfer = '1') then
            for i in 0 to 15 loop
              if inverse_data = true then
                fifo2axi_wrdata(i)     <= tdo_vector(15-i);
              else
                fifo2axi_wrdata(i)     <= tdo_vector(i);
              end if;
            end loop;
          elsif transfer_done = '1' and select_word = '0' then
            for i in 0 to 15 loop
              if inverse_data = true then
                fifo2axi_wrdata(i+16)  <= tdo_vector(15-i);
              else
                fifo2axi_wrdata(i+16)  <= tdo_vector(i);
              end if;
            end loop;
          end if;

      end case;
    end if;
end process control_signals_p;
-------------------------------------------------------------------------------
-- END OF CONTROL SIGNALS PROCESS
-------------------------------------------------------------------------------


jtagif: entity work.jtag_if
  port map
    (
    clk                      => clk,
    resetn                   => resetn,
    start_transfer           => start_transfer,
    transfer_done            => transfer_done,
    tck_divider              => calib_reg(31 downto 28),
    calib_shifts             => calib_reg(25 downto 24),
    transfer_length          => transfer_length,
    tms_vector               => tms_vector,
    tdi_vector               => tdi_vector,
    tdo_vector               => tdo_vector,
    tck                      => tck,
    tms                      => tms,
    tdi                      => tdi,
    tdo                      => tdo
    );


 
end rtl;
