---------------------------------- VHDL Code ----------------------------------
-- package      = N/A
-- 
-- File         = dualportmem.vhd
-- 
-- Purpose      = Dual Port Memory
-- 
-- Library      = work
-- 
-- Dependencies = None
-- 
-- Author       = A. Tavoularis
--
-- Copyright    = TELETEL 2010-2011, updated in 2015
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity dualportmem is
   generic
     (
     addr_width  : integer := 6;
     data_width  : integer := 8
     );
   port
     (
     clk         : in  std_logic;
     wr_en       : in  std_logic;
     wr_addr     : in  std_logic_vector(addr_width - 1 downto 0);
     data_in     : in  std_logic_vector(data_width - 1 downto 0);
     rd_en       : in  std_logic;
     rd_addr     : in  std_logic_vector(addr_width - 1 downto 0);
     data_out    : out std_logic_vector(data_width - 1 downto 0) := (others => '0')
     );
end dualportmem;

architecture rtl of dualportmem is

constant mem_capacity_c : integer := 2**addr_width;

type   mem_array_t is array (0 to mem_capacity_c - 1) of std_logic_vector(data_width-1 downto 0);
signal mem_array   : mem_array_t := (others => (others => '0'));

begin

memory_write_process:
  process(clk)
    begin
      if clk'event and clk = '1' then
        if wr_en = '1' then
          mem_array(conv_integer(wr_addr)) <= data_in;
        end if;
      end if;
    end process memory_write_process;

memory_read_process:
  process(clk)
    begin
      if clk'event and clk = '1' then
        if rd_en = '1' then
          data_out <= mem_array(conv_integer(rd_addr));
        end if;
      end if;
    end process memory_read_process;
  
end rtl;
