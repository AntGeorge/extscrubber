---------------------------------- VHDL Code ------------------------------------
-- File name    = log2_pkg.vhd
-- 
-- Purpose      = Package for log2 look-up table.
--
-- Note         = None
--
-- Limitations  = Supported up to 128 Kbytes
--
-- Library      = work
-- 
-- Dependencies = None
-- 
-- Author       = V. Vlagkoulis
---------------------------------------------------------------------------------
package log2_pkg is

function log2(value : integer) return integer; 

end log2_pkg;



package body log2_pkg is

function log2(value : integer) return integer is 
begin
  case value is 
    when 0              => return 0;
    when 1              => return 0;
    when 2              => return 1;
    when 3     to 4     => return 2;
    when 5     to 8     => return 3;
    when 9     to 16    => return 4;
    when 17    to 32    => return 5;
    when 33    to 64    => return 6;
    when 65    to 128   => return 7;
    when 129   to 256   => return 8;
    when 257   to 512   => return 9;
    when 513   to 1024  => return 10;
    when 1025  to 2048  => return 11;
    when 2049  to 4096  => return 12;
    when 4097  to 8192  => return 13;
    when 8193  to 16384 => return 14;
    when 16385 to 32768 => return 15;
    when 32769 to 65536 => return 16;
    when others => return 17;
  end case;
end log2;

end package body log2_pkg;
