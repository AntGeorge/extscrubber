---------------------------------- VHDL Code ----------------------------------
-- File         = jtag_if.vhd
-- 
-- Purpose      = JTAG Interface.
-- 
-- Note         = 
--
-- Limitations  = None
--
-- Library      = work
-- 
-- Dependencies = 
-- 
-- Author       = V. Vlagkoulis (University of Piraeus)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


entity jtag_if is
  port
    (
    ---------------------------------------------------------------------------
    -- Global Signals
    ---------------------------------------------------------------------------
    clk                      : in  std_logic;
    resetn                   : in  std_logic;
    ---------------------------------------------------------------------------
    -- Control Signals
    ---------------------------------------------------------------------------
    start_transfer           : in  std_logic;
    transfer_done            : out std_logic;
    tck_divider              : in  std_logic_vector(3 downto 0);
    calib_shifts             : in  std_logic_vector(1 downto 0);
    transfer_length          : in  std_logic_vector(4 downto 0);
    tms_vector               : in  std_logic_vector(15 downto 0);
    tdi_vector               : in  std_logic_vector(15 downto 0);
    tdo_vector               : out std_logic_vector(15 downto 0);
    ---------------------------------------------------------------------------
    -- JTAG Signals
    ---------------------------------------------------------------------------
    tck                      : out std_logic;
    tms                      : out std_logic;
    tdi                      : out std_logic;
    tdo                      : in  std_logic
    );
end jtag_if;

architecture rtl of jtag_if is


type   states_t              is (IDLE_S, TRANSFER_S, XTRA_SHIFTS_S, END_S);
signal state                 : states_t := IDLE_S;
signal read_transfer         : std_logic := '0';
signal tms_output            : std_logic_vector(15 downto 0) := (others => '0');
signal tdi_output            : std_logic_vector(15 downto 0) := (others => '0');
signal tdo_capture           : std_logic_vector(15 downto 0) := (others => '0');
signal tck_count             : std_logic_vector(3 downto 0) := (others => '0');
signal bit_count             : std_logic_vector(4 downto 0) := (others => '0');
signal end_shift_count       : std_logic_vector(4 downto 0) := (others => '0');
signal xtra_shift_count      : std_logic_vector(1 downto 0) := (others => '0');
signal i_tck                 : std_logic := '0';
signal tck_int               : std_logic := '0';

attribute dont_touch         : string;
attribute dont_touch of i_tck   : signal is "true";
attribute dont_touch of tck_int : signal is "true";

begin

-------------------------------------------------------------------------------
-- OUTPUT SIGNALS
-------------------------------------------------------------------------------
tdi                          <= tdi_output(0);
tms                          <= tms_output(0);
tck                          <= i_tck;
tdo_vector                   <= tdo_capture;
-------------------------------------------------------------------------------
-- END OF OUTPUT SIGNALS
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- CONTROL LOGIC
-------------------------------------------------------------------------------
control_logic_p: process(resetn, clk)
  begin
    if resetn = '0' then
      state                       <= IDLE_S;
      transfer_done               <= '0';
      tms_output                  <= (others => '0');
      tdi_output                  <= (others => '0');
      tdo_capture                 <= (others => '0');
      tck_count                   <= (others => '0');
      bit_count                   <= (others => '0');
      xtra_shift_count            <= (others => '0');
      end_shift_count             <= (others => '0');
      i_tck                       <= '0';
      tck_int                     <= '0';

    elsif (clk'event and clk = '1') then
      transfer_done               <= '0';

      case state is

        when IDLE_S =>
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          i_tck                   <= '0';
          tck_int                 <= '0';


          if start_transfer = '1' then
            state                 <= TRANSFER_S;
            tms_output            <= tms_vector;
            tdi_output            <= tdi_vector;
            tck_count             <= '0' & tck_divider(3 downto 1);
            bit_count             <= transfer_length;
            xtra_shift_count      <= calib_shifts;
            end_shift_count       <= "10000" - transfer_length;
            tdo_capture           <= (others => '0');
          else
            tms_output            <= (others => '0');
            tdi_output            <= (others => '0');
          end if;



        when TRANSFER_S =>
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          if tck_count(3 downto 1) = 0 then
            tck_count             <= '0' & tck_divider(3 downto 1);
            i_tck                 <= not i_tck;
            tck_int               <= not tck_int;
          else
            tck_count             <= tck_count - 1;
          end if;

          if tck_count(3 downto 1) = 0 and tck_int = '1' then
            bit_count             <= bit_count - 1;
            tms_output            <= '0' & tms_output(15 downto 1);
            tdi_output            <= '0' & tdi_output(15 downto 1);
            if bit_count(4 downto 1) = 0 and xtra_shift_count /= 0 then
              state               <= XTRA_SHIFTS_S;
            elsif bit_count(4 downto 1) = 0 then
              state               <= END_S;
            end if;
          end if;

          if tck_count(3 downto 1) = 0 and tck_int = '0' then
            tdo_capture           <= tdo & tdo_capture(15 downto 1);
          end if;



        when XTRA_SHIFTS_S =>
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          i_tck                   <= '0';


          if tck_count(3 downto 1) = 0 then
            tck_count             <= '0' & tck_divider(3 downto 1);
            tck_int               <= not tck_int;
          else
            tck_count             <= tck_count - 1;
          end if;

          if tck_count(3 downto 1) = 0 and tck_int = '1' then
            xtra_shift_count      <= xtra_shift_count - 1;
            if xtra_shift_count(1) = '0' then
              state               <= END_S;
            end if;
          end if;

          if tck_count(3 downto 1) = 0 and tck_int = '0' then
            tdo_capture           <= tdo & tdo_capture(15 downto 1);
          end if;



        when END_S =>
          ---------------------------------------------------------------------
          ---------------------------------------------------------------------
          if end_shift_count = 0 then
            state                 <= IDLE_S;
            transfer_done         <= '1';
          end if;

          if end_shift_count /= 0 then
            tdo_capture           <= '0' & tdo_capture(15 downto 1);
            end_shift_count       <= end_shift_count - 1;
          end if;


        when others =>
          state                   <= IDLE_S;

      end case;
    end if;
end process control_logic_p;

end rtl;
