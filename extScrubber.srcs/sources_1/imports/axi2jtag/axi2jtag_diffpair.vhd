---------------------------------- VHDL Code ----------------------------------
-- File         = axi2jtag.vhd
-- 
-- Purpose      = Top Level of the AXI Slave to JTAG bridge.
-- 
-- Note         = 
--
-- Limitations  = None
--
-- Library      = work
-- 
-- Dependencies = 
-- 
-- Author       = V. Vlagkoulis (University of Piraeus)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity axi2jtag is
  generic
    (
    inverse_data             : boolean := true
    );
  port
    (
    ---------------------------------------------------------------------------
    -- Global Signals
    ---------------------------------------------------------------------------
    clk                      : in  std_logic;
    resetn                   : in  std_logic;
    ---------------------------------------------------------------------------
    -- Slave AXI Write Signals
    ---------------------------------------------------------------------------
    s_axi_awaddr             : in  std_logic_vector(31 downto 0);
    s_axi_awvalid            : in  std_logic;
    s_axi_awready            : out std_logic;
    s_axi_awid               : in  std_logic_vector(11 downto 0);
    s_axi_awlock             : in  std_logic;
    s_axi_awcache            : in  std_logic_vector(3 downto 0);
    s_axi_awprot             : in  std_logic_vector(2 downto 0);
    s_axi_awlen              : in  std_logic_vector(3 downto 0);
    s_axi_awsize             : in  std_logic_vector(1 downto 0);
    s_axi_awburst            : in  std_logic_vector(1 downto 0);
    s_axi_awqos              : in  std_logic_vector(3 downto 0);
    s_axi_wdata              : in  std_logic_vector(31 downto 0);
    s_axi_wvalid             : in  std_logic;
    s_axi_wready             : out std_logic;
    s_axi_wid                : in  std_logic_vector(11 downto 0);
    s_axi_wlast              : in  std_logic;
    s_axi_wstrb              : in  std_logic_vector(3 downto 0);
    s_axi_bvalid             : out std_logic;
    s_axi_bready             : in  std_logic;
    s_axi_bid                : out std_logic_vector(11 downto 0);
    s_axi_bresp              : out std_logic_vector(1 downto 0);
    ---------------------------------------------------------------------------
    -- Slave AXI Read Signals
    ---------------------------------------------------------------------------
    s_axi_araddr             : in  std_logic_vector(31 downto 0);
    s_axi_arvalid            : in  std_logic;
    s_axi_arready            : out std_logic;
    s_axi_arid               : in  std_logic_vector(11 downto 0);
    s_axi_arlock             : in  std_logic_vector(1 downto 0);
    s_axi_arcache            : in  std_logic_vector(3 downto 0);
    s_axi_arprot             : in  std_logic_vector(2 downto 0);
    s_axi_arlen              : in  std_logic_vector(3 downto 0);
    s_axi_arsize             : in  std_logic_vector(1 downto 0);
    s_axi_arburst            : in  std_logic_vector(1 downto 0);
    s_axi_arqos              : in  std_logic_vector(3 downto 0);
    s_axi_rdata              : out std_logic_vector(31 downto 0);
    s_axi_rvalid             : out std_logic;
    s_axi_rready             : in  std_logic;
    s_axi_rid                : out std_logic_vector(11 downto 0);
    s_axi_rlast              : out std_logic;
    s_axi_rresp              : out std_logic_vector(1 downto 0);
    ---------------------------------------------------------------------------
    -- JTAG Signals
    ---------------------------------------------------------------------------
    tck_p                    : out std_logic;
    tck_n                    : out std_logic;
    tms_p                    : out std_logic;
    tms_n                    : out std_logic;
    tdi_p                    : out std_logic;
    tdi_n                    : out std_logic;
    tdo_p                    : in  std_logic;
    tdo_n                    : out std_logic
    );
end axi2jtag;

architecture top of axi2jtag is


signal status_reg            : std_logic_vector(31 downto 0);
signal calib_reg             : std_logic_vector(31 downto 0);
signal calib_reg_updated     : std_logic;
signal axi2ctrlfifo_write    : std_logic;
signal axi2ctrlfifo_wrdata   : std_logic_vector(31 downto 0);
signal axi2ctrlfifo_read     : std_logic;
signal axi2ctrlfifo_rddata   : std_logic_vector(31 downto 0);
signal axi2ctrlfifo_empty    : std_logic;
signal axi2ctrlfifo_full     : std_logic;
signal axi2ctrlfifo_dcount   : std_logic_vector(7 downto 0);
signal axi2datafifo_write    : std_logic;
signal axi2datafifo_wrdata   : std_logic_vector(31 downto 0);
signal axi2datafifo_read     : std_logic;
signal axi2datafifo_rddata   : std_logic_vector(31 downto 0);
signal axi2datafifo_empty    : std_logic;
signal axi2datafifo_full     : std_logic;
signal axi2datafifo_dcount   : std_logic_vector(10 downto 0);
signal fifo2axi_write        : std_logic;
signal fifo2axi_wrdata       : std_logic_vector(31 downto 0);
signal fifo2axi_read         : std_logic;
signal fifo2axi_rddata       : std_logic_vector(31 downto 0);
signal fifo2axi_full         : std_logic;
signal fifo2axi_empty        : std_logic;
signal fifo2axi_dcount       : std_logic_vector(9 downto 0);
signal fifo2axi_reset        : std_logic;
signal jtag_busy             : std_logic;

begin

tck_n                        <= '0';
tms_n                        <= '0';
tdi_n                        <= '0';
tdo_n                        <= '0';

status_reg(31)               <= jtag_busy;
status_reg(30)               <= '0';
status_reg(29 downto 20)     <= fifo2axi_dcount;
status_reg(19)               <= '0';
status_reg(18 downto 8)      <= axi2datafifo_dcount;
status_reg(7 downto 0)       <= axi2ctrlfifo_dcount;



axi_slv_fifo: entity work.axi_slave_if
  port map
    (
    ---------------------------------------------------------------------------
    -- Global Signals
    ---------------------------------------------------------------------------
    clk                      => clk,
    resetn                   => resetn,
    ---------------------------------------------------------------------------
    -- Slave AXI Write Signals
    ---------------------------------------------------------------------------
    s_axi_awaddr             => s_axi_awaddr,
    s_axi_awvalid            => s_axi_awvalid,
    s_axi_awready            => s_axi_awready,
    s_axi_awid               => s_axi_awid,
    s_axi_awlock             => s_axi_awlock,
    s_axi_awcache            => s_axi_awcache,
    s_axi_awprot             => s_axi_awprot,
    s_axi_awlen              => s_axi_awlen,
    s_axi_awsize             => s_axi_awsize,
    s_axi_awburst            => s_axi_awburst,
    s_axi_awqos              => s_axi_awqos,
    s_axi_wdata              => s_axi_wdata,
    s_axi_wvalid             => s_axi_wvalid,
    s_axi_wready             => s_axi_wready,
    s_axi_wid                => s_axi_wid,
    s_axi_wlast              => s_axi_wlast,
    s_axi_wstrb              => s_axi_wstrb,
    s_axi_bvalid             => s_axi_bvalid,
    s_axi_bready             => s_axi_bready,
    s_axi_bid                => s_axi_bid,
    s_axi_bresp              => s_axi_bresp,
    ---------------------------------------------------------------------------
    -- Slave AXI Read Signals
    ---------------------------------------------------------------------------
    s_axi_araddr             => s_axi_araddr,
    s_axi_arvalid            => s_axi_arvalid,
    s_axi_arready            => s_axi_arready,
    s_axi_arid               => s_axi_arid,
    s_axi_arlock             => s_axi_arlock,
    s_axi_arcache            => s_axi_arcache,
    s_axi_arprot             => s_axi_arprot,
    s_axi_arlen              => s_axi_arlen,
    s_axi_arsize             => s_axi_arsize,
    s_axi_arburst            => s_axi_arburst,
    s_axi_arqos              => s_axi_arqos,
    s_axi_rdata              => s_axi_rdata,
    s_axi_rvalid             => s_axi_rvalid,
    s_axi_rready             => s_axi_rready,
    s_axi_rid                => s_axi_rid,
    s_axi_rlast              => s_axi_rlast,
    s_axi_rresp              => s_axi_rresp,
    ---------------------------------------------------------------------------
    -- STATUS Signals
    ---------------------------------------------------------------------------
    status_reg               => status_reg,
    ---------------------------------------------------------------------------
    -- CONTROL FIFO Signals
    ---------------------------------------------------------------------------
    calib_reg                => calib_reg,
    calib_reg_updated        => calib_reg_updated,
    axi2ctrlfifo_write       => axi2ctrlfifo_write,
    axi2ctrlfifo_wrdata      => axi2ctrlfifo_wrdata,
    axi2ctrlfifo_full        => axi2ctrlfifo_full,
    ---------------------------------------------------------------------------
    -- AXI2FIFO Signals
    ---------------------------------------------------------------------------
    axi2datafifo_write       => axi2datafifo_write,
    axi2datafifo_wrdata      => axi2datafifo_wrdata,
    axi2datafifo_full        => axi2datafifo_full,
    ---------------------------------------------------------------------------
    -- FIFO2AXI Signals
    ---------------------------------------------------------------------------
    fifo2axi_read            => fifo2axi_read,
    fifo2axi_rddata          => fifo2axi_rddata,
    fifo2axi_full            => fifo2axi_full,
    fifo2axi_empty           => fifo2axi_empty
    );


jtag_ctrl: entity work.jtag_controller
  generic map
    (
    inverse_data             => inverse_data
    )
  port map
    (
    ---------------------------------------------------------------------------
    -- Global Signals
    ---------------------------------------------------------------------------
    clk                      => clk,
    resetn                   => resetn,
    calib_reg                => calib_reg,
    calib_reg_updated        => calib_reg_updated,
    uploaded_length          => open,
    jtag_busy                => jtag_busy,
    ---------------------------------------------------------------------------
    -- CONTROL FIFO Signals
    ---------------------------------------------------------------------------
    ctrlfifo_read            => axi2ctrlfifo_read,
    ctrlfifo_rddata          => axi2ctrlfifo_rddata,
    ctrlfifo_empty           => axi2ctrlfifo_empty,
    ---------------------------------------------------------------------------
    -- DATA FIFO Signals
    ---------------------------------------------------------------------------
    axi2fifo_read            => axi2datafifo_read,
    axi2fifo_rddata          => axi2datafifo_rddata,
    axi2fifo_empty           => axi2datafifo_empty,
    fifo2axi_write           => fifo2axi_write,
    fifo2axi_wrdata          => fifo2axi_wrdata,
    fifo2axi_reset           => fifo2axi_reset,
    ---------------------------------------------------------------------------
    -- JTAG Signals
    ---------------------------------------------------------------------------
    tck                      => tck_p,
    tms                      => tms_p,
    tdi                      => tdi_p,
    tdo                      => tdo_p
    );


ctrlfifo: entity work.fwft_fifo
  generic map
    (
    async_reset              => true,
    sync_reset               => false,
    width                    => 32,
    depth                    => 128,
    progfull_threshold       => 64,
    progfull_threshold2      => 64,
    progempty_threshold      => 64
    )
  port map
    (
    clk                      => clk,
    areset_n                 => resetn,
    sreset                   => '0',
    write_mem                => axi2ctrlfifo_write,
    read_mem                 => axi2ctrlfifo_read,
    data_in                  => axi2ctrlfifo_wrdata,
    data_out                 => axi2ctrlfifo_rddata,
    data_count               => axi2ctrlfifo_dcount,
    empty                    => axi2ctrlfifo_empty,
    full                     => axi2ctrlfifo_full,
    progempty                => open,
    progfull                 => open,
    progfull2                => open
    );


datafifo: entity work.fwft_fifo
  generic map
    (
    async_reset              => true,
    sync_reset               => false,
    width                    => 32,
    depth                    => 1024,
    progfull_threshold       => 512,
    progfull_threshold2      => 512,
    progempty_threshold      => 512
    )
  port map
    (
    clk                      => clk,
    areset_n                 => resetn,
    sreset                   => '0',
    write_mem                => axi2datafifo_write,
    read_mem                 => axi2datafifo_read,
    data_in                  => axi2datafifo_wrdata,
    data_out                 => axi2datafifo_rddata,
    data_count               => axi2datafifo_dcount,
    empty                    => axi2datafifo_empty,
    full                     => axi2datafifo_full,
    progempty                => open,
    progfull                 => open,
    progfull2                => open
    );


replyfifo: entity work.fwft_fifo
  generic map
    (
    async_reset              => true,
    sync_reset               => true,
    width                    => 32,
    depth                    => 512,
    progfull_threshold       => 256,
    progfull_threshold2      => 256,
    progempty_threshold      => 256
    )
  port map
    (
    clk                      => clk,
    areset_n                 => resetn,
    sreset                   => fifo2axi_reset,
    write_mem                => fifo2axi_write,
    read_mem                 => fifo2axi_read,
    data_in                  => fifo2axi_wrdata,
    data_out                 => fifo2axi_rddata,
    data_count               => fifo2axi_dcount,
    empty                    => fifo2axi_empty,
    full                     => fifo2axi_full,
    progempty                => open,
    progfull                 => open,
    progfull2                => open
    );

end top;
