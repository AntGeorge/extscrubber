---------------------------------- VHDL Code ----------------------------------
-- File         = axi_slave_if.vhd
-- 
-- Purpose      = This block implements AXI Slave Interface to/from Registers/FIFOs.
-- 
-- Note         = 
--
-- Limitations  = None
--
-- Library      = work
-- 
-- Dependencies = 
-- 
-- Author       = V. Vlagkoulis (University of Piraeus)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


entity axi_slave_if is
  port
    (
    ---------------------------------------------------------------------------
    -- Global Signals
    ---------------------------------------------------------------------------
    clk                      : in  std_logic;
    resetn                   : in  std_logic;
    ---------------------------------------------------------------------------
    -- Slave AXI Write Signals
    ---------------------------------------------------------------------------
    s_axi_awaddr             : in  std_logic_vector(31 downto 0);
    s_axi_awvalid            : in  std_logic;
    s_axi_awready            : out std_logic;
    s_axi_awid               : in  std_logic_vector(11 downto 0);
    s_axi_awlock             : in  std_logic;
    s_axi_awcache            : in  std_logic_vector(3 downto 0);
    s_axi_awprot             : in  std_logic_vector(2 downto 0);
    s_axi_awlen              : in  std_logic_vector(3 downto 0);
    s_axi_awsize             : in  std_logic_vector(1 downto 0);
    s_axi_awburst            : in  std_logic_vector(1 downto 0);
    s_axi_awqos              : in  std_logic_vector(3 downto 0);
    s_axi_wdata              : in  std_logic_vector(31 downto 0);
    s_axi_wvalid             : in  std_logic;
    s_axi_wready             : out std_logic;
    s_axi_wid                : in  std_logic_vector(11 downto 0);
    s_axi_wlast              : in  std_logic;
    s_axi_wstrb              : in  std_logic_vector(3 downto 0);
    s_axi_bvalid             : out std_logic;
    s_axi_bready             : in  std_logic;
    s_axi_bid                : out std_logic_vector(11 downto 0);
    s_axi_bresp              : out std_logic_vector(1 downto 0);
    ---------------------------------------------------------------------------
    -- Slave AXI Read Signals
    ---------------------------------------------------------------------------
    s_axi_araddr             : in  std_logic_vector(31 downto 0);
    s_axi_arvalid            : in  std_logic;
    s_axi_arready            : out std_logic;
    s_axi_arid               : in  std_logic_vector(11 downto 0);
    s_axi_arlock             : in  std_logic_vector(1 downto 0);
    s_axi_arcache            : in  std_logic_vector(3 downto 0);
    s_axi_arprot             : in  std_logic_vector(2 downto 0);
    s_axi_arlen              : in  std_logic_vector(3 downto 0);
    s_axi_arsize             : in  std_logic_vector(1 downto 0);
    s_axi_arburst            : in  std_logic_vector(1 downto 0);
    s_axi_arqos              : in  std_logic_vector(3 downto 0);
    s_axi_rdata              : out std_logic_vector(31 downto 0);
    s_axi_rvalid             : out std_logic;
    s_axi_rready             : in  std_logic;
    s_axi_rid                : out std_logic_vector(11 downto 0);
    s_axi_rlast              : out std_logic;
    s_axi_rresp              : out std_logic_vector(1 downto 0);
    ---------------------------------------------------------------------------
    -- STATUS Signals
    ---------------------------------------------------------------------------
    status_reg               : in  std_logic_vector(31 downto 0);
    ---------------------------------------------------------------------------
    -- CONTROL FIFO Signals
    ---------------------------------------------------------------------------
    axi2ctrlfifo_write       : out std_logic;
    axi2ctrlfifo_wrdata      : out std_logic_vector(31 downto 0);
    axi2ctrlfifo_full        : in  std_logic;
    calib_reg                : out std_logic_vector(31 downto 0);
    calib_reg_updated        : out std_logic;
    ---------------------------------------------------------------------------
    -- AXI2FIFO Signals
    ---------------------------------------------------------------------------
    axi2datafifo_write       : out std_logic;
    axi2datafifo_wrdata      : out std_logic_vector(31 downto 0);
    axi2datafifo_full        : in  std_logic;
    ---------------------------------------------------------------------------
    -- FIFO2AXI Signals
    ---------------------------------------------------------------------------
    fifo2axi_read            : out std_logic;
    fifo2axi_rddata          : in  std_logic_vector(31 downto 0);
    fifo2axi_full            : in  std_logic;
    fifo2axi_empty           : in  std_logic
    );
end axi_slave_if;

architecture rtl of axi_slave_if is

type   wr_states_t           is (IDLE_S, ADDRESS_S, TRANSFER_S, RESPONSE_S);
type   rd_states_t           is (IDLE_S, ADDRESS_S, TRANSFER_S);
signal write_state           : wr_states_t;
signal read_state            : rd_states_t;
signal read_transfer         : std_logic;
signal select_axi2fifo       : std_logic;
signal select_axi2datafifo   : std_logic;
signal select_fifo2axi       : std_logic;
signal read_status_reg       : std_logic;
signal i_fifo2axi_read       : std_logic;
signal read_len              : std_logic_vector(3 downto 0);

begin

-------------------------------------------------------------------------------
-- OUTPUT SIGNALS
-------------------------------------------------------------------------------
s_axi_bid                    <= (others => 'Z');
s_axi_rid                    <= (others => 'Z');
s_axi_bresp                  <= (others => '0');
s_axi_rresp                  <= (others => '0');
s_axi_rvalid                 <= ((not fifo2axi_empty) and read_transfer) when select_fifo2axi = '1' else read_transfer; 
s_axi_rdata                  <= fifo2axi_rddata when select_fifo2axi = '1' else status_reg;
fifo2axi_read                <= i_fifo2axi_read;
i_fifo2axi_read              <= s_axi_rready and select_fifo2axi and (not fifo2axi_empty);
-------------------------------------------------------------------------------
-- END OF OUTPUT SIGNALS
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- WRITE TRANSFER PROCESS
-------------------------------------------------------------------------------
write_transfer_p: process(resetn, clk)
  begin
    if resetn = '0' then
      write_state                 <= IDLE_S;
      s_axi_awready               <= '0';
      s_axi_wready                <= '0';
      s_axi_bvalid                <= '0';
      axi2ctrlfifo_write          <= '0';
      axi2datafifo_write          <= '0';
      calib_reg_updated           <= '0';
      select_axi2fifo             <= '0';
      select_axi2datafifo         <= '0';

    elsif (clk'event and clk = '1') then

      case write_state is

        when IDLE_S =>
          ---------------------------------------------------------------------
          -- If the awvalid is asserted by the AXI Master then AXI write_state 
          -- goes to ADDRESS_S state and the awready output is asserted.
          ---------------------------------------------------------------------
          s_axi_wready            <= '0';
          s_axi_bvalid            <= '0';
          axi2ctrlfifo_write      <= '0';
          axi2datafifo_write      <= '0';
          calib_reg_updated       <= '0';

          if s_axi_awvalid = '1' then
            write_state           <= ADDRESS_S;
            s_axi_awready         <= '1';
          else
            s_axi_awready         <= '0';
          end if;

          ---------------------------------------------------------------------
          -- 0x0: CONTROL FIFO
          -- 0x4: DATA FIFO
          -- 0x8: CALIB REGISTER
          ---------------------------------------------------------------------
          if s_axi_awvalid = '1' and s_axi_awaddr(3) = '0' then
            select_axi2fifo       <= '1';
            select_axi2datafifo   <= s_axi_awaddr(2);
          else
            select_axi2fifo       <= '0';
            select_axi2datafifo   <= '0';
          end if;
         

        when ADDRESS_S =>
          ---------------------------------------------------------------------
          -- The state machine remains to ADDRESS_S state until the AXI Master
          -- asserts the wvalid and all the FIFOs are not full. 
          ---------------------------------------------------------------------
          s_axi_awready           <= '1';
          s_axi_bvalid            <= '0';
          axi2ctrlfifo_write      <= '0';
          axi2datafifo_write      <= '0';
          calib_reg_updated       <= '0';

          if s_axi_wvalid = '1' and 
             (select_axi2fifo = '0' or 
              (axi2ctrlfifo_full = '0' and axi2datafifo_full = '0' and fifo2axi_full = '0')) then
            write_state           <= TRANSFER_S;
            s_axi_wready          <= '1';
          else
            s_axi_wready          <= '0';
          end if;


        when TRANSFER_S =>
          ---------------------------------------------------------------------
          -- This is the write transfer state. The wready is set to '1', 
          -- so that no waiting state is inserted. At the end of this state,
          -- the state machine goes to RESPONSE_S and the bvalid is asserted.
          ---------------------------------------------------------------------
          s_axi_awready           <= '1';
          s_axi_wready            <= '1';

          if s_axi_wvalid = '1' and s_axi_wlast = '1'then
            write_state           <= RESPONSE_S;
            s_axi_bvalid          <= '1';
          else
            s_axi_bvalid          <= '0';
          end if;

          if s_axi_wvalid = '1' and select_axi2fifo = '1' and select_axi2datafifo = '1' then
            axi2ctrlfifo_write    <= '0';
            axi2datafifo_write    <= '1';
            axi2datafifo_wrdata   <= s_axi_wdata;
          elsif s_axi_wvalid = '1' and select_axi2fifo = '1' and select_axi2datafifo = '0' then
            axi2ctrlfifo_write    <= '1';
            axi2datafifo_write    <= '0';
            axi2ctrlfifo_wrdata   <= s_axi_wdata;
          else
            axi2ctrlfifo_write    <= '0';
            axi2datafifo_write    <= '0';
          end if;

          if s_axi_wvalid = '1' and select_axi2fifo = '0' then
            calib_reg             <= s_axi_wdata;
            calib_reg_updated     <= '1';
          else
            calib_reg_updated     <= '0';
          end if;


        when RESPONSE_S =>
          ---------------------------------------------------------------------
          -- The state machine remains in this state until the bready signal 
          -- is asserted by the AXI Master.
          ---------------------------------------------------------------------
          axi2ctrlfifo_write      <= '0';
          axi2datafifo_write      <= '0';
          calib_reg_updated       <= '0';

          if s_axi_bready = '1' then
            write_state           <= IDLE_S;
            s_axi_awready         <= '0';
            s_axi_wready          <= '0';
            s_axi_bvalid          <= '0';
          else
            s_axi_awready         <= '1';
            s_axi_wready          <= '1';
            s_axi_bvalid          <= '1';
          end if;


        when others =>
          write_state             <= IDLE_S;

      end case;
    end if;
end process write_transfer_p;
-------------------------------------------------------------------------------
-- END OF WRITE TRANSFER PROCESS
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- READ TRANSFER PROCESS
-------------------------------------------------------------------------------
read_transfer_p: process(resetn, clk)
  begin
    if resetn = '0' then
      read_state                  <= IDLE_S;
      read_transfer               <= '0';
      s_axi_arready               <= '0';
      s_axi_rlast                 <= '0';
      read_len                    <= (others => '0');
      read_status_reg             <= '0';
      select_fifo2axi             <= '0';

    elsif (clk'event and clk = '1') then

      case read_state is

        when IDLE_S =>
          ---------------------------------------------------------------------
          -- If the arvalid is asserted by the AXI Master then AXI read_state 
          -- goes to ADDRESS_S state and the arready output is asserted.
          ---------------------------------------------------------------------
          s_axi_rlast             <= '0';
          select_fifo2axi         <= '0';
          read_transfer           <= '0';

          if s_axi_arvalid = '1' then
            read_state            <= ADDRESS_S;
            read_len              <= s_axi_arlen;
            s_axi_arready         <= '1';
          else
            s_axi_arready         <= '0';
          end if;

          if s_axi_arvalid = '1' and s_axi_araddr(2) = '0' then
            read_status_reg       <= '1';
          else
            read_status_reg       <= '0';
          end if;
         

        when ADDRESS_S =>
          ---------------------------------------------------------------------
          -- The AXI Master can have access to FIFO using the s_axi_araddr = 0x4.
          -- Otherwise (s_axi_araddr = 0x0), it can have access to the current  
          -- number of entries in the FIFO (status register). 
          -- The state machine goes to TRANSFER_S state, if the FIFO has available 
          -- data or if the FIFO counter is going to be accessed. 
          ---------------------------------------------------------------------
          s_axi_arready           <= '1';

          if read_status_reg = '1' or fifo2axi_empty = '0' then
            read_state            <= TRANSFER_S;
            select_fifo2axi       <= not read_status_reg;
            read_transfer         <= '1';
          else
            read_transfer         <= '0';
          end if;

          if read_len = 0 then
            s_axi_rlast           <= '1';
          else
            s_axi_rlast           <= '0';
          end if;


        when TRANSFER_S =>
          ---------------------------------------------------------------------
          -- This is the read transfer state. At the end of the transfer,
          -- the state machine goes to IDLE_S.
          ---------------------------------------------------------------------
          if (s_axi_rready = '1' and select_fifo2axi = '0') or i_fifo2axi_read = '1' then
            read_len              <= read_len - 1;
          end if;

          if ((s_axi_rready = '1' and select_fifo2axi = '0') or i_fifo2axi_read = '1') and read_len = 0 then
            read_state            <= IDLE_S;
            read_transfer         <= '0';
            select_fifo2axi       <= '0';
            s_axi_arready         <= '0';
          else
            read_transfer         <= '1';
            s_axi_arready         <= '1';
          end if;

          if s_axi_rready = '1' and read_len = 1 then
            s_axi_rlast           <= '1';
          end if;


        when others =>
          read_state              <= IDLE_S;

      end case;
    end if;
end process read_transfer_p;


 
end rtl;
