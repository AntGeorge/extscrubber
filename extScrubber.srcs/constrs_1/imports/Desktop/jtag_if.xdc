set_input_delay -clock [get_clocks clk_fpga_0] -min -add_delay 1.000 [get_ports tdo_p]
set_input_delay -clock [get_clocks clk_fpga_0] -max -add_delay 9.000 [get_ports tdo_p]

set_output_delay -clock [get_clocks clk_fpga_0] -min -add_delay -2.000 [get_ports tck_p]
set_output_delay -clock [get_clocks clk_fpga_0] -max -add_delay 1.000 [get_ports tck_p]
set_output_delay -clock [get_clocks clk_fpga_0] -min -add_delay -2.000 [get_ports tdi_p]
set_output_delay -clock [get_clocks clk_fpga_0] -max -add_delay 1.000 [get_ports tdi_p]
set_output_delay -clock [get_clocks clk_fpga_0] -min -add_delay -2.000 [get_ports tms_p]
set_output_delay -clock [get_clocks clk_fpga_0] -max -add_delay 1.000 [get_ports tms_p]

