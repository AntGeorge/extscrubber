
![Block Diagram](blockDesign.PNG)

# Table of contents
- [How to use](#how-to-use)
    - [Device.json](#devicejson)
- [Detailed Configuration](#detailed-configuration)
  - [Processing-System](#processing-system)
    - [linker libraries (-l)](#linker-libraries--l)
    - [compiler directories (include paths)](#compiler-directories-include-paths)
    - [BSP libraries](#bsp-libraries)
    - [FreeRTOS Settings](#freertos-settings)
  - [Programmable Logic](#programmable-logic)
    - [Zynq7 Processing System Settings](#zynq7-processing-system-settings)


# How to use

Copy the following into an SD card.

1. BOOT.bin (located on MasterSystem*/bootimage/)
2. Device.json (located on Devices/. Use the correct json file based on the target FPGA and **rename it to Device.json**)
3. webfiles/ (located on WebPanel/)

### Device.json

This file describes the target FPGA. It must contain the following parameters:

1. part: The part name of the target
2. ARCH: The architecture of the target
   + 0: ZYNQ7000
   + 1: ULTRASCALE
3. IDCODE: The idcode value of the target
4. WPF: Words per frame
5. BPW: Bits per word
6. IR_LEN: Instruction register length
7. JC_BEFORE: Number of bits in the JTAG chain before the target
8. JC_AFTER: Number of bits in the JTAG chain after the target
9. JMODE: JTAG mode
    + 0: INDEPENDENT
    + 1: CASCADE
10. PIPELINE: The number of pipeline words.
11. BSI_e: The dictionary of all BSI registers and their values (see defined Device.json for details).


# Detailed Configuration

## Processing-System

### linker libraries (-l)

+ lwip4
+ xilffs
+ m

### compiler directories (include paths)

+ $\{workspace\_loc:/Sources\}
+ $\{workspace\_loc:/MasterSystem\_&lt;target&gt;\_bsp/ps7\_cortexa9\_0/include\}

### BSP libraries

+ lwip211
    + api_mode: SOCKET_API
    + dhcp_options:
        + dhcp_does_arp_check: true
        + lwip_dhcp: true
+ xilffs
    + set_fs_rpath: 1
    + use_lfn: 1
    + use_strfunc: 1

### FreeRTOS Settings

+ kernel_behavior
    + total_heap_size = 500MB

**Notes for Xilinx SDK 2018.3:**

*You might see an error like ‘conflicting types of f_gets’ in ff.c
This is because xilffs has an unfixed bug in 2018.3 version. To solve this issue
you need to copy modded_bsp_lib directory into src folder of the application. After
that go to the top menu Xilinx > Repositories and add the modded_bsp_lib to the
local repositories as relative path. Click OK and then regenerate the BSP sources.*

## Programmable Logic

### Zynq7 Processing System Settings

+ I/O Peripherals
    + UART 1
    + SD 0
    + ENET 0
+ Clock Configuration
    + PL Fabric Clocks
        + FCLK_CLK0
+ PS-PL Configuration
    + General
        + Enable Clock Resets
            + FCLK_RESET0_N
    + AXI Non Secure Enablement
        + GP Master AXI Interface
            + M AXI GP0 interface
